<?php
/**
 * This class contains some common functions
 *
 * @author Trung Nguyen
 */

namespace API\CoreBundle\Util;

class Helper
{
	const APC_CACHE = 'apc';
	const MEM_CACHE = 'memcache';
	const NO_CACHE 	= 'none';

    /**
     * Generate session token
     *
     * @author  Co Vu Thanh Tung
     * @return  string $token
     */
    public static function genSessionToken()
    {
        return md5(uniqid(rand(0,999999), true));
    }
    
    /**
     * Generate API Signature for Content-Type : application/json
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public static function genSignatureJson($params, $pathInfo)
    {
        $params = str_replace("\n", '', $params);
        $params = str_replace("\r", '', $params);
    
        return md5($pathInfo . $params);
    }
  
    /**
     * Generate API Signature
     *
     * @author  Co Vu Thanh Tung
     * @param   string $params
     * @param   array  $pathInfo
     * @return  string
     */
    public static function genSignature($params, $pathInfo)
    {
        $result = $pathInfo;
    
        if(isset($params['sig'])) {
        	unset($params['sig']);
        }
        
        ksort($params);
    
        foreach ($params as $key => $value) {
            if($key != 'sig') {
                if (is_array($value)) {
          		    foreach ($value as $subValue) {
          			    $result .= rawurlencode( $key . '[]' ) . urlencode($subValue);
          		    }
          	    } else {
                    $result .= $key . urlencode($value);
          	    }
            }
        }
        
        return md5($result);
    }
}

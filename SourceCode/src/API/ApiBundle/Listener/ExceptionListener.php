<?php
namespace API\ApiBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;

class ExceptionListener {
    /**
     * Customize response
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event) {
        $exception = $event->getException();

        $jsonData = array(
            'status' => intval($exception->getCode()),
            'error' => $exception->getMessage()
        );

        try {
            $response = new Response(json_encode($jsonData), strval($exception->getCode()));
        } catch (\Exception $e) {
            $response = new Response(json_encode($jsonData), '400');
            $response->headers->set('X-Status-Code', '400');
        }

        // setup the Response object based on the caught exception
        $event->setResponse($response);

        // you can alternatively set a new Exception
        // $exception = new \Exception('Some special exception');
        // $event->setException($exception);
    }
}
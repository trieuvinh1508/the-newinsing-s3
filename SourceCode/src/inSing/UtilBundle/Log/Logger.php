<?php

namespace inSing\UtilBundle\Log;

use Monolog\Logger as MLogger;
use Monolog\Handler\StreamHandler;

/**
 * Logger service
 * 
 * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
 */
class Logger 
{
    // log type constants
    const LOG_TYPE_FILE = 'file';
    const LOG_TYPE_DB   = 'db';
    
    // default channel
    const CHANNEL_NAME  = 'inSing_util_logger';
    
    protected $logType = array('file', 'db');
    
    // log level list
    protected $logLevel = array('info', 'notice', 'warning', 'error');
    
    /**
     * @var array
     */
    protected $config;
    
    /**
     * @var MLogger
     */
    protected $logger;
    
    /**
     * @var MLogger
     */
    protected $dbLogger;
    
    /**
     * @var array
     */
    protected $handlerKey;

    /**
     * @var string
     */
    protected $currentLogType;
    
    /**
     * Constructor
     * 
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     * @param  array $config
     */
    public function __construct(array $config) 
    {
        if (!in_array(self::LOG_TYPE_FILE, $config['type']) && !in_array(self::LOG_TYPE_FILE, $config['type'])) {
            throw new \Exception('Invalid log type');
        }
        
        // init.
        $this->config     = $config;
        $this->handlerKey = array();
    }

    /**
     * Magic __call method
     * 
     * Supported methods: 
     * - addInfoToFile | addNoticeToFile | addWarningToFile | addErrorToFile 
     * 
     * Usage:
     * - $logger->addInfoToFile($message, $context, $log_filename)
     * - $logger->file->addInfo($message, $context, $log_filename)
     * 
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     * @param  string $method
     * @param  mixed  $args
     * @return mixed
     * @throws \Exception
     */
    public function __call($method, $args)
    {
        $pattern1 = strtr('/^add(?<log_level>[LOG_LEVEL])To(?<log_type>[LOG_TYPE])/i', array(
            '[LOG_LEVEL]' => implode('|', $this->logLevel), 
            '[LOG_TYPE]'  => implode('|', $this->logType)
        ));
        
        $pattern2 = strtr('/^add(?<log_level>[LOG_LEVEL])/i', array(
            '[LOG_LEVEL]' => implode('|', $this->logLevel) 
        ));
        
        if ($result = preg_match($pattern1, $method, $matches)) {
            $logType  = $matches['log_type'];
            $logLevel = $matches['log_level'];
        } elseif ($result = preg_match($pattern2, $method, $matches)) {
            $logType  = $this->currentLogType;
            $logLevel = $matches['log_level'];
        }
        
        if ($result) {
            
            switch (strtolower($logType)) {
                
                case self::LOG_TYPE_FILE:
                    // instance Logger if needed
                    if (empty($this->logger)) {
                        $this->logger = new MLogger(self::CHANNEL_NAME);
                    }
                    
                    // generate log file's path
                    $logDir = $this->config['file']['path'];
                    $logFileName = ( isset($args[2]) == '' || $args[2] == 'log' ) ? $this->config['file']['name'] : $args[2];
                    $fullPath = $logDir.DIRECTORY_SEPARATOR.$logFileName;
                    $handlerKey = md5($fullPath);
                    
                    // instance StreamHandler if needed
                    if (!in_array($handlerKey, $this->handlerKey)) {
                        $this->handlerKey[] = $handlerKey;
                        $this->logger->pushHandler(new StreamHandler($fullPath, $handlerKey, false));
                    }
                    
                    // configure method name
                    $method = 'add'.ucfirst(strtolower($logLevel));
                    $this->logger->$method($args[0], isset($args[1]) ? $args[1] : array());
                    break;
                
               
                case self::LOG_TYPE_DB:                    
                    // implementation will be soon ;)
                    break;
            }
            
        } else {
            throw new \Exception('Call to undefined method');
        }
    }
    
    /**
     * Magic getter
     *
     * @param string $name
     * @return $this
     */
    public function __get($name)
    {
        if (!in_array($name, $this->logType)) {
            throw new \Exception('Call to undefined property');
        }
        
        $this->currentLogType = $name;
        
        return $this;
    }

    /**
     * This function is similar to [__call]. Just make compatible with old version ;)
     *
     * @author Le Quoc Duan (re-edited by Trung Nguyen)
     * @param  string $message
     * @param  array  $context
     * @param  array  $extra  
     * @return boolean
     */
    public function setLog($message, $context = array(), $extra = array())
    {
        return $this->addInfoToFile($message, $context);
    }
}
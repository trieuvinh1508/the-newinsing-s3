<?php

namespace inSing\UtilBundle\Cache;

/**
 * File cache driver.
 *
 * @author  Trung Nguyen <trung.nguyen@s3corp.com.vn>
 */
class FileCache extends AbstractCache
{
    
    protected $path;
    
    /**
     * {@inheritdoc}
     */
    public function getIds()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function _doGetCache($id)
    {
        $ret = false;
        
        if (file_exists($this->path.$id)) {
            $ret = unserialize(file_get_contents($this->path.$id));
        }
        
        return $ret;
    }

    /**
     * {@inheritdoc}
     */
    protected function _doHasCache($id)
    {
        return file_exists($this->path.$id);
    }

    /**
     * {@inheritdoc}
     */
    protected function _doSetCache($id, $data, $lifeTime = 0)
    {
        return file_put_contents($this->path.$id, serialize($data));
    }

    /**
     * {@inheritdoc}
     */
    protected function _doDeleteCache($id)
    {
        return unlink($this->path.$id);
    }
    
    /**
     * Set the full path of cache dir
     * 
     * @param string $path
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     */
    public function setPath($path) 
    {
        $path = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        
        $this->path = $path;
    }
    
    /**
     * Get the duration that cache entry created
     * 
     * @param string $id
     * @return int/false
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     */
    public function getLifetime($id)
    {
        if (file_exists($this->path.$id)) {
            $time = time() - filemtime($this->path.$id);
        }
        
        return isset($time) ? $time : false;
    }
    
}
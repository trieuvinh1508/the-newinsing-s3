<?php
namespace inSing\ApiBundle\Controller;

use Doctrine\ORM\Internal\CommitOrderCalculator;
use Symfony\Component\HttpFoundation\Request,
    Symfony\Component\HttpFoundation\JsonResponse,
    API\CoreBundle\Util\InsingAPI,
    API\CoreBundle\Util\Helper as APIHelper,
    inSing\ApiBundle\Util\Common,
    FOS\RestBundle\Util\Codes as APICode,
    inSing\DataSourceBundle\Repository\ContentRepository
    ;


class RestController extends APIBaseController
{

    function getAllFeaturesAction(Request $request)
    {
        $IsAPI = $this->get('IsAPI');
        $data = $IsAPI->getAllFeaturesContent();

        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.features_insing_api");
        $routeOptions = $this->currentRoute->getOptions();
        $params = array(
            'function' => 'insing_features_all_content',
            'page_number'   => Common::convertInt($request->query->get('page', 1), 'page'),
            'page_size'     => Common::convertInt($request->query->get('per_page', 10), 'page_size')
        );

        $arrSortBy = array('publish_date', 'last_updated_date');
        if ( isset($params['sort_by']) && (empty($params['sort_by']) || !in_array($params['sort_by'], $arrSortBy)) ) {
            throw new \Exception('Sort by invalid.', strval(APICode::HTTP_BAD_REQUEST));
        }
        $cache = null;
        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cache_name = $cache->createCacheName($params);
        }

        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {
                $IsAPI = $this->get('IsAPI');
                $data = $IsAPI->getAllFeaturesContent();

            if ( !is_null($cache) && is_array($data) && count($data) ) {
                $cache->setCache($cache_name, $data, $cache_time);
            }
        } else {
            $logger->addInfo('===== HAS CACHE Features Content from CACHE ===== with Cache name: ' . $cache_name);
            $data = $cache->getCache($cache_name);
        }
        $result = array();

        $result = array(
            'status' => (string)APICode::HTTP_OK,
            'data' => $data
        );

        return $this->output(APICode::HTTP_OK, $result);

    }

    function getAllEventsAction(Request $request)
    {
        $params = array();
        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.events_insing_api");
        $routeOptions = $this->currentRoute->getOptions();
        $params = array(
            'function' => 'insing_events_all_content',
            'page'   => Common::convertInt($request->query->get('page', 1), 'page'),
            'per_page'     => Common::convertInt($request->query->get('per_page', 10), 'page_size')
        );
        $cache = null;
        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cache_name = $cache->createCacheName($params);
        }
        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {

            $data = $this->getDoctrine()->getRepository('inSingDataSourceBundle:PulledEvent')
                ->getApiPulledEventListByCondition($params);

            if ( !is_null($cache) && is_array($data) && count($data) ) {
                $cache->setCache($cache_name, $data, $cache_time);
            }
        } else {
            $logger->addInfo('===== HAS CACHE Features Content from CACHE ===== with Cache name: ' . $cache_name);
            $data = $cache->getCache($cache_name);
        }
        $result = array();
        $result = array(
            'status' => (string)APICode::HTTP_OK,
            'data' => $data
        );
        return $this->output(APICode::HTTP_OK, $result);
    }

    function getAllMoviesAction(Request $request)
    {
        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.movies_insing_api");
        $routeOptions = $this->currentRoute->getOptions();
        $params = array(
            'function' => 'insing_movies_all_content',
            'page'   => Common::convertInt($request->query->get('page', 1), 'page'),
            'per_page'     => Common::convertInt($request->query->get('per_page', 10), 'page_size')
        );

        $cache = null;
        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cache_name = $cache->createCacheName($params);
        }
        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {

            $data = $this->getDoctrine()->getRepository('inSingDataSourceBundle:PulledMovie')
                ->getApiPulledMovieListByCondition($params);

            if ( !is_null($cache) && is_array($data) && count($data) ) {
                $cache->setCache($cache_name, $data, $cache_time);
            }
        } else {
            $logger->addInfo('===== HAS CACHE Movies Content from CACHE ===== with Cache name: ' . $cache_name);
            $data = $cache->getCache($cache_name);
        }
        $result = array();
        $result = array(
            'status' => (string)APICode::HTTP_OK,
            'data' => $data
        );
        return $this->output(APICode::HTTP_OK, $result);

    }
    /**
     * @author Khuong.Dang
     */
    function getCarouselContentAction()
    {
        $requestParams = $this->createCarouselParams($_REQUEST);
        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.movies_insing_api");
        $routeOptions = $this->currentRoute->getOptions();

        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cacheKey = implode('-', array(__METHOD__, implode('-', $requestParams)));
            $cache_name = $cache->createCacheName($cacheKey);

        }

        if(isset($requestParams['use_cache']) && $requestParams['use_cache'] == "N") {
            $cache = null;
        }

        //if not cache get newest data
        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {
            try {
                $carouselContent = array();
                $carouselContent = $this->getDoctrine()->getRepository('inSingDataSourceBundle:Content')
                    ->getListHomepageApi($requestParams);

            } catch (\Exception $e){
                echo $e->getMessage();
            }

            $carouselContentData = array();
            if(count($carouselContent > 0)){
                foreach($carouselContent  as $i => $data) {
                    $carouselContentData[$i] = $this->createCarouselContentData($data);
                }
            } else {
                    $carouselContentData = array();
            }
                    $carouselData = $carouselContentData;

            //set cache data
            if ( !is_null($cache) && is_array($carouselData) && count($carouselData) ) {
                $cache->setCache($cache_name, $carouselData, $cache_time);
            }
        } else {

            //get data from cache
            $logger->addInfo('===== HAS CACHE Carousel Content from CACHE ===== with Cache name: ' . $cache_name);
            $carouselData = $cache->getCache($cache_name);
        }
        //pagination
        $page = $requestParams['page'];
        $page = isset($page) && $page >= 1 ? $page : 1;
        $pageOffset = $requestParams['page_offset'];
        $perPage = $requestParams['per_page'];

        if (!isset($perPage) || !($perPage = Common::filterInt($requestParams['per_page'])) || $perPage <= 0) {
            $perPage = $this->container->getParameter('new_insing_api_per_page');
        }

        $perPage = isset($perPage) ? $perPage : $this->container->getParameter('new_insing_api_per_page');

        if (isset($pageOffset)) {
            $start = $pageOffset - 1;
        } else {
            $start = (($page - 1) * $perPage);
        }
        $data = array_slice($carouselData, $start, $perPage);
        $result = array();

        if ($start < count($carouselData)) {
            $result = array(
                'status' => (string)APICode::HTTP_OK,
                'page' => $page,
                'per_page' => $perPage,
                'total' => count($data),
                'data' => $data
            );
        } else {
            $result = array(
                'status' => (string)APICode::HTTP_CREATED,
                'data' => array()
            );
        }

        return $this->output(APICode::HTTP_OK, $result);

    }

    /**
     * @author Khuong.Dang
     */
    function getHgwContentAction()
    {
        $requestParams = $this->createHgwParams($_REQUEST);
        $cache_time = $this->container->getParameter('new_insing_api_cache_lifetime');
        $logger = $this->get("monolog.logger.movies_insing_api");
        $routeOptions = $this->currentRoute->getOptions();

        if ($routeOptions['cache'] == true) {
            $cache = $this->cache;
            $cacheKey = implode('-', array(__METHOD__, implode('-', $requestParams)));
            $cache_name = $cache->createCacheName($cacheKey);

        }
        if(isset($requestParams['use_cache']) && $requestParams['use_cache'] == "N") {
            $cache = null;
        }
        //if not cache get newest data
        if ( is_null($cache) || ( $cache && !$cache->hasCache($cache_name))) {
            $hgwContent = array();
            $hgwContent = $this->getDoctrine()->getRepository('inSingDataSourceBundle:Content')
                ->getListHomepageApi($requestParams);
            if(count($hgwContent) > 0 ) {
                foreach ($hgwContent as $i => $data) {
                    $hgwContentData[$i] = $this->createHgwContentData($data);
                }
            } else {
                $hgwContentData = array();
            }
            $hgwData = $hgwContentData;
            //set cache data
            if ( !is_null($cache) && is_array($hgwData) && count($hgwData) ) {
                $cache->setCache($cache_name, $hgwData, $cache_time);
            }
        } else {
            //get data from cache
            $logger->addInfo('===== HAS CACHE Hgw Content from CACHE ===== with Cache name: ' . $cache_name);
            $hgwData = $cache->getCache($cache_name);
        }
        //pagination
        $page = $requestParams['page'];
        $page = isset($page) && $page >= 1 ? $page : 1;
        $pageOffset = $requestParams['page_offset'];
        $perPage = $requestParams['per_page'];

        if (!isset($perPage) || !($perPage = Common::filterInt($requestParams['per_page'])) || $perPage <= 0) {
            $perPage = $this->container->getParameter('new_insing_api_per_page');
        }
        $perPage = isset($perPage) ? $perPage : $this->container->getParameter('new_insing_api_per_page');

        if (isset($pageOffset)) {
            $start = $pageOffset - 1;
        } else {
            $start = (($page - 1) * $perPage);
        }
        $data = array_slice($hgwData, $start, $perPage);

        $result = array();
        if ($start < count($hgwData)) {
            $result = array(
                'status' => (string)APICode::HTTP_OK,
                'page' => $page,
                'per_page' => $perPage,
                'total' => count($data),
                'data' => $data
            );
        } else {
            $result = array(
                'status' => (string)APICode::HTTP_CREATED,
                'data' => array()
            );
        }

        return $this->output(APICode::HTTP_OK, $result);

    }

    private function createCarouselParams(array $request) {
        $requestParams = array();
        if (!isset($request['page']) || !($requestParams['page'] = Common::filterInt($request['page'])) || $requestParams['page'] <= 0) {
            $requestParams['page'] = 1;
        }
        $requestParams['page_offset'] = isset($request['page_offset']) && $request['page_offset'] >= 1 ? $request['page_offset'] : null;
        if (!isset($request['per_page']) || !($requestParams['per_page'] = Common::filterInt($request['per_page'])) || $requestParams['per_page'] <= 0) {
            $requestParams['per_page'] = $this->container->getParameter('new_insing_api_per_page');
        }
        if(isset($request['content_type']) && $request['content_type'] != "") {
            if(!is_numeric($request['content_type'])) {
                throw new \Exception("Content type must be a number!", APICode::HTTP_BAD_REQUEST);
            } else {
                $requestParams['content_type'] = $request['content_type'];
            }
        }

        if(isset($request['filter']) && $request['filter'] != "") {
            if($request['filter']!= "published"  && $request['filter']!= "scheduled" && $request['filter']!= "expired" ) {
                throw new \Exception("Invalid filter param! Please try again.", APICode::HTTP_BAD_REQUEST);
            } else {
                $requestParams['filter'] = $request['filter'];
            }
        }

        if (isset($request['use_cache']) && $request['use_cache'] != "" ) {
            $requestParams['use_cache'] = $request['use_cache'];
        }

        $requestParams['module'] = ContentRepository::CAROUSEL_MODULE;

        ksort($requestParams);
        return $requestParams;
    }

    private function createCarouselContentData($carouselContent) {
        if(isset($carouselContent) && $carouselContent != "") {
            $dataCarousel["id"] = $carouselContent['id'];
            $dataCarousel["module"] = $carouselContent['module'];
            $chanel = $carouselContent['content_type'];
            $chanel_name = "";
            switch($chanel) {
                case "1":
                    $chanel_name = "Movie";
                break;
                case "2":
                    $chanel_name = "Event";
                break;
                case "3":
                    $chanel_name = "Restaurant";
                break;
                case "4":
                    $chanel_name = "Restaurant Deal";
                break;
                case "5":
                    $chanel_name = "Hungry Deal";
                break;
                case "6":
                    $chanel_name = "Article";
                break;
                case "7":
                    $chanel_name = "Gallery";
                    break;
                default:
                break;
            }
            $dataCarousel["content_type"]       = $chanel_name;
            $dataCarousel["content_type_num"]   = $chanel;
            $dataCarousel["item_id"]            = $carouselContent['item_id'];
            $dataCarousel["chanel_name"]        = $carouselContent['channel_name'];
            $dataCarousel["editor"]             = isset($carouselContent['editor']) ? $carouselContent['editor'] : ""; //
            $dataCarousel["title"]              = $carouselContent['title'];
            $dataCarousel["description"]        = $carouselContent['description'];
            $dataCarousel["contentUrl"]         = isset($carouselContent['content_url']) ? $carouselContent['content_url'] : "";
            $dataCarousel["imageUrl"]           = isset($carouselContent['image_url']) ? $carouselContent['image_url'] : "";
            $dataCarousel["slot"]               = $carouselContent['slot'];
            $dataCarousel["publish_on"]         = $carouselContent['publish_on'];

            $dataCarousel["expired_on"]         = isset($carouselContent['expire_on']) ? $carouselContent['expire_on'] : "";
            $dataCarousel["status"]             = $carouselContent['status'];
            $dataCarousel["created_at"]         = $carouselContent['created_at'];
            $dataCarousel["updated_at"]         = $carouselContent['updated_at'];
            $dataCarousel["deal_id"]            = "";
            $dataCarousel["tabledb_id"]         = "";
            $dataCarousel["slug"]               = "";
            $dataCarousel["series_seo_url"]     = "";
            $dataCarousel["event_data"]         = "";

            if(isset($carouselContent['extra_field']) && $carouselContent['extra_field'] != "") {
                $extra_field = json_decode($carouselContent['extra_field'], true);
                $dataCarousel["deal_id"]    = isset($extra_field['deal_id']) ? $extra_field['deal_id'] : "";
                $dataCarousel["tabledb_id"] = isset($extra_field['tabledb_id']) ? $extra_field['tabledb_id'] : "";
                $dataCarousel["slug"]       = isset($extra_field['slug']) ? $extra_field['slug'] : "";
                $dataCarousel["series_seo_url"] = isset($extra_field['series_seo_url']) ? $extra_field['series_seo_url'] : "";
                $event_data = json_decode(isset($extra_field['event_data']) ? $extra_field['event_data']: "" ,true);
                $dataCarousel["event_data"] = isset($event_data) ? $event_data : "";
            }

            return $dataCarousel;
        }
    }

    private function createHgwParams(array $request) {
        $requestParams = array();
        if (!isset($request['page']) || !($requestParams['page'] = Common::filterInt($request['page'])) || $requestParams['page'] <= 0) {
            $requestParams['page'] = 1;
        }
        $requestParams['page_offset'] = isset($request['page_offset']) && $request['page_offset'] >= 1 ? $request['page_offset'] : null;
        if (!isset($request['per_page']) || !($requestParams['per_page'] = Common::filterInt($request['per_page'])) || $requestParams['per_page'] <= 0) {
            $requestParams['per_page'] = $this->container->getParameter('new_insing_api_per_page');
        }
        if(isset($request['content_type']) && $request != "") {
            if(!is_numeric($request['content_type'])) {
                throw new \Exception("Content type must be a number!", APICode::HTTP_BAD_REQUEST);
            } else {
                $requestParams['content_type'] = $request['content_type'];
            }
        }
        if(isset($request['filter']) && $request != "") {
            if($request['filter']!= "published"  && $request['filter']!= "scheduled" && $request['filter']!= "expired" ) {
                throw new \Exception("Invalid filter param! Please try again.", APICode::HTTP_BAD_REQUEST);
            } else {
                $requestParams['filter'] = $request['filter'];
            }
        }
        if (isset($request['use_cache']) && $request['use_cache'] != "" ) {
            $requestParams['use_cache'] = $request['use_cache'];
        }
        $requestParams['module']    = ContentRepository::HGW_MODULE;
        ksort($requestParams);
        return $requestParams;
    }

    private function createHgwContentData($hgwContent) {
        if(isset($hgwContent) && $hgwContent != "") {
            $dataHgw["id"] = $hgwContent['id'];
            $dataHgw["module"] = $hgwContent['module'];
            $chanel = $hgwContent['content_type'];
            $chanel_name = "";
            switch($chanel) {
                case "1":
                    $chanel_name = "Movie";
                    break;
                case "2":
                    $chanel_name = "Event";
                    break;
                case "3":
                    $chanel_name = "Restaurant";
                    break;
                case "4":
                    $chanel_name = "Restaurant Deal";
                    break;
                case "5":
                    $chanel_name = "Hungry Deal";
                    break;
                case "6":
                    $chanel_name = "Article";
                    break;
                case "7":
                    $chanel_name = "Gallery";
                    break;
                default:
                    break;
            }
            $dataHgw["content_type"]        = $chanel_name;
            $dataHgw["content_type_num"]    = $chanel;
            $dataHgw["item_id"]             = $hgwContent['item_id'];
            $dataHgw["chanel_name"]         = $hgwContent['channel_name'];
            $dataHgw["editor"]              = isset($hgwContent['editor']) ? $hgwContent['editor'] : ""; //
            $dataHgw["title"]               = $hgwContent['title'];
            $dataHgw["description"]         = $hgwContent['description'];
            $dataHgw["contentUrl"]          = isset($hgwContent['content_url'])?$hgwContent['content_url'] : "";//
            $dataHgw["imageUrl"]            = isset($hgwContent['image_url']) ? $hgwContent['image_url'] : "";//
            $dataHgw["slot"]                = $hgwContent['slot'];
            $dataHgw["publish_on"]          = $hgwContent['publish_on'];
            $dataHgw["expired_on"]          = isset($hgwContent['expire_on']) ? $hgwContent['expire_on'] : "";//
            $dataHgw["status"]              = $hgwContent['status'];
            $dataHgw["created_at"]          = $hgwContent['created_at'];
            $dataHgw["updated_at"]          = $hgwContent['updated_at'];
            $dataHgw["deal_id"]             = "";
            $dataHgw["tabledb_id"]          = "";
            $dataHgw["slug"]                = "";
            $dataHgw["series_seo_url"]      = "";
            $dataHgw["event_data"]          = "";

            if(isset($hgwContent['extra_field']) && $hgwContent['extra_field'] != "") {
                $extra_field = json_decode($hgwContent['extra_field'], true);
                $dataHgw["deal_id"]       = isset($extra_field['deal_id']) ? $extra_field['deal_id'] : "";
                $dataHgw["tabledb_id"]    = isset($extra_field['tabledb_id']) ? $extra_field['tabledb_id'] : "";
                $dataHgw["slug"]       = isset($extra_field['slug']) ? $extra_field['slug'] : "";
                $dataHgw["series_seo_url"]       = isset($extra_field['series_seo_url']) ? $extra_field['series_seo_url'] : "";
                $event_data = json_decode(isset($extra_field['event_data']) ? $extra_field['event_data'] : "",true);
                $dataHgw["event_data"] = isset($event_data) ? $event_data : "";
            }
            return $dataHgw;
        }
    }

}

<?php
namespace inSing\FrontendBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use inSing\FrontendBundle\Security\User\inSingUser;

/**
 * UserProvider class
 *
 * @author Trung Nguyen
 */
class inSingUserProvider implements UserProviderInterface
{
    /**
     * @var array
     */
    private $users;

    /**
     * Constructor.
     *
     * @param array $users An array of users
     */
    public function __construct(array $users = array())
    {
        $this->users = $users;
    }

    /**
     * Adds a new User to the provider.
     *
     * @param UserInterface $user A UserInterface instance
     *
     * @throws \LogicException
     */
    public function createUser(UserInterface $user)
    {
        if (isset($this->users[strtolower($user->getUsername())])) {
            throw new \LogicException('Another user with the same username already exist.');
        }

        $this->users[strtolower($user->getUsername())] = $user;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        if (!isset($this->users[strtolower($username)])) {
            $ex = new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
            $ex->setUsername($username);

            throw $ex;
        }


        $user = $this->users[strtolower($username)];

        return new inSingUser($user->getUsername(), $user->getPassword(), null, $user->getRoles());
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof inSingUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === 'inSing\FeaturesBundle\Security\User\inSingUser';
    }
}
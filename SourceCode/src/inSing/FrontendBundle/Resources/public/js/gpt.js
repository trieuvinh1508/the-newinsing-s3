var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
    (function(){ var gads = document.createElement('script');
       gads.async = true; gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') 
            + '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
})();





function loadFeaturesGpt($gpt_ad_zones)
{
    //Add class for ALL ads
    var rand = Math.random();
    var ad_1x1 = "<div id='ad_1x1" + rand + "' class='bnr728'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[1,1]], 'ad_1x1" + rand + "'); }); </" + "script></div>";
    $('#ad_1x1').html(ad_1x1);
    if( $(window).width() > 974 ) {
        //For DESKTOP

        //For Wallpaper
        $('#wallpaper-ads').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var ad_wallpaper_html = "<div id='wallpaper-ads" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[1600,900]], 'wallpaper-ads" + rand + "'); }); </" + "script></div>";
                $(this).html(ad_wallpaper_html);
            }
        });

        //For Leaderboard
        $('#ad_leaderboard').each(function(){

            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var leaderboard_html = "<div id='ad_leaderboard" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[728,90]], 'ad_leaderboard" + rand + "'); }); </" + "script></div>";

                $(this).html(leaderboard_html);
            }
        });

        //For MPU 1
        $('.gpt-mpu-landscape').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu_html = "<section id='ad_mpu" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,250]], 'ad_mpu" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu_html);
            }
        });

        //For MPU 2
        $('.gpt-mpu2-landscape').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu2_html = "<section id='ad_mpu2" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,251]], 'ad_mpu2" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu2_html);
            }
        });

        //For MKTG
        $('.gpt-mktg-landscape').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var ad_300_150_html = "<section id='ad_300_150" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,151]], 'ad_300_150" + rand + "'); }); </" + "script></section>";
                $(this).html(ad_300_150_html);
            }
        });

    } else if( $(window).width() >= 753 && $(window).width() <= 974 ) {
        //For TABLET

        //For Leaderboard
        $('#ad_leaderboard').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var leaderboard_html = "<div id='ad_leaderboard" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[728,90]], 'ad_leaderboard" + rand + "'); }); </" + "script></div>";
                $(this).html(leaderboard_html);
            }
        });

        //For MPU 1
        $('.gpt-mpu-portrait').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu_html = "<section id='ad_mpu" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,250]], 'ad_mpu" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu_html);
            }
        });

        //For MPU 2
        $('.gpt-mpu2-portrait').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu2_html = "<section id='ad_mpu2" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,251]], 'ad_mpu2" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu2_html);
            }
        });

        //For MKTG
        $('.gpt-mktg-portrait').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var ad_300_150_html = "<section id='ad_300_150" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,151]], 'ad_300_150" + rand + "'); }); </" + "script></section>";
                $(this).html(ad_300_150_html);
            }
        });

    } else if( $(window).width() < 753 ) {
        //For MOBILE
        //For MPU 1
        $('.gpt-mpu-mobile').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu_html = "<section id='ad_mpu" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,250]], 'ad_mpu" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu_html);
            }
        });

        //For MPU 2
        $('.gpt-mpu2-mobile').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var mpu2_html = "<section id='ad_mpu2" + rand + "'>" + "<script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,251]], 'ad_mpu2" + rand + "'); }); </" + "script></section>";
                $(this).html(mpu2_html);
            }
        });

        //For MKTG
        $('.gpt-mktg-mobile').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var ad_300_150_html = "<section id='ad_300_150" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[300,151]], 'ad_300_150" + rand + "'); }); </" + "script></section>";
                $(this).html(ad_300_150_html);
            }
        });

        //For 320x50
        $('.gpt-mobile-portrait').each(function(){
            if ( $(this).html() == '' ) {
                var rand = Math.random();
                var ad_320_50_html = "<section id='ad_320_50" + rand + "'><script type='text/javascript'>googletag.cmd.push( function() { googletag.pubads().display('"+$gpt_ad_zones+"', [[320,50]], 'ad_320_50" + rand + "'); }); </" + "script></section>";
                $(this).html(ad_320_50_html);
            }
        });
    }
}


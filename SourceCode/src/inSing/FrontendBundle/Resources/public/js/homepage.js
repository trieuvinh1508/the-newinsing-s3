/**
 * Created by root on 8/28/15.
 */
$().ready(function() {
    $('body').on('click', 'a.book_now_post', function() {
        // do something for dynamic element
        //slug, tabledbid
        var id = $('#frmBookNow_restaurant_id');
        var frm = $('#frmBookNow');
        var action = $('#frmBookNow_action').val();
        action = action.replace("{slug_restaurant_name}", $(this).data('slug'));
        frm.attr('action', action);
        id.val($(this).data('tabledb_id'));
        //console.log(frm.attr('action'));
        frm.submit();
        return false;
    });

    //default value
    var isHgwModuleCalled = false;

    $('#id_hgw_expend').on('click', function() {
        var $this = $(this);
        var $el = $('#id_hgw_expand_item');
        var link = $(this).data('url');
        var iTag = '<i class="icon icon-expand"></i>';
        if(isHgwModuleCalled == false) {
            $.ajax({
                method: "POST",
                url: link,
                dataType: "json",
                beforeSend: function() {
                    $el.html(iTag + $el.data('textloading'));
                },
                success: function(res) {
                    if(res.status == 200 && res.data) {
                        $('#id-hgw-module').append(res.data);
                        isHgwModuleCalled = true;
                        //fixed for ... on title
                        //main.checkellipsis('.module-grid-list .item');
                        main.checkellipsis('.module-grid-list.hgw-deal .item');
                        main.toggle_grid_list($this);
                    }
                    $el.html(iTag + $el.attr('title'));
                }
            });
        } else {
            main.toggle_grid_list($this);
        }

    });

    // Handle event expand
    var event_booted = false;

    $("#id-event-expand").on('click', function() {

        var $this = $(this);

        if (event_booted == false)
        {
            var $el = $('#id-event-expand-span');
            var iTag = '<i class="icon icon-expand"></i>';

            $.ajax({
                method: "POST",
                url: $this.data('url'),
                dataType: 'json',
                beforeSend: function() {
                    $el.html(iTag + $el.data('textloading'));
                },
            })
                .done(function( response ) {
                    if(response.status == 200 && response.data) {
                        for (i = 0; i < 6; i++)
                        {
                            if (response.data[i])
                            {
                                var event = response.data[i];
                                var eventTitle = htmlEntities(event['title']);
                                $('#id-event-module').append('<li id="id-event-' + i + '-' + event['id'] + '" class="item">' +
                                    '<figure class="align-focus">' +
                                    '<a href="' + event['item_url'] + '" title="' + eventTitle + '">' +
                                    '<img src="' + event['imageUrl'] + '" alt="' + eventTitle + '">' +
                                    '</a>' +
                                    '</figure>' +
                                    '<div class="entry">' +
                                    '<h4 class="title-wrap rs-regular-s">' +
                                    '<a href="' + event['item_url'] + '" title="' + eventTitle + '">' + eventTitle + '</a>' +
                                    '</h4>' +
                                    '<div class="desc">' + event['summary'] + '</div>' +
                                    '</div>' +
                                    '</li>'
                                );

                                if (typeof event !== 'undefined'
                                    && event['type'] == 'Event'
                                    && typeof event['event_data'] !== 'undefined'
                                    && typeof event['event_data'][0] !== 'undefined')
                                {
                                    if (eventIsFree(event['event_data'][0]))
                                    {
                                        $('#id-event-' + i + '-'  + event['id']).append('<a title="FREE" href="javascript:void(0);" rel="nofollow" onclick="this.href=\'' + event['item_url'] + '\'" class="link free">FREE</a>');
                                    }
                                    else
                                    {
                                        if (eventIsBuy(event['event_data'][0]))
                                        {
                                            $('#id-event-' + i + '-'  + event['id']).append('<a title="BUY TICKETS" href="javascript:void(0);" rel="nofollow" onclick="this.href=\'' + event['item_url'] + '\'" class="link">BUY TICKETS</a>');
                                        }
                                        else/* if (eventIsView(event['event_data'][0]))*/
                                        {
                                            $('#id-event-' + i + '-'  + event['id']).append('<a title="VIEW" href="javascript:void(0);" rel="nofollow" onclick="this.href=\'' + event['item_url'] + '\'" class="link free">VIEW</a>');
                                        }
                                    }
                                }

                                //main.checkellipsis('#id-event-' + event['id']);
                            }
                        }

                        event_booted = true;
                        $el.html(iTag + $el.attr('title'));
                        main.checkellipsis('.module-grid-list.event-highlight .item');
                        main.toggle_grid_list($this);

                    }
                });
        }
        else
        {
            main.toggle_grid_list($this);
        }
    });

    eventIsFree = function (event) {
        if (typeof event['price_min'] !== 'undefined'
            && typeof event['price_max'] !== 'undefined'
            && event['price_min'] != "" && event['price_max'] != ""
            && event['price_min'] == 0 && event['price_max'] == 0
        )
        {
            return true;
        }

        return false;
    };

    eventIsBuy = function (event) {
        var today = new Date();
        var end_date = new Date(event['end_date']);

        if (eventIsFree(event) === false
            && typeof event['vendor_url'] !== 'undefined' && event['vendor_url'] != ""
            && ((typeof event['end_date'] !== 'undefined'  && (today <= end_date))
                ||(typeof event['on_going'] !== 'undefined' && event['on_going'] == 1))
        )
        {
            return true;
        }

        return false;
    };

    /*eventIsView = function (event) {
        if (eventIsFree(event) === false
            && typeof event['on_going'] !== 'undefined'
            && event['on_going'] == 1
        )
        {
            return true;
        }

        return false;
    };*/

    // Handle movie expand
    var movie_booted = false;

    $("#id-movie-expand").on('click', function() {

        var $this = $(this);

        if (movie_booted == false)
        {
            var $el = $('#id-movie-expand-span');
            var iTag = '<i class="icon icon-expand"></i>';

            $.ajax({
                method: "POST",
                url: $this.data('url'),
                dataType: 'json',
                beforeSend: function() {
                    $el.html(iTag + $el.data('textloading'));
                },
            })
                .done(function( response ) {
                    if(response.status == 200 && response.data) {
                        for (i = 0; i < 6; i++)
                        {
                            if (response.data[i])
                            {
                                var movie = response.data[i];
                                var movieTitle = htmlEntities(movie['title']);
                                $('#id-movie-module').append('<li id="id-movie-' + i + '-' + movie['id'] + '" class="item">' +
                                    '<figure class="align-focus">' +
                                    '<a href="' + movie['item_url'] + '" title="' + movieTitle + '">' +
                                    '<img src="' + movie['default_image_url'] + '" alt="' + movieTitle + '">' +
                                    '</a>' +
                                    '</figure>' +
                                    '<div class="entry">' +
                                    '<h4 class="title-wrap rs-regular-s">' +
                                    '<a href="' + movie['item_url'] + '" title="' + movieTitle + '">' + movieTitle + '</a>' +
                                    '</h4>' +
                                    '<div class="desc">' + movie['summary'] + '</div>' +
                                    '</div>' +
                                    '</li>'
                                );
                            }

                            if (movie['showtime_url'])
                            {
                                $('#id-movie-' + i + '-'  + movie['id']).append('<a title="SHOWTIMES" href="javascript:void(0);" rel="nofollow" onclick="this.href=\'' + movie['showtime_url'] + '\'" class="link">SHOWTIMES</a>');
                            }

                            //main.checkellipsis('#id-movie-' + movie['id']);
                        }

                        movie_booted = true;
                        $el.html(iTag + $el.attr('title'));
                        main.checkellipsis('.module-grid-list.top-movie .item');
                        main.toggle_grid_list($this);
                    }
                });
        }
        else
        {
            main.toggle_grid_list($this);
        }
    });

    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }

    //st add event auto expanded
    var dataNewInSingHomeCookie = {
        HGW_COOKIE_NAME: "new_insing_is_expanded_hgw",
        MOVIE_COOKIE_NAME: "new_insing_is_expanded_movie",
        EVENT_COOKIE_NAME: "new_insing_is_expanded_event",
        EXPIRE_DAY_COOKIE: 7300, // 20*365 -> 20 years
        EXPANDED_VALUE: 'on',
        COLLAPSE_VALUE: 'off'
    };

    // Handle HGW Cookies
    $('#id_hgw_expand_item').on('click', function() {
        $.cookie(dataNewInSingHomeCookie.HGW_COOKIE_NAME,
            dataNewInSingHomeCookie.EXPANDED_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    });
    $('#id_hgw_callapse_item').on('click', function() {
        $.cookie(dataNewInSingHomeCookie.HGW_COOKIE_NAME,
            dataNewInSingHomeCookie.COLLAPSE_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    });

    if($.cookie(dataNewInSingHomeCookie.HGW_COOKIE_NAME) == dataNewInSingHomeCookie.EXPANDED_VALUE) {
        $('#id_hgw_expend').trigger( "click");
    }

    // Handle EVENT Cookies
    $("#id-event-expand-span").on('click', function() {
        $.cookie(dataNewInSingHomeCookie.EVENT_COOKIE_NAME,
            dataNewInSingHomeCookie.EXPANDED_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    })

    $("#id-event-collapse-span").on('click', function() {
        $.cookie(dataNewInSingHomeCookie.EVENT_COOKIE_NAME,
            dataNewInSingHomeCookie.COLLAPSE_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    })

    if($.cookie(dataNewInSingHomeCookie.EVENT_COOKIE_NAME) == dataNewInSingHomeCookie.EXPANDED_VALUE) {
        $('#id-event-expand').trigger( "click");
    }

    // Handle MOVIE Cookies
    $("#id-movie-expand-span").on('click', function() {
        $.cookie(dataNewInSingHomeCookie.MOVIE_COOKIE_NAME,
            dataNewInSingHomeCookie.EXPANDED_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    })

    $("#id-movie-collapse-span").on('click', function() {
        $.cookie(dataNewInSingHomeCookie.MOVIE_COOKIE_NAME,
            dataNewInSingHomeCookie.COLLAPSE_VALUE,
            { expires: dataNewInSingHomeCookie.EXPIRE_DAY_COOKIE });
    })

    if($.cookie(dataNewInSingHomeCookie.MOVIE_COOKIE_NAME) == undefined || $.cookie(dataNewInSingHomeCookie.MOVIE_COOKIE_NAME) == dataNewInSingHomeCookie.EXPANDED_VALUE) {
        $('#id-movie-expand').trigger( "click");
    }
    //ed add event auto expanded
});


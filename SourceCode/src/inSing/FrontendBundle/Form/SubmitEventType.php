<?php
namespace inSing\FrontendBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubmitEventType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('fullName', 'text', array('required' => true))
            ->add('companyName', 'text', array('required' => false))
            ->add('emailAddress', 'email', array('required' => true))
            ->add('primaryContactNo', 'number', array('required' => true))
            ->add('title', 'text', array('required' => true))
            ->add('startDate', 'text', array('required' => true))
            ->add('endDate', 'text', array('required' => true))
            ->add('time', 'text', array('required' => false))
            ->add('venueName', 'text', array('required' => true))
            ->add('unitNo', 'number', array('required' => true))
            ->add('buildingName', 'text', array('required' => true))
            ->add('blockNo', 'number', array('required' => true))
            ->add('streetName', 'text', array('required' => true))
            ->add('postalCode', 'text', array('required' => true))
            ->add('venuePhoneNumber', 'text', array('required' => false))
            ->add('adultRating', 'checkbox', array('required' => false))
            ->add('eventSummary', 'textarea', array('required' => true))
            ->add('facebookEventIds', 'text', array('required' => false))
            ->add('eventUrl', 'url', array('required' => false))

            ->add('priceMin', 'hidden')
            ->add('priceMax', 'hidden')
            ->add('ticketUrl', 'url', array('required' => false))
            ->add('ticketVendorName', 'text', array('required' => false))
            // ->add('ticketVendorImageUrl', 'url', array('required' => false, 'label' => 'Ticket Vendor Image URL :',
            //     'attr' => array('disabled' => true, 'class' => 'ticket-vendor-details')
            // ))
            //multiple
                //autocomplete
            ->add('images', 'file', array('required' => false))

            ->add('subcategories', 'hidden', array('required' => true))
            ->add('termOfUse', 'checkbox', array('required' => true))
        ;

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array());
    }

    public function getName() {
        return 'submit_event';
    }

}
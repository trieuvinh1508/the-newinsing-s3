<?php
namespace inSing\FrontendBundle\Controller;

use inSing\FrontendBundle\Entity\SubmitEvent;
use inSing\FrontendBundle\Form\SubmitEventType;
use inSing\FrontendBundle\Utils\Helper;
use inSing\FrontendBundle\Utils\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class EventController extends Controller
{
     /**
     * @param Request $request
     * @return Response
     * author: Cuong.au
     */

    public function autoCompleteAction(Request $request)
    {
        // Get request parram
        if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
            return die('No direct access allowed.');
        }
        $str = $request->query->get('term');
        $result = "";

        if(empty($str))
        {
            //throw new NotFoundHttpException('Page Not Found');
        }
        // get service
        $events_api = $this->get('events_api');
        // Call apdapter an return String

//        echo json_encode($result);
        $params = array('search_term' => $str);
        $result = $events_api->autoComplete($params);
        $data = array();
        if(isset($result['status']) && $result['status']=="200"){
            if(isset($result['data']) && is_array($result['data'])){
                    foreach($result['data'] as $item){
                            $data[]=$item['name'];
                        }
           }
       }
        echo json_encode($data);
        exit();
//        return new Response(var_dump($result));
    }

    /**
     * @author Dat.Dao
     * @param $submitEvent
     * @return \Symfony\Component\Form\Form
     */
    private function createSubmitForm($submitEvent)
    {
        $form = $this->createForm(new SubmitEventType(), $submitEvent);
        return $form;
    }

    /**
     * @author Dat.Dao
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function submitAction(Request $request)
    {
        //st generate breadscrumb
        $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
        $breadcrumbs->addItem("Events", $this->generateUrl('event_finder'));
        $breadcrumbs->addItem('Submit');
        //ed generate breadscrumb

        $apiCaller = $this->get('events_api');
        $categories = $apiCaller->getAllCategories();

        $submitEvent = new SubmitEvent();
        $form = $this->createSubmitForm($submitEvent);
        $_logger = $this->get('monolog.logger.submit_event_frontend');
        $errorMessage = '';
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            $errorMessage = self::bindSubcategories($request, $submitEvent, $form);
            if(! empty($errorMessage)) {
                $_logger->debug("invalid input: " . $errorMessage);
                return $this->render('inSingFrontendBundle:Event:submit.html.twig',
                    array('form' => $form->createView()
                    , 'errorMessage' => $errorMessage
                    , 'categories' => $categories));
            }
            $errorMessage = self::validateAttachments($form, $submitEvent);
            if(! empty($errorMessage)) {
                $_logger->debug("invalid input: " . $errorMessage);
                return $this->render('inSingFrontendBundle:Event:submit.html.twig',
                    array('form' => $form->createView()
                    , 'errorMessage' => $errorMessage
                    , 'categories' => $categories));
            }
            //bind maxPrice, MinPrice
            $submitEvent->setPriceMin($request->request->get('multiplePriceMin'));
            $submitEvent->setPriceMax($request->request->get('multiplePriceMax'));
            if ($form->isValid()) {
                /**
                 * current logic
                    1.create categories(if have any photo)
                    api: create-gallery
                    api: create-gallery-photo
                    2.save event draft
                    api: 2.0/admin/events
                    ok: send email to user
                    not good: show error
                 */
                $this->createGallery($submitEvent);
                $result = $this->saveEventAsDraft($submitEvent);
                if (isset($result['id'])) {
                    try {
                        //---------------- Compose mail for the submitter -------------------
                        $inputMessage = array();
                        $inputMessage['subject'] = $this->container->getParameter("submit_event_mail_subject");
                        $inputMessage['from'] = array($this->container->getParameter("submit_event_mail_sender") => "inSing");
                        $inputMessage['to'] = array($form->get('emailAddress')->getData() => $form->get('fullName')->getData());
                        $inputMessage['body'] = $this->renderView("inSingFrontendBundle:Event:_submitter_mail_template.txt.twig");

                        $message = \Swift_Message::newInstance()
                            ->setSubject($inputMessage['subject'])
                            ->setFrom($inputMessage['from'])
                            ->setTo($inputMessage['to'])
                            ->setBody($inputMessage['body'], "text/plain");

                        $this->get("mailer")->send($message);

                        $_logger->debug("Success send message: " . json_encode($inputMessage));
                    } catch (\Exception $e) {
                        $_logger->debug("Failed send message to submitter exception: " . $e->getMessage());
                    }
                    // redirect to thank-you page
                    return $this->redirect($this->generateUrl('insing_frontend_events_submit_sucess'));
                } else {
                    $_logger->debug("create event error: " . $result['error']);
                    //$form->addError(new FormError($result['error']));
                    $errorMessage = $result['error'];
                }
            }
        }

        return $this->render('inSingFrontendBundle:Event:submit.html.twig',
            array('form' => $form->createView()
            , 'errorMessage' => $errorMessage
            , 'categories' => $categories));
    }

    /**
     * @author Dat.Dao
     * @param Request $request
     */
    public function successAction(Request $request) {
        //st generate breadscrumb
        $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
        $breadcrumbs->addItem("Events", $this->generateUrl('event_finder'));
        $breadcrumbs->addItem('Submit');
        //ed generate breadscrumb

        return $this->render('inSingFrontendBundle:Event:submitthank.html.twig');
    }

    /**
     * @author Dat.Dao
     * @param $request
     * @param SubmitEvent $event
     * @param $form
     * @return string
     */
    private function bindSubcategories($request, SubmitEvent $event, $form)
    {
        $subcats = $request->get("subcategories");
        if (empty($subcats)) {
            //$form->addError(new FormError("Select at least one category."));
            return "Select at least one category.";
        } else {
            $event->setSubcategories($subcats);
        }
        return '';
    }

    /**
     * @author Dat.Dao
     * @param $form
     * @param SubmitEvent $event
     * @return string
     */
    private function validateAttachments($form, SubmitEvent $event)
    {
        $imagesArray = is_array($form["images"]->getData()) ? $form["images"]->getData() : array($form["images"]->getData());
        if (empty($imagesArray)) {
            //$form->addError(new FormError("Add at least one photo."));
            return "Add at least one photo.";
        } else {
            //---------------Photos Validations------------------
            $validPhotos = array();
            foreach ($imagesArray as $photo) {
                if (is_a($photo, 'Symfony\Component\HttpFoundation\File\UploadedFile')) {
                    if($photo->isValid() && !$photo->getError() && preg_match('/image\//', $photo->getClientMimeType())) {
                        $validPhotos[] = $photo;
                    }
                }
            }
            $event->setImages($validPhotos);
        }
        return '';
    }

    /**
     * @author Dat.Dao
     * @param SubmitEvent $event
     */
    private function createGallery(SubmitEvent $event)
    {
        $_logger = $this->get('monolog.logger.submit_event_frontend');
        if (count($event->getImages())) {
            $apiCaller = $this->get('cms_api');
            $galleryParams['status'] = 0; //DRAFT
            $galleryParams['gallery_type'] = $this->container->getParameter('cms_gallery_type_photo');
            $galleryParams['region'] = $this->container->getParameter('cms_region_asia');
            $galleryParams['country'] = $this->container->getParameter('cms_country_singapore');
            $galleryParams['city'] = $this->container->getParameter('cms_city_singapore');
            $galleryParams['channel'] = $this->container->getParameter('cms_channel_events_id');
            $galleryParams['category'] = $this->container->getParameter('cms_category_events_id');
            $galleryParams['content_series'] = $this->container->getParameter('cms_series_event_photos');
            $galleryParams['title'] = $event->getTitle() . " - Photos";
            $galleryParams['summary'] = $event->getEventSummary();
            try {
                $galleryId = $apiCaller->createGallery($galleryParams);
                if( $galleryId > 0 ) {
                    $_logger->debug("Success created new galleryId[$galleryId] for event '" . $event->getTitle(). "'");
                } else {
                    $_logger->debug("Failed creating new galleryId[$galleryId] for event '" . $event->getTitle(). "'");
                }

                //update galleryId
                $event->setGalleryId($galleryId);
                foreach ($event->getImages() as $imageFile) {
                    if(is_object($imageFile)) {
                        $photoParams['gallery_id'] = $galleryId;
                        $photoParams['caption'] = $event->getTitle();
                        $photoParams['credit'] = $event->getTitle();
                        $imageFileBinary = fread(fopen($imageFile->getPathname(), 'r'), filesize($imageFile->getPathname()));
                        $photoParams['image_content'] = base64_encode($imageFileBinary);
                        $file = explode(".", $imageFile->getClientOriginalName());
                        $fileExt = array_pop($file);
                        $filename = Helper::cleanUrl(implode("-", $file)) . ".$fileExt";
                        $filename = (strlen($filename) > 30) ? substr($filename, strlen($filename) - 30) : $filename;
                        $photoParams['image_filename'] = $filename;
                        $this->createMediaAndLinkToGallery($photoParams);
                    }
                }
            } catch (\Exception $e) {
                $_logger->debug("Unable to create gallery for event '" .
                    $event->getTitle(). "': " . $e->getMessage());
            }
        }
    }

    /**
     * @author Dat.Dao
     * @param $photoParams
     */
    private function createMediaAndLinkToGallery($photoParams) {
        $_logger = $this->get('monolog.logger.submit_event_frontend');
        $apiCaller = $this->get('cms_api');
        try {
            $result = $apiCaller->createGalleryPhoto($photoParams);
            $_logger->debug("Success created media: " . json_encode($result));
        } catch (\Exception $e) {
            $_logger->debug("Unable to create media: " . $e->getMessage());
        }
    }

    /**
     * @author Dat.Dao
     * @param SubmitEvent $submittedEvent
     * @return array
     */
    private function saveEventAsDraft(SubmitEvent $submittedEvent) {
        $_logger = $this->get('monolog.logger.submit_event_frontend');
        $result = array();
        $eventsApi = $this->get('events_api');
        try {
            $id = $eventsApi->createEvent($submittedEvent->toApiParams());
            if ($id > 0) {
                $_logger->debug("User submitted eventId:[$id] successfully saved as draft.");
            } else {
                $_logger->debug("User submitted eventId:[$id] failed as draft.");
            }
            $result['id'] = $id;
        } catch (\Exception $e) {
            $_logger->debug("Unable save user submitted event: " . $e->getMessage());
            $result['error'] = "Error in saving event: " . $e->getMessage();
        }

        return $result;
    }

    /**
     * @author Dat.Dao
     * @param Request $request
     * @return Response
     */
    public function _renderHotRestauRantDealBlockAction(Request $request, $limit = 3) {
        $data = $this->_prepareDataForHotRestaurantDeal($limit);
        //print_r($data);die;
        return $this->render('inSingFrontendBundle:Event:_hotrestaurantdeal.html.twig', array('data' => $data));
    }

    /**
     * @author Dat.Dao
     * @return array
     */
    private function _prepareDataForHotRestaurantDeal($limit) {
        $rawData = $this->_getDataSearchPromotion($limit);
        if( empty($rawData)) {
            return null;
        }
        $output = array();
        $getRestaurantItemBy = function ($item, $fieldName, $root = 'restaurant') {
            if (!empty($item[$root][$fieldName])) {
                return $item[$root][$fieldName];
            }
            return '';
        };
        //{#item.title, item.description, item.photo, item.bookNowUrl#}
        foreach($rawData as $item) {
            $dealId = $getRestaurantItemBy($item, 'id', 'promotion');
            $restaurantSlug = $getRestaurantItemBy($item, 'restaurantSlug');
            //$=>COMFIRMED(CLEAR): the first item of restaurant.images
            //CONFIRM WITH QC: itemDeal->promotion.promotionImage
            $promotionImage = $getRestaurantItemBy($item, 'promotionImage', 'promotion');
            $imageUrl = ( !empty($promotionImage ) ) ? $promotionImage : '';
            $bookNowUrl = $this->container->get('insing.hellper')->generateUrlDealDetail($restaurantSlug, $dealId);
            $output[] = array(
                'title' => $getRestaurantItemBy($item, 'title', 'promotion'),
                'description' => $getRestaurantItemBy($item, 'description', 'promotion'),
                'photo' => $imageUrl,
                'bookNowUrl' => $bookNowUrl,
            );
        }
        return $output;
    }

    /**
     * @author Dat.Dao
     * @param int $limit
     * @return null
     */
    private function _getDataSearchPromotion($limit)
    {
        $cache = $this->get('new.insing.cache');
        //http://sg.tabledb.com/tabledb-web/promotion/search/0/0?perPage=5&displayInHGW=true&partnerCode[]=hgw&countryCode[]=SG&featuredDeal=1&sortField=createdDate&order=DESC
        $limit = ($limit > 20) ? 20: $limit;
        $params = array('perPage' => $limit,
            'displayInHGW' => 'true',
            'partnerCode[]' => 'hgw',
            'countryCode[]' => 'SG',
            'featuredDeal' => 1,
            'sortField' => 'featuredDeal',
            'order' => 'DESC'
        );
        $apiCaller = $this->container->get('tabledb_deals_api');
        $caheKey = 'TABLE_DB_SEARCH_PROMOTION_CACHE_KEY' . "_limit{$limit}";
        $caheTime = $this->container->getParameter('new_insing_tabledb_cache_time');

        $result = $cache->getCache($caheKey);
        if( empty($result) ) {
            $result = $apiCaller->searchPromotion($params);
            if (!empty($result['response']['data'])) {
                //set data into cache
                $cache->setCache($result['response']['data'], $caheKey, $caheTime, true);
                return $result['response']['data'];
            } else {
                //call api failed
                return null;
            }
        }
        //from cache
        return $result;
    }


    public function searchAction()
    {
        $events_api = $this->get('events_api');
        $result = $events_api->search();

        return new Response(var_dump($result));
    }

    public function detailsAction(Request $request)
    {
        $reversedHexId = $request->get("reversedHexId");
        $id = Utils::getId($reversedHexId);
        //call event details API
        $events = $this->get('events_api')->getDetails($id);
        //event previous
        $eventP = null;
        //event next
        $eventN = null;

        $events_details_data = $events['data'][0];
        $searchParams =  array();
        $uriParams = array();
        $searchTerm = urldecode($this->getRequest()->get('q'));
        if (isset($searchTerm) && Utils::isNotBlankString($searchTerm)) {
            $uriParams['q'] = $searchTerm;
            $searchParams['filter_search_term'] = $searchTerm;
        }

        // subcategories
        $subcategories = $this->getRequest()->get('subcategories');
        if (isset($subcategories) && count($subcategories) > 0) {
            $uriParams['subcategories'] = $subcategories;
            $subcatIds = array();
            foreach ($subcategories as $id => $value) {
                if ($value == 1) {
                    $subcatIds[] = $id;
                }
            }
            if (count($subcatIds) > 0) {
                sort($subcatIds);
                $searchParams['filter_subcategory'] = implode(',', $subcatIds);
            }
        }
        // date filter
        $start_date = $this->getRequest()->get('startDate');
        if (isset($start_date)) {
            $uriParams['startDate'] = $start_date;
            $searchParams['filter_start_date'] = Utils::dateChecker($start_date);
        }
        $end_date = $this->getRequest()->get('endDate');
        if (isset($end_date)) {
            $uriParams['endDate'] = $end_date;
            $searchParams['filter_end_date'] = Utils::dateChecker($end_date);
        }

        // add tags filter
        if ($this->getRequest()->get('tags')) {
            $tags = $this->getRequest()->get('tags');
            $uriParams['tags'] = $tags;
            $tags = explode(',', $tags);
            if (in_array('free', $tags)) {
                $searchParams['filter_free'] = 'y';
            }
        }

        // Sorting options
        $ordering = $request->get("search_event");
        if (isset($ordering) && isset($ordering['order'])) {
            $uriParams['search_event'] = $ordering;
            switch ($ordering['order']) {
                case 'nameA':
                    $searchParams['sort_field'] = 'event_name';
                    $searchParams['sort_reverse'] = 'n';
                    break;
                case 'nameZ':
                    $searchParams['sort_field'] = 'event_name';
                    $searchParams['sort_reverse'] = 'y';
                    break;
                case 'earliest':
                    $searchParams['sort_field'] = 'start_date';
                    $searchParams['sort_reverse'] = 'n';
                    break;
                case 'latest':
                    $searchParams['sort_field'] = 'start_date';
                    $searchParams['sort_reverse'] = 'y';
                    break;
            }
        }

        $uri = explode("?", $request->getUri());
        if (count($uriParams) == 0 && !isset($uri[1])) {
            $subcatId = $this->getFirstSubcategory($events);
            $uriParams['subcategories'] = array($subcatId => "1");
            $searchParams['filter_subcategory'] = $subcatId;
        }

        // build base parameter string for prev/next links
        if(!isset($uriParams['q'])) {
            $uriParams['q'] = '';
        }
        $uriParamsString = '?' . http_build_query($uriParams);

        $searchParams['get_prev_next_of'] = $events_details_data['id'];

        $eventResult = $this->get('events_api')->search($searchParams);
        //print_r($searchParams);die;
        if (isset($eventResult) && isset($eventResult['data'])) {
            $eventResult = $eventResult['data'];
            if (isset($eventResult['prev'])) {
                $eventP = $eventResult['prev'];
            }
            if (isset($eventResult['next'])) {
                $eventN = $eventResult['next'];
            }
        }

        $categories = $this->get('events_api')->getAllCategories();


        if($events_details_data && $events_details_data['id']) {
            $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
            $breadcrumbs->addItem("Events");
            $event_url = $this->container->getParameter("event_channel_base");
            $events_details_data['event_url'] = isset($event_url) ? $event_url : "";

            //set empty field of api
            if(empty($events_details_data['end_date'])) {
                $events_details_data['end_date'] = null;
            }
            if(empty($events_details_data['start_date'])) {
                $events_details_data['start_date'] = null;
            }

            //generate breadscrumb
            if(!empty($events_details_data['categories'])) {
                $breadcrumbs->addItem($events_details_data['categories'][0]['name'], 'abc');
            }

            if(!empty($events_details_data['name'])){
                $breadcrumbs->addItem($events_details_data['name']);
            }

            $category_details =  $this->get('events_api')->getCategory($events_details_data['categories'][0]['id']);
            //get you might like events
            $searchParamsMightLike = array();
            $searchParamsMightLike['filter_category'] = "Y";
            $searchParamsMightLike['sort_feld'] = "start_date";
            $searchParamsMightLike['sort_reverse'] = "Y";
            $searchParamsMightLike['flter_exclude_ids'] = $events_details_data['id'];
            $searchParamsMightLike['per_page'] = 4;
            $eventMightLike = $this->get('events_api')->search($searchParamsMightLike);
            $resultEventMightLike = $eventMightLike ['data'];
            $data = array(
                'event_data' => $events_details_data,
                'uriParams' => $uriParamsString,
                'eventPrevious' => $eventP,
                'eventNext' => $eventN,
                'eventMightLike' => $resultEventMightLike,
                'category_url' => $category_details['data']['url_fragment']
            );

            return $this->render('inSingFrontendBundle:Event:_details.html.twig', $data);

        } else {
            throw new NotFoundHttpException();
        }
    }

    public function topthingstodoAction()
    {
        $events_api = $this->get('events_api');
        $result = $events_api->getTopThingsToDo(array('use_cache' => 'N'));

        return new Response(var_dump($result));
    }

    public function supermoduleAction()
    {
        $events_api = $this->get('events_api');
        $result = $events_api->getSupermodule(array('use_cache' => 'N'));

        return new Response(var_dump($result));
    }

    private function getFirstSubcategory($event) {
        $categories = $event['data'][0]['categories'];
        if (isset($categories) && count($categories) > 0) {
            foreach ($categories as $category) {
                $subcategories = $category['subcategories'];
                if (isset($subcategories) && count($subcategories) > 0) {
                    foreach ($subcategories as $subcategory) {
                        return $subcategory['id'];
                    }
                }
            }
        }
    }

}

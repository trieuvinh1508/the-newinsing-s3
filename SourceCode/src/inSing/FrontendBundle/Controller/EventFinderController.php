<?php

namespace inSing\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventFinderController extends Controller
{
    /*
    * @author Vu Luu
    */
    public function indexAction(Request $request)
    {
        $breadcrumbs = $this->get('insing.util.helper')->getBreadCrumb();
        $breadcrumbs->addItem("Singapore Events");

        $currentMonth = date('Y-m-01');
        //showing events list with 2 months
        $results = $this->loadEventsList($currentMonth, 3, 2, 1);
        //echo json_encode($results); exit;

        if (!empty($results[0])){
            $results = $results[0];
        }

        return $this->render('inSingFrontendBundle:EventFinder:index.html.twig', array(
            'results' => $results
        ));
    }

    /*
     * @author Vu Luu
     */
    public function ajaxLoadAction(Request $request)
    {
        $currentMonth = date('Y-m-01');
        $callTime = $request->get('call-time',1);

        $results = $this->loadEventsList($currentMonth,3, 2, 3);

        $output_html = '';
        foreach($results[$callTime] as $key=>$data){
            $output_html .= $this->renderView('inSingFrontendBundle:EventFinder:_event_list_result.html.twig', array(
                'event_list' => $data,
                'key' => $key
            ));
        }

        return new Response($output_html);
    }

    /*
     * @author Vu.Luu
     * load events in each month
     * $currentMonth: start of month. Ex: 2015-09-01
     * $contentNum: number of content each month
     * $times: number of month
     * $counter: number of counter
     * @output: array with number $times month from first day of month to end day of month
     */
    public function loadEventsList($currentMonth, $contentNum = 3, $times = 2, $counter = 3)
    {
        $cache = $this->get('new.insing.cache');
        $key = $this->container->getParameter('new_insing_event_finder_cache_key'). '_' . md5($currentMonth.$counter);
        $lifetime = $this->container->getParameter('new_insing_event_finder_cache_time');
        $events_api = $this->get('events_api');
        $results = array();

        $mCounter = 0;
        $showContent = array();

        if ($cache->checkCache($key)) {
            $results =  $cache->getCache($key);
        } else {
            for ($c = 0; $c < $counter; $c++) {
                $result = array();
                for ($i = 0; $i < $times; $i++) {
                    $startOfMonth = date("Y-m-01", strtotime("+$mCounter month", strtotime($currentMonth)));
                    $endOfMonth = date("Y-m-t", strtotime($startOfMonth));

                    $params = array(
                        'filter_start_date' => $startOfMonth,
                        'filter_end_date' => $endOfMonth,
                        'per_page' => $contentNum,
                        'sort_field' => 'updated_date',
                        'sort_reverse' => 'Y'
                    );

                    //exclude ids for showed event list for not duplicate content
                    $params['filter_exclude_ids'] = implode(',', $showContent);

                    $events = $events_api->search($params);
                    if (!empty($events['data'])) {
                        $data = array();
                        foreach ($events['data'] as $event) {
                            $data[] = $event;
                            array_push($showContent, $event['id']);

                        }
                        $result[date('M Y', strtotime($startOfMonth))] = $data;
                    }
                    $mCounter++;
                }
                $results[] = $result;
            }

            //save event list result
            $cache->setCache($results, $key,$lifetime);

        }
        return $results;
    }

    /**
     * @author Khai Vu
     *
     * @param Request $request
     * @return Response
     */
    public function resultAction(Request $request)
    {
        $events_api = $this->get('events_api');

        $arrApiParams = array(
            'use_cache' => 'N',
            'sort_field' => 'start_date',
            'page' => 1,
            'per_page' => 10
        );

        // It is for re-fill search box after search
        $searchBoxParams = array();

        $extra = $request->get('extra');
        $arrExtra = explode("/", $extra);

        $arrParams = $request->query->all();

        if (count($arrExtra) == 1) // extra is an category
        {
            $arrCategories = array();

            if (!empty($arrParams['category']))
            {
                $arrCategories = explode(',', $arrParams['category']);
            }

            $arrCategories[] = $arrExtra[0];

            $strCategoryIds = $this->_getCategoryIdsFromNames($arrCategories);

            $searchBoxParams['category'] = $strCategoryIds;

            $arrApiParams['filter_category'] = $strCategoryIds;

            if (in_array('free-events', $arrCategories))
            {
                $arrApiParams['filter_free'] = 'Y';

                if (!empty($searchBoxParams['category']))
                {
                    $searchBoxParams['category'] .= ',0';
                }
                else
                {
                    $searchBoxParams['category'] .= '0';
                }
            }
        }
        else // extra is a date or a search keyword
        {
            if ($arrExtra[0] == 'd') // extra is a date
            {
                // Handle start date and end date
                $arrDate = explode("-", $arrExtra[1]);

                if (!empty($arrDate[0]))
                {
                    $urlStartDate = $arrDate[0] . '-' . $arrDate[1] . '-' . $arrDate[2];
                    $searchBoxParams['from_date'] = $urlStartDate;
                    $startDate = $this->_convertDate($urlStartDate);

                    $arrApiParams['filter_start_date'] = $startDate;

                    if (!empty($arrDate[3]))
                    {
                        $urlEndDate = $arrDate[3] . '-' . $arrDate[4] . '-' . $arrDate[5];
                        $searchBoxParams['to_date'] = $urlEndDate;
                        $endDate = $this->_convertDate($urlEndDate);

                        $arrApiParams['filter_end_date'] = $endDate;
                    }
                }

                // Handle category
                if (!empty($arrParams['category']))
                {
                    $arrCategories = explode(',', $arrParams['category']);
                    //$searchBoxParams['category'] = $arrCategories;
                    $strCategoryIds = $this->_getCategoryIdsFromNames($arrCategories);
                    $searchBoxParams['category'] = $strCategoryIds;
                    $arrApiParams['filter_category'] = $strCategoryIds;

                    if (in_array('free-events', $arrCategories))
                    {
                        $arrApiParams['filter_free'] = 'Y';

                        if (!empty($searchBoxParams['category']))
                        {
                            $searchBoxParams['category'] .= ',0';
                        }
                        else
                        {
                            $searchBoxParams['category'] .= '0';
                        }
                    }
                }
            }
            else // extra is a search keyword ($extra[0] == 's')
            {
                // Handle search term
                $arrApiParams['filter_search_term'] = $arrExtra[1];

                $searchBoxParams['term'] = $arrExtra[1];

                // Handle start date and end date
                $arrParams = $request->query->all();

                if (!empty($arrParams['start']))
                {
                    $searchBoxParams['from_date'] = $arrParams['start'];
                    $startDate = $this->_convertDate($arrParams['start']);
                    $arrApiParams['filter_start_date'] = $startDate;

                    if (!empty($arrParams['end']))
                    {
                        $searchBoxParams['to_date'] = $arrParams['end'];
                        $endDate = $this->_convertDate($arrParams['end']);
                        $arrApiParams['filter_end_date'] = $endDate;
                    }
                }

                // Handle category
                if (!empty($arrParams['category']))
                {
                    $arrCategories = explode(',', $arrParams['category']);
                    //$searchBoxParams['category'] = $arrCategories;
                    $strCategoryIds = $this->_getCategoryIdsFromNames($arrCategories);
                    $searchBoxParams['category'] = $strCategoryIds;
                    $arrApiParams['filter_category'] = $strCategoryIds;

                    if (in_array('free-events', $arrCategories))
                    {
                        $arrApiParams['filter_free'] = 'Y';

                        if (!empty($searchBoxParams['category']))
                        {
                            $searchBoxParams['category'] .= ',0';
                        }
                        else
                        {
                            $searchBoxParams['category'] .= '0';
                        }
                    }
                }
            }
        }

        $eventData = $events_api->search($arrApiParams);

        $groupEvents = array();

        // Group event by month
        if (!empty($eventData) && $eventData['status'] == 200)
        {
            foreach ($eventData['data'] as $event)
            {
                $eventDate = date('M Y', strtotime($event['start_date']));

                $groupEvents[$eventDate][] = $event;
            }
        }

        return $this->render('inSingFrontendBundle:EventFinder:result.html.twig', array(
            'results' => $groupEvents,
            'searchBoxParams' => $searchBoxParams,
            'apiParams' => json_encode($arrApiParams)
        ));
    }

    /**
     * @author Khai.Vu
     *
     * Get category ids from their names
     *
     * @param $allCategories
     * @param $arrCategories
     *
     * @return String Category ids
     */
    private function _getCategoryIdsFromNames($arrCategories)
    {
        $cache = $this->get('new.insing.cache');
        $categoryKey = $this->container->getParameter('new_insing_event_category_cache_key');

        $strCategoryIds = "";

        if($cache->checkCache($categoryKey))
        {
            $allCategories = $cache->getCache($categoryKey);

            foreach ($allCategories['cat-search'] as $key => $value)
            {
                if (in_array($key, $arrCategories))
                {
                    $strCategoryIds .= ($value . ",");
                }
            }
        }
        else
        {
            $events_api = $this->get('events_api');
            $allCategories = $events_api->getAllCategories();

            foreach ($allCategories as $category)
            {
                if (in_array($category['url_fragment'], $arrCategories))
                {
                    $strCategoryIds .= ($category['id'] . ",");
                }
            }
        }

        $strCategoryIds = rtrim($strCategoryIds, ",");

        return $strCategoryIds;
    }

    /**
     * @author Khai.Vu
     *
     * Convert date from url format (1-Jan-2000) to api param format (2000-01-01)
     *
     * @param $strDate Date form url format
     *
     * @return String Date form api param format
     */
    private function _convertDate($strDate)
    {
        $arrDayMap = array(
            1 => '01',
            2 => '02',
            3 => '03',
            4 => '04',
            5 => '05',
            6 => '06',
            7 => '07',
            8 => '08',
            9 => '09'
        );

        $arrMonthMap = array(
            'Jan' => '01',
            'Feb' => '02',
            'Mar' => '03',
            'Apr' => '04',
            'May' => '05',
            'Jun' => '06',
            'Jul' => '07',
            'Aug' => '08',
            'Sep' => '09',
            'Oct' => '10',
            'Nov' => '11',
            'Dec' => '12'
        );

        $arrDate = explode("-", $strDate);

        if (isset($arrDayMap[$arrDate[0]]))
        {
            $arrDate[0] = $arrDayMap[$arrDate[0]];
        }

        $strResult = $arrDate[2] . '-' . $arrMonthMap[$arrDate[1]] . '-' . $arrDate[0];

        return $strResult;
    }

    /**
     * @author Khai Vu
     *
     * @param Request $request
     * @return Response
     */
    public function ajaxResultLoadAction(Request $request)
    {
        if ($request->isXmlHttpRequest())
        {
            $page = $request->get('page', 0);
            $apiParams = $request->get('apiParams', '');
            $arrApiParams = (array)json_decode($apiParams);
            $arrApiParams['page'] = $page;

            $groupEvents = $this->loadEventsResultList($arrApiParams);

            $output_html = '';

            foreach ($groupEvents as $key => $data) {
                $output_html .= $this->renderView('inSingFrontendBundle:EventFinder:_event_list_result.html.twig', array(
                    'event_list' => $data,
                    'key' => $key
                ));
            }

            return new Response($output_html);
        }

        return JsonResponse::create(array(
            'status' => Response::HTTP_BAD_REQUEST,
            'message' => 'Invalid AJAX request.'
        ));
    }

    /**
     * @author Khai Vu
     *
     * @param $page
     * @param $currentMonth
     * @param int $contentNum
     * @param int $times
     * @param int $counter
     * @return array|bool
     */
    public function loadEventsResultList($arrApiParams)
    {
        $events_api = $this->get('events_api');
        $eventData = $events_api->search($arrApiParams);

        $groupEvents = array();

        // Group event by month
        if (!empty($eventData) && $eventData['status'] == 200)
        {
            foreach ($eventData['data'] as $event)
            {
                $eventDate = date('M Y', strtotime($event['start_date']));

                $groupEvents[$eventDate][] = $event;
            }
        }

        return $groupEvents;
    }
}

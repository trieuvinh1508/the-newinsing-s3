<?php
namespace inSing\AdminBundle\Service;

class inSingApiService
{
    protected $sessionToken;
    protected $container;
    protected $logger;

    public function __construct($container, $logger)
    {
        $this->container = $container;
        $this->logger = $logger;
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function setContainer($container)
    {
        $this->container = $continer;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * Getting the session token from API
     */
    protected function getSession()
    {
        try {
            $url = $this->container->getParameter('insing_datasource_movie_auth_url');
            $this->logger->debug("url = " . $url);
            $client = @new \SoapClient($url, array('exceptions' => 1));
            $params = array(
                'deviceIdentifier' => array('type' => 'IMEI', 'identifier' => '9876543210'),
                'clientId' => $this->container->getParameter('insing_datasource_movie_client_id'),
                'password' => $this->container->getParameter('insing_datasource_movie_password'),
            );
            $this->logger->debug("params = " . var_export($params, true));

            $result = $client->login($params);
            $this->logger->debug("result = " . var_export($result, true));

            if ($result->resultCode == 200) {
                $this->sessionToken = $result->sessionToken;
            } else {
                throw new \Exception('Error logging in to inSing API!');
            }
        } catch (\Exception $e) {
            throw new \Exception('An exception occured - ' . $e);
        }
    }

    /**
     * Getting movie and cinema list
     */
    public function getMoviesAndCinemas()
    {
        $this->logger->info('Getting movies list from inSing API ...');

        $movies = array();
        $cinemas = array();

        if (!$this->sessionToken) {
            $this->getSession();
        }

        if ($this->sessionToken) {
            try {
                // Getting movies first
                $url = $this->getContainer()->getParameter('insing_datasource_movie_service_url');
                $this->logger->debug("Connecting to $url");

                $client = @new \SoapClient($url, array('exceptions' => 1));

                $result = $client->findMovies(
                    array(
                        'sessionToken' => $this->sessionToken,
                        'pageNumber' => 1,
                        'pageSize' => 100,
                        'showingNow' => array(
                            'showingNowRange' => 3,
                        ),
                        'sortByValues' => array(
                            'field' => 'SCREENS',
                            'reverse' => true,
                        ),
                    )
                );

                $this->logger->debug('$result = ' . print_r($result, true));

                if ($result->resultCode == 200) {
                    foreach ($result->results->movie as $m) {
                        $this->logger->debug('$m = ' . print_r($m, true));
                        $movies[self::filterString($m->movieId)] = self::filterString($m->movieTitle);
                    }
                } else {
                    throw new \Exception("Error findMovies - " . $result->resultMessage);
                }

                // Getting cinemas next
                $result = $client->getCinemaList(
                    array(
                        'sessionToken' => $this->sessionToken,
                    )
                );

                $this->logger->debug('$result = ' . print_r($result, true));

                if ($result->resultCode == 200) {
                    foreach ($result->results->cinema as $c) {
                        $this->logger->debug('$m = ' . print_r($c, true));
                        $cinemas[self::filterString($c->cinemaId)] = self::filterString($c->cinemaName);
                    }
                } else {
                    throw new \Exception("Error findMovies - " . $result->resultMessage);
                }
            } catch (Exception $e) {
                throw new \Exception('Exception - ' . $e);
            }
        } else {
            throw new \Exception("Error no session token!");
        }

        return array('movies' => $movies, 'cinemas' => $cinemas);
    }

    /**
     * Getting list of cinemas for the movie
     */
    public function getCinemas($movieId)
    {
        $this->logger->info("Getting showtimes for movie $movieId from inSing API ...");

        $cinemas = array();

        if (!$this->sessionToken) {
            $this->getSession();
        }

        if ($this->sessionToken) {
            try {
                // Getting movies first
                $url = $this->getContainer()->getParameter('insing_datasource_movie_service_url');
                $this->logger->debug("Connecting to $url");

                $client = @new \SoapClient($url, array('exceptions' => 1));

                $result = $client->getShowTimesByMovieId(
                    array(
                        'sessionToken' => $this->sessionToken,
                        'movieId' => $movieId,
                    )
                );

                $this->logger->debug('$result = ' . print_r($result, true));
                if ($result->resultCode == 200) {
                    if (is_array($result->results->movieShowtime)) {
                        $movieShowtimes = $result->results->movieShowtime;
                    } else {
                        $movieShowtimes = array($result->results->movieShowtime);
                    }
                    foreach ($movieShowtimes as $m) {
                        $this->logger->debug('$m = ' . print_r($m, true));
                        if (!in_array($m->cinemaId, $cinemas)) {
                            $cinemas[] = $m->cinemaId;
                        }
                    }
                } else {
                    //throw new \Exception("Error getShowTimesByMovieId - " . $result->resultMessage);
                    $this->logger->debug("Error getShowTimesByMovieId - " . $result->resultMessage);
                }

            } catch (Exception $e) {
                //throw new \Exception('Exception - ' . $e);
                $this->logger->debug("Error " . $e->getMessages());
            }
        } else {
            ///throw new \Exception("Error no session token!");
            $this->logger->debug("Error no session token!");
        }

        return $cinemas;
    }

    protected static function filterString($var)
    {
        return filter_var($var, FILTER_SANITIZE_STRING);
    }

    protected static function filterUrl($var)
    {
        return filter_var($var, FILTER_VALIDATE_URL);
    }

    protected static function filterInt($var)
    {
        return intval(filter_var($var, FILTER_VALIDATE_INT));
    }

    protected static function filterFloat($var)
    {
        return filter_var($var, FILTER_VALIDATE_FLOAT);
    }
}
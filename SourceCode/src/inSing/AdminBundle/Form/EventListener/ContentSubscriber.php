<?php
namespace inSing\AdminBundle\Form\EventListener;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ContentSubscriber implements EventSubscriberInterface
{
    private $factory;
    private $doctrine;
    private $mode;

    public function __construct(FormFactoryInterface $factory, $doctrine, array $mode)
    {
        $this->factory = $factory;
        $this->doctrine = $doctrine;
        $this->mode = $mode;
    }

    public static function getSubscribedEvents()
    {
        return array(FormEvents::POST_BIND => 'prePostData');
    }

    public function prePostData(FormEvent $event)
    {
        $form = $event->getForm();

        if (isset($form['live_datetime']) && is_object($form['live_datetime']->getData())) {
            $dateTime = $form['live_datetime']->getData()->format('Y-m-d H:i');

            if ($dateTime < date('Y-m-d H:i')) {
                $form['live_datetime']->addError(new FormError('Publish DateTime is invalid.'));
            }

            if (isset($form['expire_datetime']) && is_object($form['expire_datetime']->getData())) {
                $expire = $form['expire_datetime']->getData()->format('Y-m-d H:i');

                if ($expire < $dateTime) {
                    $form['expire_datetime']->addError(new FormError('Expiry DateTime is invalid.'));
                }
            }
        }
    }
}
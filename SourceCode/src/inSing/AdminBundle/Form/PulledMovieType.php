<?php

namespace inSing\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PulledMovieType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('movieId', 'text',
                array(
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Movie id must be required.')),
                ),
                'attr' => array('maxlength' => 20)
            ))
            ->add('movieTitle', 'text',
                array(
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Title must be required.')),
                    new Length(array('maxMessage' => 'Title max length is 255 characters.', 'max' => 255)),
                ),
                'attr' => array('maxlength' => 255)
            ))
            ->add('movieDescription', 'textarea',
                array('required' => true,
                    'constraints' => array(
                        new NotBlank(array('message' => 'Description must be required.'))
                    ),
                'attr' => array('class' => 'full')
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'inSing\DataSourceBundle\Entity\PulledMovie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'insing_datasourcebundle_pulledmovie';
    }
}

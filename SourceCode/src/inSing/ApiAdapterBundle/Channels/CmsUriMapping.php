<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class CmsUriMapping
{
    const ARTICLE_DETAILS           = "/article-details";
    const PREV_NEXT_ARTICLES        = "/articles-prev-next";
    const FIND_ARTICLES             = "/find-articles";
    const CREATE_GALLERY_PHOTO      = "/create-gallery-photo";
    const CREATE_GALLERY            = "/create-gallery";
    const MULTIPLE_ARTICLE_DETAILS  = "/multiple-article-details";
    const GALLERY_DETAILS           = "/gallery-details";
    const PREV_NEXT_GALLERY         = "/galleries-prev-next"; /*@author Cuong Au*/
    const FIND_CONTENTS             = "/find-contents";
}
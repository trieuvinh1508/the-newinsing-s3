<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\ApiAdapterBundle\Channels;

use inSing\ApiAdapterBundle\Channels\Helper\ApiCmsAdapter;

/**
 * Description of ThirdPartyMovies
 *
 * @author linuxmint
 */
class Cms extends ApiCmsAdapter
{
    public function __construct($container, $cache, $channelLogger)
    {        
        parent::__construct($container, $cache, $channelLogger, "cms");
    }

    public function getArticleDetails($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::ARTICLE_DETAILS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getPrevNextArticles($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::PREV_NEXT_ARTICLES;
        return $this->runApiByMethod($url, "GET", $params);
    }
    
    public function findArticles($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::FIND_ARTICLES;
        return $this->runApiByMethod($url, "GET", $params);        
    }

    /**
     * @author Dat.Dao
     * @param array $params
     * @return array|Helper\Ambigous
     */
    public function findContents($params = array())
    {
        //https://api.cms.test.insing.com/find-contents?channel[0]=HungryGoWhere.com&channel[1]=HungryGoWhere.com&session_token=ed422ef5148774d5ff47cf5aad559cb2&sig=10fc3c74e2385580cb032f627f02c481
        $url = $this->basicUrl . CmsUriMapping::FIND_CONTENTS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @author Dat.Dao
     * @param $params
     * @return int
     */
    public function createGallery($params)
    {
        /**
        $params = array();
        $params['status'] = 0; //DRAFT
        $params['gallery_type'] = 0;
        $params['region'] = 'ASIA';
        $params['country'] = 'SG';
        $params['city'] = 'singapore';
        $params['channel'] = 5;
        $params['category'] = 36;
        $params['content_series'] = 684;
        $params['title'] = 'dmdtest-title' . " - Photos";
        $params['summary'] = 'dmdtest-summary';
         */
        $url = $this->basicUrl . CmsUriMapping::CREATE_GALLERY;
        $result = $this->runApiByMethod($url, "POST", $params);
        /**
         array (size=3)
        'result_code' => string '200' (length=3)
        'result_message' => string 'Create gallery successfully' (length=27)
        'created_gallery_id' => string '20022707' (length=8)
         */
        if( ($result['result_code'] == 200) && isset($result['created_gallery_id'])) {
            return $result['created_gallery_id'];
        }
        return 0;
    }

    /**
     * @author Dat.Dao
     * @param $params
     * @return array|Helper\Ambigous
     */
    public function createGalleryPhoto($params)
    {
        /**
        $params = array();

        $params['gallery_id'] = 20022687;
        $params['caption'] = 'caption-tets';
        $params['credit'] = 'caption-tets';
        $path = '/home/dat/Downloads/event-thankyou.jpg';
        $imageFileBinary = fread(fopen($path, 'r'),
        filesize($path));
        $params['image_content'] = base64_encode($imageFileBinary);
        $file = explode(".", $path);
        $fileExt = array_pop($file);
        //$filename = Utils::cleanUrl(implode("-", $file)) . ".$fileExt";
        $filename = "event-thankyou.jpg";
        //$filename = (strlen($filename) > 30) ? substr($filename, strlen($filename) - 30) : $filename;
        //$filename = 'event-thankyou.jpg';

        $params['image_filename'] = $filename;
         */
        $url = $this->basicUrl . CmsUriMapping::CREATE_GALLERY_PHOTO;
        return $this->runApiByMethod($url, "POST", $params);
    }

    public function getMultipleArticleDetails($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::MULTIPLE_ARTICLE_DETAILS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    public function getGalleryDetails($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::GALLERY_DETAILS;
        return $this->runApiByMethod($url, "GET", $params);
    }

    /**
     * @param array $params
     * @return array|Helper\Ambigous
     * @author Cuong Au
     */
    public function getPrevNextGallery($params = array())
    {
        $url = $this->basicUrl . CmsUriMapping::PREV_NEXT_GALLERY;
        return $this->runApiByMethod($url, "GET", $params);
    }
}

<?php

namespace inSing\ApiAdapterBundle\Channels;

/**
 * Class UriMapping
 *
 * @package inSing\ApiBundle\Channels
 */
class EventsUriMapping
{
    const SEARCH  = "/2.0/events/search";
    const DETAILS = "/2.0/event/{id}";
    const AUTO_COMPLETE = "/2.0/events/autocomplete";
    const CREATE_EVENTS = "/2.0/admin/events";
    const TOP_THINGS_TO_DO = "/2.0/events/topthings";
    const SUPER_MODULE = "/2.0/events/supermodule";
    const GET_ALL_CATEGORIES = "/2.0/category/get-all-categories";
}
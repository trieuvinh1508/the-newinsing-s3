<?php

/*
 * This file is part of the Monolog package.
 *
 * (c) Jordi Boggiano <j.boggiano@seld.be>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace inSing\DataSourceBundle\Utilities;


/**
 * Stores logs to files that are rotated every day and a limited number of files are kept.
 *
 * This rotation is only intended to be used as a workaround. Using logrotate to
 * handle the rotation is strongly encouraged when you can use it.
 *
 * @author Christophe Coevoet <stof@notk.org>
 * @author Jordi Boggiano <j.boggiano@seld.be>
 */
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class HGWLoggerHander extends StreamHandler
{
    protected $filename;
    protected $maxSize = 10000000; // 10MB

    /**
     * @param string   $filename
     * @param integer  $maxSize       The maximal size for log file will be logged (0 means unlimited)
     * @param integer  $level          The minimum logging level at which this handler will be triggered
     * @param Boolean  $bubble         Whether the messages that are handled can bubble up the stack or not
     * @param int|null $filePermission Optional file permissions (default (0644) are only for owner read/write)
     * @param Boolean  $useLocking     Try to lock log file before doing any writes
     */
    public function __construct($filename, $maxSize = 10000000, $level = Logger::DEBUG, $bubble = true, $filePermission = null, $useLocking = false)
    {
        $this->filename = $filename;
        if(intval($maxSize))
        {
            $this->maxSize = (int) $maxSize;
        }

        parent::__construct($filename, $level, $bubble, $filePermission, $useLocking);
    }

    /**
     * {@inheritdoc}
     */
    protected function write(array $record)
    {
        // on the first record written, if the log is new, we should rotate (once per day)
        $this->splitFile();

        parent::write($record);
    }

    /**
     * Rotates the files.
     */
    protected function splitFile()
    {
        if($this->getLogFileSize() < $this->maxSize){
            return;
        }

        $logFile = fopen($this->filename, 'r');
        $filenameGz = $this->getGzFileName();

        fseek($logFile, 0);
        $logFileGz = gzopen($filenameGz, 'wb9');
        while (!feof($logFile)) {
            gzwrite($logFileGz, fread($logFile, 65536));
        }
        gzclose($logFileGz);

        fclose($logFile);
        file_put_contents($this->filename, "");
        //unlink($this->filename);
    }
    public function getLogFileSize()
    {
        return is_file($this->filename) ? filesize($this->filename) : 0;
    }

    public function getGzFileName()
    {
        $pathinfo = pathinfo($this->filename);
        return  "{$pathinfo['dirname']}/{$pathinfo['filename']}_" . date('Y-m-d-H-i') . ".{$pathinfo['extension']}" . '.gz';
    }
}

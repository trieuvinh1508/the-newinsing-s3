<?php
namespace inSing\DataSourceBundle\Utilities;
use \Symfony\Component\DependencyInjection\ContainerInterface ;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
class SSOApi {
    
    private static $consumer_key = null ;
    private static $consumer_secret = null ;
    private static $access_token = null ;
    private static $access_token_secret = null ;
    private static $user_rolelst_url = null ;
    private static $oauth_connect = null ;
    private static $role_name_list = null ;
    private static $channel_id = null ;
    private static $container = null ;
    
    const STATUS_CODE = 200 ;
    const STATUS_NODE_ERR = "error" ;
    const STATUS_NODE = "status" ;
    const USER_NODE = "users" ;
    const ROLE_NAME_LIST = "role_name_list" ;
    const CHANNEL_ID = "channel_id" ;        
    
    
    /* Author Minh  ;
     * Contructor set variable
     */
    public function __construct(ContainerInterface $container){
        self::$consumer_key = $container->getParameter("oauth_consumer_key") ;
        self::$consumer_secret = $container->getParameter("oauth_consumer_secret") ;
        self::$user_rolelst_url = $container->getParameter("oauth_get_users_byrole_url") ;
        self::$access_token = $container->getParameter("oauth_accesstoken_key") ;
        self::$access_token_secret = $container->getParameter("oauth_accesstoken_secret") ;
        self::$role_name_list = $container->getParameter("admin_roles_check_access_object") ;
        self::$channel_id = $container->getParameter("admin_sso_hgwr_channel_id") ;
        
        //connect OAuth
        self::$oauth_connect = new \OAuth(self::$consumer_key, self::$consumer_secret);
        self::$oauth_connect->setSSLChecks(1);
        
        self::$container = $container ;
    }
    
    /* Author Minh Tong
     * get User Info By Role from SSO Server
     * @return Array User List
     */
    public function getUsersByRoles() {
        
        try {
             //Register Token
            self::$oauth_connect->setToken(self::$access_token , self::$access_token_secret);
            
            $data = array(self::ROLE_NAME_LIST => implode(",", self::$role_name_list) , self::CHANNEL_ID => self::$channel_id);
            //Get User Data
            self::$oauth_connect->fetch(self::$user_rolelst_url, $data , OAUTH_HTTP_METHOD_GET);                

            $response = self::$oauth_connect->getLastResponse() ;

            if($response) {
                $response_data = self::getResponse($response) ;
                
                if($response_data[self::STATUS_NODE] == self::STATUS_CODE){
                    return $response_data[self::USER_NODE] ;
                }
                else {
                    throw new ResourceNotFoundException($response_data[self::STATUS_NODE_ERR]) ;
                }
            }
        } catch (\Exception $exc) {
            self::$container->get('monolog.logger.auth')->error($exc->getCode() . ':' . $exc->getMessage());
        }
       
        return array() ;
    }
    
    private function getResponse($response) {
        return json_decode($response,true) ;
    }
    
}

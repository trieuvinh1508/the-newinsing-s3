<?php

namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;
use inSing\DataSourceBundle\Utilities\HgwUtils;
use inSing\DataSourceBundle\Utilities\HgwLogger;

class TableDbApi {
    
    /**
     * @var ContainerInterface
     */    
    public $container = null;
    
    /**
     * @var HgwLogger
     */
    private $logger;

    const CACHE_KEY_ALL_RESERVATIONS = 'CACHE_KEY_ALL_RESERVATIONS';

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
	$this->logger = new HgwLogger($container, 'table_db_api');
	
    }
    
    /**
     * get All User Reservations By Email
     * url pattern: http://qa1.tabledb.com/tabledb-web/reservation/email/{email}/
     * @param type $email
     */
    public function getAllReservationsByEmail($email) {
        if (empty($email)) {
            return false;
        }
        
        $cacheKey = self::CACHE_KEY_ALL_RESERVATIONS . $email;
        $reservations = $this->container->get('hgw.cache')->fetch($cacheKey);
        
        if (empty($reservations)) {
            $reservationUrl = $this->container->getParameter('booking_get_reservations');
            $reservationUrl = str_replace('{email}', $email, $reservationUrl);
            
            $result = $this->container->get('curl')->setMethod(Curl::HTTP_GET)->call($reservationUrl);

            $reservations = array();
            if (!empty($result) && isset($result['response']) && $result['response']['status'] == CURLE_OK) {
                $reservations = $result['response'];
                $this->container->get('hgw.cache')->save($cacheKey, $reservations);
            }
        }
        
        return $reservations;
    }

    public function cancelReservation($id, $verificationKey, $version) {
        $booking_info = $this->container->getParameter('booking_by_country');
        $booking_info = $booking_info[$this->container->getParameter('country_code')];
        $partnerCode = $booking_info['partner_code'];
        $partnerAuthCode = $booking_info['partner_auth'];
        
        $bookingCancelReservationUrl = $this->container->getParameter('booking_cancel_reservation');
        $bookingCancelReservationUrl = str_replace(
            array('{id}', '{verificationKey}', '{version}', '{partnercode}', '{partnerauthcode}'),
            array($id, $verificationKey, $version, $partnerCode, $partnerAuthCode), $bookingCancelReservationUrl
        );
        
        $result = $this->container->get('curl')->setMethod(Curl::HTTP_DELETE)->call($bookingCancelReservationUrl);
        
    }
    
    /**
     * Get list time slot
     * @author LapTo
     * @param array $param
     * @return array
     */
    public function getListTimeSlot($param) {
	try {
	    if (isset($param['date']) && !empty($param['date']) 
		    && isset($param['restaurant_id']) && !empty($param['restaurant_id'])) {
		$urlTimeSlot    = $this->container->getParameter('booking_get_time_slot_list');
		$urlTimeSlot    = str_replace('{date}', $param['date'], $urlTimeSlot);
		$urlTimeSlot    = str_replace('{restaurant_id}', $param['restaurant_id'], $urlTimeSlot);
		$listTimeSlot   = HgwUtils::doCurlGet($urlTimeSlot);
		//Write log url
		$data = array();
		$this->logger->info($urlTimeSlot);
		if (isset($listTimeSlot['data']) && !empty($listTimeSlot['data'])) {
		    $data = $this->sort_arr_of_arr($listTimeSlot['data'], 'timeSlotId');
		}
		return $data;
	    }
	    return array();
	} catch (Exception $ex) {
	    $this->logger->exp_err($ex->getMessage());
	    return array();
	}
	
    }
    
    private function sort_arr_of_arr($array, $sortby, $direction='asc') {

        $sortedArr = array();
        $tmp_Array = array();

        foreach($array as $k => $v) {
            $tmp_Array[] = strtolower($v[$sortby]);
        }

        if($direction=='asc'){
            asort($tmp_Array);
        }else{
            arsort($tmp_Array);
        }

        foreach($tmp_Array as $k=>$tmp){
            $sortedArr[] = $array[$k];
        }

        return $sortedArr;
    }
    
    /**
     * Parse timeslot for search
     * @author LapTo
     * @param array $listTimeSlot
     * @return array
     */
    public function parseTimeSlotForSearch($listTimeSlot, $date) {
	$result = array();
	foreach ($listTimeSlot as $row) {
	    $timeSlotId = $row['timeSlotId'];
	    if (count($timeSlotId) < 4) {
		$timeSlotId = sprintf('%04d', $timeSlotId);
	    }
	    $timeslot = array(
		'date' => $date,
		'time_slot' => $timeSlotId,
		'time' => substr($timeSlotId, 0, 2) . ':' . substr($timeSlotId, 2, 2),
		'remaining_time_slot_capacity' => $row['remainingTimeSlotCapacity'],
		'remaining_shift_capacity' => $row['remainingShiftCapacity'],
		'min_covers' => $row['minNumberOfSeatsForSingleReservation'],
		'max_covers' => $row['maxNumberOfSeatsForSingleReservation'],
		'cutoff' => $row['cutOffTime'],
	    );
	    array_push($result, $timeslot);
	}
	return $result;
    }
    
}


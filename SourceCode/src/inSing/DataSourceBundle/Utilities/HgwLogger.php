<?php

/**
 * Description of HgwLogger
 *
 * @author BienTran
 */

namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class HgwLogger extends Logger
{
  private $container;
  protected $name;
  public function __construct(ContainerInterface $container, $name) {
    parent::__construct($name);
    $this->container = $container;
    $this->name = strval($name);
    $this->pushHandlerLogger();
  }

  public function pushHandlerLogger() {
    $stream = $this->container->get('kernel')->getLogDir(). '/' . $this->name . '_' . $this->container->get('kernel')->getEnvironment() . '.log';
    $file_size = intval($this->container->getParameter('logger_file_size'));
    $hander = new HGWLoggerHander($stream, $file_size);
    $this->pushHandler($hander);
  }

  public function info($message, array $context = array()) {

    try {
      $debug_track = debug_backtrace();
      $debug_track = array_shift($debug_track);
      $line_number = $debug_track['line'];
      $file_script = basename($debug_track['file']);

      $context = array_merge(array('file' => $file_script, 'line' => $line_number), $context);
    } catch (\Exception $exc) {
    }
    parent::info($message, $context);
  }

  public function error($message, array $context = array()) {
      return $this->err($message, $context);
  }

  public function exp_err(\Exception $exc) {
  	$context = array('file' => basename($exc->getFile()), 'line' => $exc->getLine());
  	$this->err($exc->getMessage(), $context);
  }

}

?>

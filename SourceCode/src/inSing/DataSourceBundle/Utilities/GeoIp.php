<?php

namespace inSing\DataSourceBundle\Utilities;

class GeoIp {

    /**
     *
     * @author Vu Luu
     */
    public static function getCities($container, $countryCode)
    {
        try {
            if (null != $countryCode) {
                $cities = $container->getParameter('geolocation_cities');
                return $cities[$countryCode];
            }
        } catch (\Exception $exc) {
        }
        return null;
    }
}

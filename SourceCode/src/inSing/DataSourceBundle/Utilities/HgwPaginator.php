<?php

/**
 * Description of Paginator
 *
 * @author BienTran
 */

namespace inSing\DataSourceBundle\Utilities;
class HgwPaginator {
    
    protected $container;
    protected $pager;
    public function __construct($container) {
        $this->container = $container;
        $this->pager = $this->container->get('knp_paginator');
    }
    
    /**
     * 
     * @param QueryString $query is target to paginate
     * @param int $pageNumber
     */
    public function getPaging($query, $pageNumber, $itemPerPage = 10) {
        return $this->pager->paginate($query, $pageNumber, $itemPerPage);
    }
}

?>

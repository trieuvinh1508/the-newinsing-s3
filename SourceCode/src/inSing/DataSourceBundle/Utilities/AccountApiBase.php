<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

use Symfony\Component\DependencyInjection\ContainerInterface;
use inSing\DataSourceBundle\Utilities\HgwLogger;

/**
 * Description of AccountApiBase
 *
 * @author Dat.Dao <dat.dao@s3corp.com.vn>
 * Editor: Tri Van
 */
class AccountApiBase {

    const EXPIRED_TOKEN = 400;
    const INVALID_TOKEN = 403;
    const SESSION_TOKEN_TIME_EXPIRED = 1200; // 20mins
    const ACCOUNT_API_AUTHE_CACHE_KEY = "ACCOUNT_API_AUTHE_CACHE_KEY";
    const HTTP_OK = 200;

    /**
     * @var HgwLogger
     */
    protected $logger;
    protected $cache;
    protected $container;
    protected $base_api_url = '';
    protected $consumer_secret = '';
    protected $auth_url = '';
    protected $auth_info = '';
    protected $countryCode = '';

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->logger = new HgwLogger($this->container, 'account_api_base');
        $this->cache = $this->container->get('hgw.cache');

        $this->base_api_url = $this->container->getParameter("account_api_base_api_url");
        $this->consumer_secret = $this->container->getParameter("account_api_consumer_secret");
        $this->auth_url = $this->container->getParameter("account_api_auth_url");
        $channelInfo = $this->container->getParameter("api_restful_token_for_locate");
        $this->countryCode = $this->container->getParameter('country_code');
        //Get Authen info by country
        $access_token = isset($channelInfo[$this->countryCode]['account_api_access_token']) ?
                                $channelInfo[$this->countryCode]['account_api_access_token'] : null;
        $token_secret = isset($channelInfo[$this->countryCode]['account_api_token_secret']) ?
                                $channelInfo[$this->countryCode]['account_api_token_secret'] : null;
        $this->auth_info = array("access_token" => $access_token, "token_secret" => $token_secret);
    }

    /**
     * @param $secret string
     * @param $uri string
     * @param $params array
     * @return string
     */
    public function makeSignature($secret, $uri, $params) {
        // First, add the secret
        $pre_hash = $secret;
        // Then, add the uri without slashes
        $pre_hash .= preg_replace('/\//', '', $uri);

        // After that, add the key-value of params in ascending order of keys
        $params = $this->filterParams($params);
        $this->sortParams($params);
        $paramString = http_build_query($params, '', '');
        $pre_hash .= str_replace('=', '', $paramString);
        // Lastly, do a MD5 hash and return
        $post_hash = md5($pre_hash);
        return $post_hash;
    }

    public function filterParams($array) {
        return array_filter($array, function($item) {
                if (is_array($item)) {
                    return count($item) > 0;
                } elseif (is_null($item)) {
                    return false;
                } elseif (is_bool($item)) {
                    return true;
                } else {
                    return strlen($item) > 0;
                }
            }
        );
    }

    public function sortParams(&$params) {
        if (isset($params) && is_array($params)) {
            foreach ($params as &$param) {
                $this->sortParams($param);
            }
            ksort($params);
        }
    }

    public function doPut($url, $post_json) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_json));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($post_json)))
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        $resp = curl_exec($ch);
        curl_close($ch);
        return $resp;
    }

    public function doPost($url, $post_json) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_json));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($post_json)))
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        $resp = curl_exec($ch);
        curl_close($ch);
        return $resp;
    }

    public function doDelete($url, $post_json) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_json));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen(json_encode($post_json)))
        );
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        $resp = curl_exec($ch);
        curl_close($ch);
        return $resp;
    }

    public function doGet(&$url, $params) {
        if (!empty($params)) {
            $query = http_build_query($params);
            $url .= "&" . $query;
        }
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        $resp = curl_exec($ch);
        curl_close($ch);
        $this->container->get('common.utils')
        ->collectApiCall('Profile', $url);
        return $resp;
    }

    /**
     * @author tin.tran
     * @param $baseUriUrl
     * @param $method
     * @param $params array params
     * @param bool $fistCall
     * @return string
     */
    public function runApi($baseUriUrl, $method, $params, $fistCall = true) {
        $base_api_url = $this->base_api_url;
        $consumer_secret = $this->consumer_secret;

        $secret = $consumer_secret;
        $baseUrl = $base_api_url;
        $uri = $baseUriUrl;

        //get sig
        $sig = $this->makeSignature($secret, $uri, $params);

        if ($method == 'delete') {
            $paramString = 'sig=' . $sig . '&session_token=' . $params['session_token'];
            unset($params['session_token']);
        } else {
            $paramString = 'sig=' . $sig;
        }
        $url = $baseUrl . $uri . '?' . $paramString;

        if ($method == 'put') {
            $resp = $this->doPut($url, $params);
        } elseif ($method == 'post') {
            $resp = $this->doPost($url, $params);
        } elseif ($method == 'delete') {
            $resp = $this->doDelete($url, $params);
        } elseif ($method == 'get') {
            $resp = $this->doGet($url, $params);
        } else {
            $resp = json_encode(array('error' => 'invalid method request'));
        }

        @$json = json_decode($resp, true);
        if (isset($json['status']) && ($json['status'] == self::EXPIRED_TOKEN || $json['status'] == self::INVALID_TOKEN) && $fistCall === true) {
            $sesstion_token_obj = $this->getSesstionToken();
            $params['session_token'] = $sesstion_token_obj['session_token'];
            return $this->runApi($baseUriUrl, $method, $params, false);
        }
        else {
            if ($json['status'] != self::HTTP_OK) {
                if(is_array($json)) {
                    $this->logger->error($url, $json);
                }
                else {
                    $this->logger->error('Result NULL: '. $url);
                }
            }
            else {
                $this->logger->info($url);
            }
        }
        return $resp;
    }

    /**
     *
     * @param string $auth_url
     * @param type $auth_info: array("access_token" => "9fae276e08585f78b6e99ab21f4326",
      "token_secret" => "298a205d58",)
     * @param type $base_api_url:
     * @param type $consumer_secret
     */
    private function authen() {
        $auth_url = $this->auth_url;
        $auth_info = $this->auth_info;
        $base_api_url = $this->base_api_url;
        $consumer_secret = $this->consumer_secret;
        $base_url = $base_api_url;
        $params = $auth_info;
        $uri = str_replace($base_url, "", $auth_url);
        $sig = $this->makeSignature($consumer_secret, $uri, $params);
        $auth_url = $auth_url . "?sig=" . $sig;
        $rest = $this->doPost($auth_url, $params);
        $rs = json_decode($rest, true);
        return $rs;
    }

    public function getSesstionToken()
    {
        $cache_key = self::ACCOUNT_API_AUTHE_CACHE_KEY . $this->countryCode;
        $result = $this->cache->fetch($cache_key);
        if(!$result)
        {
            $data = $this->authen();
            if (isset($data['data']['session_token']) && $data['data']['channel']) {
                $result['session_token'] = $data['data']['session_token'];
                $result['channel'] = $data['data']['channel'];
                $this->cache->save($cache_key, $result, self::SESSION_TOKEN_TIME_EXPIRED);
                return $result;
            }
            throw new \Exception("Empty session_token", 402);
        }
        return $result;
    }

}

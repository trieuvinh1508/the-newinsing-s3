<?php
namespace inSing\DataSourceBundle\Utilities;

class BreadCrumb
{
    public function getBreadCrumbDefault($container)
    {
        $breadcrumbs = $container->get('white_october_breadcrumbs');
        $breadcrumbs = $breadcrumbs->addItem('Home', $container->get('router')->generate('in_sing_hgwr_homepage', array(), true));
        return $breadcrumbs;
    }
}
?>

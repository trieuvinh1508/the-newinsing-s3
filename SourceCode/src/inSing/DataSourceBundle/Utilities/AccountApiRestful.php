<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

use inSing\DataSourceBundle\Utilities\AccountApiBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use inSing\DataSourceBundle\Utilities\HgwLogger;
/**
 * Description of AccountApiRestful
 *
 * @author Dat.Dao <dat.dao@s3corp.com.vn>
 */
class AccountApiRestful {

    //Test â€“ http://api.accountsss.test.insing.com/auth

    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $cache;

    /**
     * @var HgwLogger
     */
    protected $logger;

    protected $cache_lifetime_for_api;
    protected $channelName;
    protected $countryCode;
    protected $listBookmarkChanels;
    const CACHE_KEY_GET_USER_BY_EMAIL = 'CACHE_KEY_EA_GET_USER_BY_EMAIL';
    const CACHE_KEY_GET_USER_BY_USERNAME = 'CACHE_KEY_EA_GET_USER_BY_EMAIL';
    const CACHE_KEY_GET_USER_BY_ID = 'CACHE_KEY_EA_GET_USER_BY_EMAIL';
    /**
     * @author Dat Dao <john.doe@example.com>
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->cache = $this->container->get('hgw.cache');
        $this->logger = new HgwLogger($this->container, 'account_api_restful');
        $this->cache_lifetime_for_api = $this->container->getParameter("cache_lifetime_for_api");
        $this->countryCode = $this->container->getParameter('country_code');
        $this->listBookmarkChanels = $this->container->getParameter('account_api_restful_bookmark_chanel');
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @return AccountApiBase
     */
    private function getAccountApiBase() {
        return $this->container->get("account.api.base");
    }

    /**
     * @author Minh Tong â™«
     * @return String Current channel name
     */
    private function getCurrentChannelName(){
        return $this->channelName;
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @return type
     */
    private function getSessionToken() {
        $sesstion_token_obj = $this->getAccountApiBase()->getSesstionToken();

        if (isset($sesstion_token_obj['session_token']) && !empty($sesstion_token_obj['session_token'])) {
            if (isset($sesstion_token_obj['channel'])) {
                $this->channelName = $sesstion_token_obj["channel"];
            }
            return $sesstion_token_obj['session_token'];
        }
        throw new \Exception("Empty session_token", 402);
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $id
     * @param type $email
     * @param type $displayName
     * @return array
     * @todo  8.LIST USERS API
     The Get User Details API is used to retrieve the details for 1 or list of user Ids. Clients must use a valid session token
     obtained with a previous call to the Authenticate API.
     The URL used to access Get User Detail API is (only active user is retrieved-able):
     â—� Production â€“ http://api.account.insing.com/users
     â—� Test â€“ http://api.account.test.insing.com/users
     HTTP method supported is: GET.
     */
    public function getListUsers($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/users";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * List user options API, checking if user is owner.
     *
     *   'filter_user_id' => $userId,
     *   'filter_key' => 'business',
     *   'use_cache' => 'N'
     *
     * @param array $params
     * @param string $message
     * @param string $channelName
     * @return multitype:|mixed
     */
    public function getUserOptions($inputs, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/user_options";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    public function getMerchantSession($inputs, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/merchant_session";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListBookmarks($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/bookmarks";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            if($result['status'] == 200) {
                //$this->logger->addInfoToFile("accountapi.getListBookmarks >> success");
            }
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListFolowers($user_id, $inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/followers/$user_id";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Minh Tong
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListFollowingUser($user_id, $inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/following/$user_id";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Minh Tong
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListFolowing($user_id, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/following_ids/$user_id";
            $method = "get";
            $params["session_token"] = $session_token;
            $params["use_cache"] = 'N';
            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }


    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function postCreateUser($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/user";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            /**
             * array (size=2)
             'status' => int 200
             'data' =>
             array (size=2)
             'user_id' => int 318488
             'token' => string '2a353114419a2dd942520b11d566f049' (length=32)
             */
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $id
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function putEditUser($id, $inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/user/$id";
            $method = "put";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function  requestGetNewAndResetPassword($inputs, $is_request_reset = true, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                //$this->logger->addInfoToFile("accountapi.requestResetPassword >> empty session_token, inputs: ". json_encode($inputs));
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = ($is_request_reset) ? "/request_reset_password" : "/reset_password" ;
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function login($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/login";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function  loginFacebook($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/fb_login";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function  createFacebookUser($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();

        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/user/facebook";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * @author Dat Dao <john.doe@example.com>
     * @param type $inputs
     * @param type $message
     * @return type
     * Eatability
     */
    public function listChannelsApi($inputs, &$message) {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();//get current session token
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/channels";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;
            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * Create bookmark
     *
     * @author Thu Nguyen
     * @param array $params
     * @return array $result
     */
    public function createUserBookmark($params, $channelName = ''){
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/bookmark";
            $method = "post";
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * Delete bookmark
     *
     * @author Thu Nguyen
     * @param array $params
     * @return array $result
     */
    public function deleteUserBookmark($id, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/bookmark/" . $id;
            $method = "delete";
            $params["session_token"] = $session_token;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * Follow User
     *
     * @author Minh Tong
     * @param $onwer_id , $follow_id ,$is_follow = true
     * desc if $is_follow = true function run Follow User and else.
     * @return array $result
     */
    public function followAndUnFollowUser($onwer_id, $follow_id, $is_follow = true, &$message, $channelName = ''){
        $params = array();

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = ($is_follow === true)  ? "/follow_user" : "/unfollow_user";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["follower_id"] = $onwer_id;
            $params["following_id"] = $follow_id;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * Bookmark User
     *
     * @author Minh Tong
     * @param $user_id , $business_id , $type_id = '2' default bussiness
     * @return array $result
     */
    public function bookmarkBusiness($user_id, $business_id, $type_id = '2', &$message = null, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/bookmark" ;

            $method =  "post" ;

            $params["session_token"] = $session_token;
            $params["user_id"] = $user_id;
            $params["entity_id"] = $business_id;
            $params["type_id"] = $type_id;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * Bookmark User
     *
     * @author Tri Van
     * @param $userId , $business_id , $type_id = '2' default bussiness
     * @return array $result
     */
    public function createBookmark($userId, $entityId, $typeId) {
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                return array();
            }

            $baseUriUrl = "/bookmark";
            $method = "post";
            $params["session_token"] = $session_token;
            $params["user_id"] = $userId;
            $params["entity_id"] = $entityId;
            $params["type_id"] = $typeId;
            $params["channel"] = $this->listBookmarkChanels[$this->countryCode];

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($message);
        }
        return array();
    }

    /**
     * Delelte Bookmark User
     *
     * @author Minh Tong
     * @param $bookmark_id
     * @return array $result
     */
    public function deleteBookMark($bookmark_id, &$message = null, $channelName = '') {
        $params = array();

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);

                return array();
            }

            $baseUriUrl = "/bookmark/$bookmark_id";
            $method = "delete";
            $params["session_token"] = $session_token;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * UnBookmark User
     *
     * @author Minh Tong
     * @param $bookmark_id
     * @return array $result
     */
    public function unBookmarkBusiness($bookmark_id, &$message = null, $channelName = ''){
        $params = array();

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/bookmark/$bookmark_id" ;

            $method =  "delete" ;

            $params["session_token"] = $session_token;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * UnBookmark User
     *
     * @author Minh Tong
     * @param $bookmark_id
     * @return array $result
     */
    public function getUserListBookmark($params, &$message = null, $channelName = ''){

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/bookmarks" ;

            $method =  "get" ;
            
            $params = $params;
            $params["session_token"] = $session_token;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * Request Active User
     *
     * @author Minh Tong
     * @param $user_id
     * @return array $result
     */
    public function requestActiveUser($email, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/resend_verify_user";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["login"] = $email;
            $params["reject_resend_in"] = 0;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {

            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * Request Active User
     *
     * @author Minh Tong
     * @param $user_id
     * @return array $result
     */
    public function getActiveUser($email, $code, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);

                return array();
            }

            $baseUriUrl = "/verify_user";

            $method = "get";
            $params["session_token"] = $session_token;
            $params["login"] = $email;
            $params["verify_code"] = $code;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * Request Active User
     *
     * @author Minh Tong
     * @param $user_id
     * @return array $result
     */
    public function searchListUser($params, &$message, $channelName = ''){

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/user/search";

            $method = "get";
            $params = $params;
            $params["session_token"] = $session_token;
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {

            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * check nickname is exists
     *
     * @author Minh Tong â™«
     * @param $nickname
     * @return array $result
     */
    public function checkNicknameExits($nickname, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);

                return array();
            }

            $baseUriUrl = "/check_nickname_availability";

            $method = "get";
            $params["session_token"] = $session_token;
            $params["nickname"] = $nickname;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {

            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request forget password
     *
     * @author Minh Tong â™«
     * @param $nickname
     * @return array $result
     */
    public function requestForgotPassword($email, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/request_reset_password";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["login"] = $email;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {

            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request forget password
     *
     * @author Minh Tong â™«
     * @param $nickname
     * @return array $result
     */
    public function requestResetPassword($email, $token, $password, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/reset_password";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["login"] = $email;
            $params["token"] = $token;
            $params["password"] = $password;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {

            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request create MediaId
     * @author Minh Tong â™«
     * @param String $userId, $mediaUrl,
     *        Int $mediaType default 1
     * @return array $result
     */
    public function createMediaId($userId, $mediaUrl, $mediaType = 1, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/media";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["user_id"] = $userId;
            $params["file_path"] = $mediaUrl;
            $params["media_type"] = $mediaType;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request create MediaId
     * @author Minh Tong â™«
     * @param String $userId, $mediaUrl,
     *        Int $mediaType default 1
     * @return array $result
     */
    public function requestVerifyPhoneNumber($userId, $phoneNumber, $sms_message, &$message, $channelName = ''){
        $params = array();

        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/request_phone_verification";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["user_id"] = $userId;
            $params["phone_number"] = $phoneNumber;
            $params["sms_message"] = $sms_message;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request create MediaId
     * @author Minh Tong â™«
     * @param String $userId, $mediaUrl,
     *        Int $mediaType default 1
     * @return array $result
     */
    public function verifyPhoneNumber($userId, $phoneNumber, $code, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/verify_phone_number";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["user_id"] = $userId;
            $params["phone_number"] = $phoneNumber;
            $params["code"] = $code;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * request create MediaId
     * @author Minh Tong â™«
     * @param String $userId, $mediaUrl,
     *        Int $mediaType default 1
     * @return array $result
     */
    public function linkFacebookAccount($userId, $access_token, &$message, $channelName = ''){
        $params = array();
        try {
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }

            $baseUriUrl = "/link_fb";

            $method = "post";
            $params["session_token"] = $session_token;
            $params["user_id"] = $userId;
            $params["access_token"] = $access_token;

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);

            $result = json_decode($result, true);

            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    public function getOneUserByEmailAndUserIsNotActive($email){
        $result = null;
        try {
            $inputs = array("filter_status" => 0,'filter_login_email' => $email, 'use_cache' => 'N');

            $result = $this->searchListUser($inputs, $message,'');
            if(isset($result['data']) && count($result['data'] > 0) ) {
                $result = isset($result['data'][0]) ? (object)$result['data'][0] : null;
            } else {
                $result = null;
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $result;
    }


    public function getOneUserByEmail($email, $getCache = true){
        $result = null;
        try {
            $cacheKey = self::CACHE_KEY_GET_USER_BY_EMAIL . $email;
            $result = $this->container->get('hgw.cache')->fetch($cacheKey);
            if (empty($result) || !$getCache) {
                $inputs = array("filter_login" => $email,'include_following' => -1,'include_follower' => -1 );
                if(!$getCache) {
                    $inputs['use_cache'] = 'N';
                    $inputs['filter_status'] = '0|1';
                }
                $result = $this->getListUsers($inputs, $message);
                if(isset($result['data']) && count($result['data'] > 0) ) {
                    $result = isset($result['data'][0]) ? (object)$result['data'][0] : null;
                    if($getCache) {
                        $this->container->get('hgw.cache')->save($cacheKey, $result);
                    }
                } else {
                    $result = null;
                }
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $result;
    }

    public function getOneUserById($id, $isForce = false){
        $result = null;
        try {
            $cacheKey = self::CACHE_KEY_GET_USER_BY_ID . $id;
            if($isForce == false){
                $result = $this->container->get('hgw.cache')->fetch($cacheKey);
            }
            if (empty($result)) {
                $inputs = array("filter_user_ids" => $id , 'use_cache' => 'N' );
                $result = $this->getListUsers($inputs, $message);
                if(isset($result['data']) && count($result['data'] > 0) ) {
                    $result = isset($result['data'][0]) ? (object)$result['data'][0] : null;
                    $this->container->get('hgw.cache')->save($cacheKey, $result);
                } else {
                    $result = null;
                }
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $result;
    }

    public function getOneUserByUserName($username){
        $result = null;
        try {
            $cacheKey = self::CACHE_KEY_GET_USER_BY_USERNAME . $username;
            $result = $this->container->get('hgw.cache')->fetch($cacheKey);

            if (empty($result)) {
                $inputs = array("filter_user_name" => $username);
                $result = $this->searchListUser($inputs, $message, $channelName = 'Eatability');

                if(isset($result['data']) && count($result['data'] > 0) ) {
                    $result = isset($result['data'][0]) ? (object)$result['data'][0] : null;
                    $this->container->get('hgw.cache')->save($cacheKey, $result);
                } else {
                    $result = null;
                }
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $result;
    }


    public function getListUserEatability($user_ids){
        $cacheKey = 'getListUserMoreInfoEatability_{user_id}';
        $userInfo = null;
        $userCallAPI = null;
        try {
            //Get user from cache
            foreach($user_ids as $cacheValue => $userId){
                $cacheUserKey = str_replace('user_id', $userId, $cacheKey);
                $users_info = $this->container->get('hgw.cache')->fetch($cacheUserKey);
                if ($users_info) {
                    $userInfo[] = $users_info;
                } else {
                    $userCallAPI[] = $userId;
                }
            }

            if (!empty($userCallAPI)) {
                $inputs = array('filter_user_ids' => implode("|", $userCallAPI));
                $userResult = $this->getListUsers($inputs, $message);

                if ( isset($userResult['data']) && !empty($userResult['data'])) {
                    foreach($userResult['data'] as $key => $eatabilityUser){
                        $userInfo[] = $eatabilityUser;
                        $cacheKeyUser = str_replace("user_id", $eatabilityUser['id'], $cacheKey);
                        $this->container->get('hgw.cache')->save($cacheKeyUser, $eatabilityUser);
                    }
                }
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $userInfo;
    }


    public function getListUserFollowers($user_id, $isForce = false, $maxResult = 20, $filter_user = null, $page = 1, $isRefresh = false, &$total = 0) {
        $countryCode = $this->countryCode;
        $cacheKey = 'getListUserFolowersRestfulAPI' . $countryCode . $page;
        $userInfo = null;
        try {
            if ($isForce == false) {
                $userInfo = $this->container->get('hgw.cache')->fetch($cacheKey);

                if(isset($userInfo['total']) && !empty($userInfo['total'])){
                    $total =  $userInfo['total'];
                }
                if(isset($userInfo['data']) && !empty($userInfo['data'])){
                    return  $userInfo['data'];
                }
            }

            $inputs = array('per_page' => $maxResult,'page' => $page, 'sort_reverse' => 'Y', 'sort_fields' => 'created_date', 'use_cache' => 'N' );

            if (!empty($filter_user))  {
                $inputs = array_merge($inputs, array("filter_follower_id" => $filter_user));
            }

            $userResult = $this->getListFolowers($user_id, $inputs, $message);

            if ( isset($userResult['data']) && !empty($userResult['data'])) {
                $total = $userResult['total'];
                if ($isRefresh) {
                    //clear ALL Cache key when force data
                    $totalPage = ceil( $total / $maxResult );
                    for ($i = 1 ; $i < $totalPage + 1 ; $i++) {
                        $cacheKeyDel = 'getListUserFolowersRestfulAPI' . $countryCode . $i;
                        $this->container->get('hgw.cache')->delete($cacheKeyDel);
                    }
                }

                $this->container->get('hgw.cache')->save($cacheKey, $userResult);
                $userInfo = $userResult['data'];
            }

            return $userInfo;
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $userInfo;
    }

    public function getListFollowing($user_id, $isForce = false, $maxResult = 20, $filter_user = null, $page = 1, $isRefresh = false, &$total = 0) {
        $countryCode = $this->countryCode;
        $cacheKey = 'getListUserFolowingRestfulAPI' . $countryCode . $page;
        $userInfo = null;
        try {
            if ($isForce == false) {
                $userInfo = $this->container->get('hgw.cache')->fetch($cacheKey);

                if(isset($userInfo['total']) && !empty($userInfo['total'])){
                    $total =  $userInfo['total'];
                }
                if(isset($userInfo['data']) && !empty($userInfo['data'])){
                    return  $userInfo['data'];
                }
            }

            $inputs = array('per_page' => $maxResult,'page' => $page, 'sort_reverse' => 'Y', 'sort_fields' => 'created_date', 'use_cache' => 'N' );

            if (!empty($filter_user))  {
                $filter_user = implode('|', $filter_user);
                $inputs = array_merge($inputs, array("filter_following_id" => $filter_user));
            }

            $userResult = $this->getListFollowingUser($user_id, $inputs, $message);

            if ( isset($userResult['data']) && !empty($userResult['data'])) {
                $total = $userResult['total'];
                if ($isRefresh) {
                    //clear ALL Cache key when force data
                    $totalPage = ceil( $total / $maxResult );
                    for ($i = 1 ; $i < $totalPage + 1 ; $i++) {
                        $cacheKeyDel = 'getListUserFolowingRestfulAPI' . $countryCode . $i;
                        $this->container->get('hgw.cache')->delete($cacheKeyDel);
                    }
                }

                $this->container->get('hgw.cache')->save($cacheKey, $userResult);
                $userInfo = $userResult['data'];
            }

            return $userInfo;
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $userInfo;
    }

    public function getListUserFollowing($user_id, $isForce = false) {

        $cacheKey = 'getListUserFolowingAPIRestful' . $this->countryCode . $user_id;
        $userInfo = null;
        try {
            if ($isForce == false) {
                $userInfo = $this->container->get('hgw.cache')->fetch($cacheKey);
                if(!empty($userInfo)){
                    return  $userInfo;
                }
            }

            $userResult = $this->getListFolowing($user_id, $message);

            if ( isset($userResult['data']) && !empty($userResult['data'])) {
                $this->container->get('hgw.cache')->save($cacheKey, $userResult['data']);
                $userInfo = $userResult['data'];
            }

            return $userInfo;
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $userInfo;
    }

    public function getUserListBookmarkFromAPI($user_ids, $business_id, $bookmark_type = 2){
        $userInfo = $message = null;
        try {
            $inputs = array('filter_user_id' => $user_ids , "filter_entity_ids" => $business_id,
                     'filter_type_id' => $bookmark_type);
            $userResult = $this->getUserListBookmark($inputs, $message, 'Eatability');

            if(isset($userResult['data']) && $userResult['data']) {
                $userInfo = $userResult['data'];
            }
        }
        catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }
        return $userInfo;
    }

    public function getBookMarkUser($user_ids, $business_id, $bookmarkType) {
        $userInfo = null;
        $message = null;
        try {

            $inputs = array('filter_user_id' => $user_ids, "filter_entity_ids" => $business_id, 'filter_type_id' => $bookmarkType);
            $userResult = $this->getUserListBookmark($inputs, $message, 'Eatability');

            if (isset($userResult['data']) && !empty($userResult['data'])) {
                $userInfo = (object) $userResult;
            }
        } catch (Exception $exc) {
            $this->logger->exp_err($exc);
        }

        return $userInfo;
    }

    /**
     * 20 REQUEST UPDATE LOGIN API
     * https://api.accounts.insing.com/request_update_login
     *
     * @author Tuan Tran
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function updateLoginId($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/request_update_login";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }

    /**
     * 21 VERIFY UPDATED LOGIN API
     * https://api.accounts.insing.com/verify_update_login
     *
     * @author Tuan Tran
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function verifyUpdateLogin($inputs, &$message, $channelName = '') {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/verify_update_login";
            $method = "post";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);

        }
        return array();
    }

    /**
     * 21 VERIFY UPDATED LOGIN API
     * https://api.accounts.insing.com/verify_update_login
     *
     * @author Tuan Tran
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListUserOption() {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            $inputs = array("sort_reverse" => "Y", "sort_fields" => "login");
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", 402);
            }
            #======st input=========#
            $baseUriUrl = "/user_options";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#

            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);

            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }


    /**
     * @author VuTranQ
     * @param type $inputs
     * @param type $message
     * @return type
     */
    public function getListBookmarksGroup($inputs, &$message) {
        $message = "";
        $params = array();
        try {
            //st logic call api
            $session_token = $this->getSessionToken();
            if (empty($session_token)) {
                $message = "empty session_token";
                throw new \Exception("Empty session_token", RestfulAPIHelper::HTTP_PAYMENT_REQUIRED);
            }
            #======st input=========#
            $baseUriUrl = "/bookmark_sublists";
            $method = "get";
            $params = $inputs;
            $params["session_token"] = $session_token;

            #======ed input=========#
            $result = $this->getAccountApiBase()->runApi($baseUriUrl, $method, $params, true);
            $result = json_decode($result, true);
            if($result['status'] == RestfulAPIHelper::HTTP_OK) {
                //$this->logger->addInfoToFile("accountapi.getListBookmarks >> success");
            }
            //ed logic call api
            return $result;
        } catch (\Exception $e) {
            $message = "Call api fail, message: " . $e->getMessage();
            $this->logger->exp_err($e);
        }
        return array();
    }
    
    /**
     * 
     */
    public function checkOwneruser ($user_id)
    {
        $ret = array();
        if (!empty($user_id)) {
            $params = array(
                'filter_user_id' => $user_id,
                'filter_key' => 'business',
                'use_cache' => 'N',
            );
            
            $ret = $this->getUserOptions($params);
        }
        
        return $ret;
    }

}

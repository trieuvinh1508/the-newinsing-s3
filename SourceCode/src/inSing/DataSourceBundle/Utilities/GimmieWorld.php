<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace inSing\DataSourceBundle\Utilities;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

require_once __DIR__ . '/GimmieWorldSDK/Gimmie.sdk.php';

class GimmieWorld {

    protected $em;
    protected $container;
    protected $gimmie = null;

    protected static $gwTriggerCommandPattern = 'cd .. && php app/console inSing:admin:gw:trigger [userId] [eventName] [resourceUid] -e [env] > /dev/null &';
    protected static $gwLoginCommandPattern = 'cd .. && php app/console inSing:admin:gw:login [userId] -e [env] > /dev/null &';
    
    //st update new config Dat.Dao 06-04-2015    
    //category ~ type of place(old logic)
    public static $CATEGORY_MAPPING_FOR_SG = array(
        'Food & Drink > Cafes'          => 'tried_review_cafe',
        'Food & Drink > Hawkers'        => 'tried_review_hawker',
        'Food & Drink > Restaurants'    => 'tried_review_restaurant',
        ''                              => 'tried_review_mamak',#don't have
    );
    
    public static $CATEGORY_MAPPING_FOR_MY = array(
        'Food & Drink > Cafes'          => 'tried_review_cafe',
        'Food & Drink > Hawkers'        => 'tried_review_hawker',
        'Food & Drink > Restaurants'    => 'tried_review_restaurant',
        ''                              => 'tried_review_mamak',#don't have
    );
    
    public static $CATEGORY_MAPPING_FOR_AU = array(
        'Food & Drink > Cafes'          => 'tried_review_cafe',
        'Food & Drink > Hawkers'        => 'tried_review_hawker',#clone form SG
        'Food & Drink > Restaurants'    => 'tried_review_restaurant',
        ''                              => 'tried_review_mamak',#don't have
    );
    //suitable for
    public static $SUITABLE_FOR_MAPPING_FOR_SG = array(
        'Brunch'                        => 'tried_review_brunch',
        'Buffet'                        => 'tried_review_buffet',
        'Cheap Eat/Budget'              => 'tried_review_budget',
        'Children/Family'               => 'tried_review_family',
        'Hidden Find'                   => 'tried_review_hiddenfind',
        'High Tea'                      => 'tried_review_hightea',
        'Large Groups'                  => 'tried_review_largegroups',
        'Pet Friendly'                  => 'tried_review_petfriendly',
        'Romance/First Dates'           => 'tried_review_romance',
        'Supper'                        => 'tried_review_supper',
    );
    
    public static $SUITABLE_FOR_MAPPING_FOR_MY = array(
        'Brunch'                        => 'tried_review_brunch',
        'Buffet'                        => 'tried_review_buffet',
        'Cheap eat / budget'              => 'tried_review_budget',
        'Children / Family'               => 'tried_review_family',
        'Hidden Find'                   => 'tried_review_hiddenfind', #clone from SG
        'Hi tea'                      => 'tried_review_hightea',
        'Large Groups / Gathering'                  => 'tried_review_largegroups',
        'Pet Friendly'                  => 'tried_review_petfriendly', #clone from SG
        'Romance / First dates'           => 'tried_review_romance',
        'Supper'                        => 'tried_review_supper',
    );
    
    public static $SUITABLE_FOR_MAPPING_FOR_AU = array(
        'Brunch'                        => 'tried_review_brunch',
        'Buffet'                        => 'tried_review_buffet',
        'Cheap Eat/Budget'              => 'tried_review_budget',
        'Children/Family'               => 'tried_review_family',
        'Hidden Find'                   => 'tried_review_hiddenfind',
        'High Tea'                      => 'tried_review_hightea',
        'Large Groups'                  => 'tried_review_largegroups',
        'Pet Friendly'                  => 'tried_review_petfriendly',
        'Romance/First Dates'           => 'tried_review_romance',
        'Supper'                        => 'tried_review_supper',
    );
    
    //Cuisine
    public static $CUISINE_MAPPING_FOR_SG = array(
        'Chinese'           => 'tried_review_chinese',
        'Desserts'          => 'tried_review_desserts',
        'Dim Sum'           => 'tried_review_dimsum',
        'French'            => 'tried_review_french',
        'Halal'             => 'tried_review_halal',
        'Indian'            => 'tried_review_indian',
        'Italian'           => 'tried_review_italian',
        'Japanese'          => 'tried_review_japanese',
        'Korean'            => 'tried_review_korean',
        'Malaysian'         => 'tried_review_malaysian',
        'Singaporean'       => 'tried_review_singaporean',
        'Vegetarian'        => 'tried_review_vegetarian',
        'Western'           => 'tried_review_western',
    );
    
    public static $CUISINE_MAPPING_FOR_MY = array(
        'Chinese'           => 'tried_review_chinese',
        'Desserts'          => 'tried_review_desserts',
        'Dim Sum'           => 'tried_review_dimsum',
        'French'            => 'tried_review_french',
        'Halal'             => 'tried_review_halal',
        'Indian'            => 'tried_review_indian',
        'Italian'           => 'tried_review_italian',
        'Japanese'          => 'tried_review_japanese',
        'Korean'            => 'tried_review_korean',
        'Malaysian'         => 'tried_review_malaysian',
        'Singaporean'       => 'tried_review_singaporean',
        'Vegetarian'        => 'tried_review_vegetarian',
        'Western'           => 'tried_review_western',
    );
    
    public static $CUISINE_MAPPING_FOR_AU = array(
        'Chinese'           => 'tried_review_chinese',
        'Desserts'          => 'tried_review_desserts', #clone from SG
        'Dim Sum'           => 'tried_review_dimsum', #clone from SG
        'French'            => 'tried_review_french',
        'Halal'             => 'tried_review_halal', #clone from SG
        'Indian'            => 'tried_review_indian',
        'Italian'           => 'tried_review_italian',
        'Japanese'          => 'tried_review_japanese',
        'Korean'            => 'tried_review_korean',
        'Malaysian'         => 'tried_review_malaysian',
        'Singaporean'       => 'tried_review_singaporean',
        'Vegetarian'        => 'tried_review_vegetarian',
        'Western'           => 'tried_review_western', #clone from SG
    );
    //ed update new config Dat.Dao 06-04-2015
    
    
//    public static $mappingSuitableFors = array(
//        'romance / first dates' => 'tried_review_romance',
//        'supper'                => 'tried_review_supper',
//        'hidden find'           => 'tried_review_hiddenfind',
//        'large groups / gathering' => 'tried_review_largegroups',
//        'brunch'                => 'tried_review_brunch',
//        'buffet'                => 'tried_review_buffet',
//        'pet friendly'          => 'tried_review_petfriendly',
//        'cheap eat / budget'    => 'tried_review_budget',
//        'children / family'     => 'tried_review_family',
//        'high tea'              => 'tried_review_hightea',
//    );
//
//    public static $mappingTypeOfPlaces = array(
//        'hawker'    => 'tried_review_hawker',
//        'cafe'      => 'tried_review_cafe',
//        'restaurant'=> 'tried_review_restaurant',
//        'mamak'     => 'tried_review_mamak',
//    );
//    
//    public static $mappingCuisines = array(
//        'chinese'   => 'tried_review_chinese',
//        'indian'    => 'tried_review_indian',
//        'japanese'  => 'tried_review_japanese',
//        'korean'    => 'tried_review_korean',
//        'malaysian' => 'tried_review_malaysian',
//        'vegetarian'=> 'tried_review_vegetarian',
//        'western'   => 'tried_review_western',
//        'halal'     => 'tried_review_halal',
//        'dim sum'   => 'tried_review_dimsum',
//        'desserts'  => 'tried_review_desserts',
//        'italian'   => 'tried_review_italian',
//        'french'    => 'tried_review_french',
//        'singaporean' => 'tried_review_singaporean',
//    );
    
    const GIMMIE_WORLD_ANONYMOUS_ID = "guest:randomid";
    const TOP_20_POINTS_DATA_CACHE_KEY = 'TOP_20_POINTS_DATA_CACHE_KEY';
    
    const TOP_20_POINTS_CACHE_TIME = 600; //10 mins
    
    const ALL = 'ALL';
    const LAST_WEEK = 'LAST_WEEK';
    const THIS_WEEK = 'THIS_WEEK';
    const PAST_30_DAYS = 'PAST_30_DAYS';
    const PAST_7_DAYS = 'PAST_7_DAYS';
    
    const GET_ALL_BADGES_DATA_CACHE_KEY = 'GET_ALL_BADGES_DATA_CACHE_KEY_GIMMIE_WORLD';
    const GET_ALL_BADGES_DATA_CACHE_TIME = 86400; //1 day
    const GET_OBTAIN_BADGES_DATA_CACHE_KEY = 'GET_OBTAIN_BADGES_DATA_CACHE_KEY_GIMMIE_WORLD';
    const GET_OBTAIN_BADGES_DATA_CACHE_TIME = 600;
    const GET_USER_BADGES_DATA_CACHE_KEY = 'GET_USER_BADGES_DATA_CACHE_KEY';
    const GET_USER_BADGES_DATA_CACHE_TIME = 600;
    const GET_USER_POINT_DATA_CACHE_KEY = 'GET_USER_POINT_DATA_CACHE_KEY';
    const GET_USER_POINT_DATA_CACHE_TIME = 600;
    const GET_USER_LEVEL_DATA_CACHE_KEY = 'GET_USER_LEVEL_DATA_CACHE_KEY';
    const GET_USER_LEVEL_DATA_CACHE_TIME = 600;
    
    private $countryCode;
    private $logger;
        
    public function __construct(EntityManager $em, ContainerInterface $container) {
        $this->em = $em;
        $this->container = $container;
        
        $countryCode = $this->container->getParameter('country_code');
        $this->countryCode = $countryCode;
        $GWChannel = $this->container->getParameter('gimmie_world_game');

        $gimmieWorldConsumerKey = isset($GWChannel[$countryCode]['gimmie_world_consumer_key']) ? $GWChannel[$countryCode]['gimmie_world_consumer_key'] : null ;
        $gimmieWorldConsumerSerect = isset($GWChannel[$countryCode]['gimmie_world_consumer_serect']) ? $GWChannel[$countryCode]['gimmie_world_consumer_serect'] : null ; 
        $this->logger = $this->container->get('monolog.logger.gw');
        
        //revert code #575, disable for SG
        if($this->countryCode != Constant::COUNTRY_CODE_SINGAPORE) {
            $this->gimmie = new GimmieWorldSDK\Gimmie($gimmieWorldConsumerKey, $gimmieWorldConsumerSerect);
        }
    }

    /**
     * 'staging_user:1234' to '1234' 
     * @param type $gwId
     * @return string
     */    
    private function gwIdToId($gwId) {
        $parseGwId = explode(':', $gwId);
        if (!empty($parseGwId[1])) {
            return $parseGwId[1];
        }
        return '';
    }

    /**
     * '1234' to 'staging_user:1234' 
     * @param type $userId
     * @return string
     */
    private function idToGwId($userId, $checkTrackingNewPrefix) {
        $gimmieId = $userId;
        if ($userId != self::GIMMIE_WORLD_ANONYMOUS_ID) {
            $countryCode = $this->countryCode;
            $GWChannel = $this->container->getParameter('gimmie_world_game');

            if($checkTrackingNewPrefix) {
                $GWFormatId = isset($GWChannel[$countryCode]['gimmie_world_format_id'])
                ? $GWChannel[$countryCode]['gimmie_world_format_id'] : null;
                $gimmieId = str_replace('[id]', $userId, $GWFormatId);
                $this->logger->addInfo("UserId=[$userId]: Trigger with new prefix - ", array($gimmieId));
            }
            else {
                //trigger with old prefix
                $GWFormatId = isset($GWChannel[$countryCode]['gimmie_world_old_format_id'])
                ? $GWChannel[$countryCode]['gimmie_world_old_format_id'] : null;
                $gimmieId = str_replace('[id]', $userId, $GWFormatId);
                $this->logger->addInfo("UserId=[$userId]: Trigger with old prefix - ", array($gimmieId));
            }
        }
        return $gimmieId;
    }

    public function setUser($userId, $checkTrackingNewPrefix = true) {
        if( is_null($this->gimmie) ) {
            return true;
        }
        $gimmieId = $this->idToGwId($userId, $checkTrackingNewPrefix);
        $this->gimmie->set_user($gimmieId);
    }

    public function loginWorker($userId, $old_uid = '', $country = '', $name = '', $email = '') {  
        //check code via Vu Tran
        if( is_null($this->gimmie) ) {
            return array();
        }
        $checkTrackingNewPrefix = true;
        $this->changeUserIdMY($userId, $checkTrackingNewPrefix);
        if(!$userId) {
        	return array();
        }
        $this->setUser($userId, $checkTrackingNewPrefix);
        $result = array();
        $result = $this->gimmie->login($old_uid, $country, $name, $email);
        //update 
        //$this->updateResult($res);
        $this->logger->addInfo("UserId=[$userId]", $result);
        return $result;
    }
    
    public function login($userId) {
        if (self::isWindowsOS()){ 
            $this->loginWorker($userId);
        } else {
            $this->runGwCommand(self::$gwLoginCommandPattern, array(
                '[userId]' => $userId,
                '[env]'    => $this->container->get('kernel')->getEnvironment()
            ));
        }
        return true;
    }
    
    private function prepareUsersInfo($results) {        
        $res = array();        
        if (!empty($results) && count($results) > 0) {
            for ($i = 0, $n = count($results); $i < $n; $i++) {                
                $avatarUrl = '';
                if(!empty($results[$i]['profileImageFileName'])) {
                    $avatarUrl = $this->container->get('common.utils')->getUrlOnCDN($results[$i]['id'], $results[$i]['profileImageFileName']);   
                }                
                $results[$i]['avatar'] = $avatarUrl;                
                $res[$results[$i]['id']] = $results[$i];
                unset($results[$i]);
            }
        }
        return $res;
    }

    private function top20pointsNoneCache($duration = self::ALL, $userId) {
        /** format
         *  [0] => Array
          (
          [external_uid] => staging_user:11887
          [name] =>
          [rank] => 1
          [value] => 7045
          [id] => 11887
          )
         */
        if( is_null($this->gimmie)) {
            return false;
        }
        $this->setUser($userId);
        switch ($duration) {
            case self::ALL:
                $response = $this->gimmie->top20points();                
                break;
            case self::LAST_WEEK:
                $response = $this->gimmie->top20points_past_week();
                break;
            case self::THIS_WEEK:
                $response = $this->gimmie->top20points_this_week();
                break;
            case self::PAST_30_DAYS:
                $response = $this->gimmie->top20points_past_30_days();
                break;
            case self::PAST_7_DAYS:
                $response = $this->gimmie->top20points_past_7_days();
                break;
        }
        
        if (is_array($response)) {
            if (isset($response['response']['players']) && count($response['response']['players']) > 0) {
                return $response['response']['players'];
            }
            //$this->logger->info($duration, $response);
            //Debug: collect API calls
            
            //$this->container->get('common.utils')->collectApiCall('GW', self::TOP_20_POINTS_DATA_CACHE_KEY . $duration . $userId . $this->countryCode);
        }
        return false;
    }

    public function top20points($duration = self::ALL, $userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {        
        $cache = $this->container->get('hgw.cache');
        $cache_time = self::TOP_20_POINTS_CACHE_TIME;
        
        $cache_key = self::TOP_20_POINTS_DATA_CACHE_KEY . $duration . $userId . $this->countryCode;

        $users = $cache->fetch($cache_key);
        if (empty($users)) {
            //get users
            $users = $this->top20pointsNoneCache($duration, $userId);
            //save users in cache
            $cache->save($cache_key, $users, $cache_time);
        }
        return $users;
    }

    public function top20pointsLastWeek($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
        return $this->top20points(self::LAST_WEEK, $userId);
    }

    public function top20pointsThisWeek($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
        return $this->top20points(self::THIS_WEEK, $userId);
    }

    public function top20pointsPast30Days($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
        return $this->top20points(self::PAST_30_DAYS, $userId);
    }

    public function top20pointsPast7Days($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
        return $this->top20points(self::PAST_7_DAYS, $userId);
    }

    private function getAllBadgesNoneCache($userId) {
        if(is_null($this->gimmie)) {
            return false;
        }        
        $this->setUser($userId);        
        $result = $this->gimmie->badges();
//         $this->logger->addInfo('badges', $result);
        if (is_array($result)) {
            if (isset($result['response']['badges']) && count($result['response']['badges']) > 0) {                
                return $result['response']['badges'];
            }            
        }
        return false;
    }

    private function getAllBadges($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
        $cache = $this->container->get('hgw.cache');
        
        $cache_key = self::GET_ALL_BADGES_DATA_CACHE_KEY . $userId . $this->countryCode;
        $cache_time = self::GET_ALL_BADGES_DATA_CACHE_TIME;
        $badgesFromApi = $cache->fetch($cache_key);

        if (empty($badgesFromApi)) {
            $badgesFromApi = $this->getAllBadgesNoneCache($userId);

            $cache->save($cache_key, $badgesFromApi, $cache_time);
        }

        return $badgesFromApi;
    }

//     private function getUserBadges($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
//         //get real data
//         $badgesFromApi = $this->getAllBadges($userId);
//         //get badges from DB
//         $badgesFromDB = $this->getBadgesFromDatabase($userId);
        
//         //prepare result add key "obtain" if user obtained it
//         $userBadges = $this->prepareResultBadges($badgesFromApi, $badgesFromDB);

//         $userBadges = $this->sortingBadgesByGroup($userBadges);

//         return $userBadges;
//     }

//     public function getAllUserBadges($userId = self::GIMMIE_WORLD_ANONYMOUS_ID) {
//         $cache = $this->container->get('hgw.cache');
        
//         $cache_key = self::GET_OBTAIN_BADGES_DATA_CACHE_KEY . $userId . $this->countryCode;
//         $cache_time = self::GET_OBTAIN_BADGES_DATA_CACHE_TIME;
//         $badges = $cache->fetch($cache_key);

//         if (empty($badges)) {
//             //get user badges
//             $badges = $this->getUserBadges($userId);

//             $cache->save($cache_key, $badges, $cache_time);
//         }

//         return $badges;
//     }

    /**
     * Get top user badges
     * @param int $userId
     */
//     public function getTopUserBadges($userId) {
//         return $this->getBadgesFromDatabase($userId);
//     }
    
    private function sortingBadgesByGroup($badges) {
        $results = array();
        foreach ($badges as $key => $itemGroup) {
            $results[$key] = $this->sortingArray2DByKey($itemGroup);
        }
        return $results;
    }

    private function sortingArray2DByKey($array2D) {
        $top = array();
        $bottom = array();
        foreach ($array2D as $value) {
            if ($value['obtain'] == true) {
                $top[] = $value;
            } else {
                $bottom[] = $value;
            }
        }
        foreach ($bottom as $value) {
            $top[] = $value;
        }
        return $top;
    }

    private function prepareResultBadges($badgesFromApi, $badgesFromDB) {
        $userBadges = array();
        if( empty($badgesFromApi) ) {
            return $userBadges;
        }
        foreach ($badgesFromApi as $group => $badges) {
            foreach ($badges as $subBadges) {
                foreach ($subBadges as $key => &$badge) {
                    $badge['obtain'] = false;
                    $badge['next_description'] = $badge['description'];
                    $badge['next_rule_description_html'] = $badge['rule_description_html'];
                    $badgeId = $badge['id'];
                    if ($key == 0) {
                        $selectedBadge = $badge;
                    }
                    if (!empty($badgesFromDB[$badgeId])) {
                        $badge['obtain'] = true;
                        if (isset($subBadges[$key + 1])) {
                            $badge['next_description'] = $subBadges[$key + 1]['description'];
                            $badge['next_rule_description_html'] = $subBadges[$key + 1]['rule_description_html'];
                        }
                        $selectedBadge = $badge;
                    }
                }
                $userBadges[$group][] = $selectedBadge;
            }
        }
        return $userBadges;
    }

//     private function getBadgesFromDatabase($userId) {
//         //update into DB table gw_infos        
//         $repository = $this->em->getRepository('inSingDataSourceBundle:GwInfos');
//         //$result = $repository->findOneBy(array('userId' => $userId));
//         $result = $repository->getOneByUserId($this->container, $userId);
//         if (empty($result)) {
//             return false;
//         }
//         $sBadges = $result->getBadges();
//         $badges = json_decode($sBadges, true);

//         if (!empty($badges) && count($badges) > 0) {
//             for ($i = 0, $n = count($badges); $i < $n; $i++) {
//                 $badge = $badges[$i];
//                 $badges[$badge['id']] = $badge;
//                 unset($badges[$i]);
//             }
//         }
//         return $badges;
//     }

    /**
     * Get GW user profile
     * @param string $userId
     * @return array
     */
    public function profile($userId) {
        if(is_null($this->gimmie)) {
            return false;
        }
        $this->setUser($userId);
        $result = $this->gimmie->profile();
//         $this->updateResult($userId, $result);
        $this->container->get('common.utils')->collectApiCall('GW', '$this->gimmie->profile(): '. $userId);
        return $result;
    }
    
    /**
     * Get GW user profile
     * @param string $userId
     * @return array
     */
    public function profileNotUpdateDatabase($userId) {
        if(is_null($this->gimmie)) {
            return false;
        }
        $checkTrackingNewPrefix = true;
        $this->changeUserIdMY($userId, $checkTrackingNewPrefix);
        $this->setUser($userId, $checkTrackingNewPrefix);
        $result = $this->gimmie->profile();
        return $result;
    }

    public function parseResult($result) {
        /**
         * Mapping with Database
          gw.user_node        = data["user"]
          gw.badges           = data["badges"]
          gw.number_of_badge  = data["badges"].count
          gw.stats            = data["stats"]
         */
        $number_of_badge = count($result['response']['badges']);
        //[response][user]
        $user_node = json_encode($result['response']['user']);
        //[response][badges]
        $badges = json_encode($result['response']['badges']);
        //[response][stats]
        $stats = json_encode($result['response']['stats']);

        $array = array(
            'number_of_badge' => $number_of_badge,
            'user_node' => $user_node,
            'badges' => $badges,
            'stats' => $stats
        );
        return $array;
    }

//     private function updateResult($userId, $result) {
//         $out = $this->parseResult($result);
//         //update into DB table gw_infos        
//         $repository = $this->em->getRepository('inSingDataSourceBundle:GwInfos');
//         return $repository->update($userId, $out['number_of_badge'], $out['user_node'], $out['badges'], $out['stats']);
//     }
    
//    private function checkResponseTrigger($userId, $arrTriggerResponse) {
//        if (isset($arrTriggerResponse['response']['actions'][0]['success'])) {            
//            //success, update database table gw_info
//            $this->profile($userId);
//            return $arrTriggerResponse['response']['actions'][0];            
//        }
//
//        return array();
//    }
    
    /**
     * http://support.gimmie.io/hc/en-us/articles/202788800-API#trigger
     * @param int $userId
     * @param string $eventName
     * @param string $resourceUid
     * @return array action
     *  Array
            (
                [player_action_id] => 11081285
                [action_type] => Award Points
                [message] => Thanks for sigining up via Facebook: +15 points !
                [points] => 15
                [limit] => 1
                [success] => 1
            )
     */
    public function triggerWorker($userId, $eventName, $resourceUid = '', $forceUpdate = true) {
        if(is_null($this->gimmie)) {
            return false;
        }        
        try {
            $checkTrackingNewPrefix = true;
            $this->changeUserIdMY($userId, $checkTrackingNewPrefix);
            if(!$userId) {
            	return false;
            }
            $this->setUser($userId, $checkTrackingNewPrefix);
            $resultApi = $this->gimmie->trigger($eventName, $resourceUid);

            $result = array();
            if (isset($resultApi['response']['actions'][0]['success'])) {
                $result = $resultApi['response']['actions'][0];
                if ($forceUpdate == true) {
                    //success, update database table gw_info
                    $this->profile($userId);
                }
            } 

            $this->logger->addInfo("UserId=[$userId]| Event=[$eventName]| Uid=[$resourceUid]:", $result);
            $this->container->get('common.utils')->collectApiCall('GW', "UserId=[$userId]| Event=[$eventName]| Uid=[$resourceUid]:");
            return $result;
        } catch (\Exception $ex) {
            $this->logger->addInfo("UserId=[$userId]| Event=[$eventName]| Uid=[$resourceUid]:, error_mesage=[{$ex->getMessage()}]");
            return array(
                'error' => $ex->getCode(),
                'message' => $ex->getMessage(),
            );
        }
    }

    /**
     * check if server is using Windows OS or not
     */
    private static function isWindowsOS() {
        if (substr(php_uname(), 0, 7) == "Windows") { 
            return true;
        } else {
            return false;
        }
    }
    
    private function runGwCommand($commandPattern, array $params) {
        if( is_null($this->gimmie)) {
            return false;
        }        
        try {
            //run background
            // >/dev/null 2>/dev/null &
            $cmd = strtr($commandPattern, $params);
            $this->logger->addInfo("Gimmieworld run command line: {$cmd}");
            exec($cmd);
        } catch (Exception $ex) {
            $this->logger->error("run command: {$cmd}: error:" . $ex->getMessage());
        }
        return null;
    }
    
    public function trigger($userId, $eventName, $resourceUid = '',  $isRunBackground = false, $forceUpdate = true) {        
        //st update gimmieworld support My,AU,SG Dat.Dao 03-04-2015
        if (self::isWindowsOS() == false && $isRunBackground == true) { //Linux and $isRunBackground == true
            $this->runGwCommand(self::$gwTriggerCommandPattern, array(
                '[userId]' => $userId,
                '[eventName]' => $eventName,
                '[resourceUid]' => $resourceUid,
                '[env]' => $this->container->get('kernel')->getEnvironment()
            ));
        } else { //Windows OR $isRunBackground == false
            //run normal                
            $this->triggerWorker($userId, $eventName, $resourceUid, $forceUpdate);
        }
        //ed update gimmieworld support My,AU,SG Dat.Dao 03-04-2015
        return true;
    }
    
//    private function runTriggerByBackground($userId, $eventName, $isRunBackground, $resourceUid = '', $forceUpdate = true) {
//    }    

    /**
     * http://support.gimmie.io/hc/en-us/articles/202788800-API#trigger
     * @param int $userId
     * @param array $events  format array('event_name1', 'event_name2', array('name' => 'event_name3', 'uid' => 'xx'))
     * @return array profile info
     */
    public function triggers($userId, array $events) {
        $checkTrackingNewPrefix = true;
        $this->changeUserIdMY($userId, $checkTrackingNewPrefix);
        if(!$userId) {
        	return array();
        }
        $this->setUser($userId, $checkTrackingNewPrefix);
        $isRunBackground = true;
        $result = array();
        for ($i = 0, $n = count($events); $i < $n; $i++ ) {
            $event = $events[$i];
            if (is_array($event)) {
                $result = $this->trigger($userId, $event['name'], $event['uid'], $isRunBackground);
            } else {
                $result = $this->trigger($userId, $event, '', $isRunBackground);
            }
        }
        return $result;
    }

    /**
     * change user id and return check tracking new prefix
     * @param unknown $userId
     * @param unknown $checkTrackingNewPrefix
     * @author tri.van
     */
    private function changeUserIdMY(&$userId, &$checkTrackingNewPrefix) {
        if($this->countryCode == Constant::COUNTRY_CODE_MALAYSIA) {
            // HGW-530: Flow trigger for old prefix and new prefix
            $userGMInfo = $this->profile($userId);
            if(isset($userGMInfo['response']['user']['awarded_points'])
                    && $userGMInfo['response']['user']['awarded_points']) {}
            else {
                //get old user Id from $userId
                $userRepository = $this->em->getRepository('inSingDataSourceBundle:Users')
                ->findOneBy(array('newMyId'=>$userId));
                if($userRepository) {
                    $userId = $userRepository->getId();
                    $checkTrackingNewPrefix = false;
                }
                else {
                   $userId = 0;
                }
            }
        }
    }

    /**
     * Submit review for restaurant with cuisines = $cuisines
     * @param int $userId
     * @param array $cuisines
     * @return array
     */
    public function triggerCuisines($userId, array $cuisines) {
        //fix bug 633: update configure for countryCode
        $this->logger->addInfo("triggerCuisines: with cuisine", $cuisines);
        $cuisinesFromConfig = $this->getCuisineForEventTrigger();
        $arrCuisinesEvents = array();
        foreach ($cuisines as $cuisine) {
            //$cuisine = strtolower($cuisine);
            if (array_key_exists($cuisine, $cuisinesFromConfig)) {
                $arrCuisinesEvents[] = $cuisinesFromConfig[$cuisine];
            }
        }
        $this->logger->addInfo("triggerCuisines: with event name", $arrCuisinesEvents);
        return $this->triggers($userId, $arrCuisinesEvents);
    }
    
    /**
     * Submit review for restaurant with typeOfPlaces = $typeOfPlaces
     * @param int $userId
     * @param array $typeOfPlaces
     * @return array
     */
    public function triggerTypeOfPlaces($userId, array $typeOfPlaces) {
        //fix bug 633: update configure for countryCode
        //fix bug 633: update type of place into category
        $this->logger->addInfo("triggerTypeOfPlaces: with category", $typeOfPlaces);
        $categoriesFromConfig = $this->getCategoriesForEventTrigger();        
        $arrTypeOfPlacesEvents = array();
        foreach ($typeOfPlaces as $typeOfPlace) {
            //$typeOfPlace = strtolower($typeOfPlace);
            if (array_key_exists($typeOfPlace, $categoriesFromConfig)) {
                $arrTypeOfPlacesEvents[] = $categoriesFromConfig[$typeOfPlace];
            }            
        }
        $this->logger->addInfo("triggerTypeOfPlaces: with event name", $arrTypeOfPlacesEvents);
        return $this->triggers($userId, $arrTypeOfPlacesEvents);
    }

    /**
     * Submit review for restaurant with suitableFor = $suitableFors
     * @param int $userId
     * @param array $suitableFors
     * @return array
     */
    public function triggerSuitableFors($userId, array $suitableFors) {
        //fix bug 633: update configure for countryCode
        $this->logger->addInfo("triggerSuitableFors: with suitable fors", $suitableFors);
        $suitableForFromConfig = $this->getSuitableForForEventTrigger();
        $arrSuitableForsEvents = array();
        foreach ($suitableFors as $suitableFor) {
            //$suitableFor = strtolower($suitableFor);
            if (array_key_exists($suitableFor, $suitableForFromConfig)) {
                $arrSuitableForsEvents[] = $suitableForFromConfig[$suitableFor];
            }
        }
        $this->logger->addInfo("triggerSuitableFors: with event name", $arrSuitableForsEvents);
        return $this->triggers($userId, $arrSuitableForsEvents);
    }
    
    public function getEarnedBadgesFromGimmieWorld($user_id){
        //Get badge earned from gimmie
        $gimmieUserInfo = $this->getBadgeByUserId($user_id);        
        //get all badge from gimmie
        $all_badge = $this->getAllBadges($user_id);
        
        //prepare result add key "obtain" if user obtained it
        $userBadges = $this->prepareResultBadges($all_badge, $gimmieUserInfo);

        $userBadges = $this->sortingBadgesByGroup($userBadges);

        return $userBadges;
        
    }
    
    public function getBadgeByUserId($userId){
        $cache = $this->container->get('hgw.cache');
        
        $cache_key = self::GET_USER_BADGES_DATA_CACHE_KEY . $userId . $this->countryCode;
        $cache_time = self::GET_USER_BADGES_DATA_CACHE_TIME;
        $badgesFromApi = $cache->fetch($cache_key);
        
        if (empty($badgesFromApi)) {
            $badgesFromApi = $this->profileNotUpdateDatabase($userId);    
            if (isset($badgesFromApi['response']['badges'])) {
                $badgesFromApi = $badgesFromApi['response']['badges'];
                if (!empty($badgesFromApi) && count($badgesFromApi) > 0) {
                    for ($i = 0, $n = count($badgesFromApi); $i < $n; $i++) {
                        $badge = $badgesFromApi[$i];
                        $badgesFromApi[$badge['id']] = $badge;
                        unset($badgesFromApi[$i]);
                    }
                }
                $cache->save($cache_key, $badgesFromApi, $cache_time);
            } else {
                $badgesFromApi = null;
            }    
        }
        return $badgesFromApi;
    }
    
    public function getPointEarnedByUserId($userId){
        try {    
            $result = array('all_time' => 0, 'this_week' => 0);
            
            $cache = $this->container->get('hgw.cache');
            
            $cache_key = self::GET_USER_POINT_DATA_CACHE_KEY . $userId . $this->countryCode;
            $cache_time = self::GET_USER_POINT_DATA_CACHE_TIME;
            $statFromApi = $cache->fetch($cache_key);
            if (empty($statFromApi)) {
                $statFromApi = $this->profileNotUpdateDatabase($userId);
                if (isset($statFromApi['response']['stats']['point'])) {
                    $statFromApi = $statFromApi['response']['stats']['point'];
                    if (!empty($statFromApi)) {
                       $result['all_time'] = isset($statFromApi['all_time']) ? $statFromApi['all_time'] : 0;
                       $result['this_week'] = isset($statFromApi['this_week']) ? $statFromApi['this_week'] : 0;
                    }
                    $cache->save($cache_key, $result, $cache_time);
                } 
            } else {
                $result = $statFromApi;
            }            
        } catch (Exception $ex) {
            $this->logger->error('Error : ' . $ex->getMessage());
        }
        return $result;
    }
    
    public function getUserLevelByUserId($userId){
        try {    
            $result = 0;
            
            $cache = $this->container->get('hgw.cache');
            
            $cache_key = self::GET_USER_LEVEL_DATA_CACHE_KEY . $userId . $this->countryCode;
            $cache_time = self::GET_USER_LEVEL_DATA_CACHE_TIME;
            $levelFromApi = $cache->fetch($cache_key);
            
            if (empty($levelFromApi)) {
                $levelFromApi = $this->profileNotUpdateDatabase($userId);
        
                if (isset($levelFromApi['response']['user'])) {
                    $levelFromApi = $levelFromApi['response']['user'];
                    if (!empty($levelFromApi)) {
                       $result = isset($levelFromApi['current_level']) ? $levelFromApi['current_level'] : 0;
                    }
                    $cache->save($cache_key, $result, $cache_time);
                } 
            } else {
                $result = $levelFromApi;
            }            
        } catch (Exception $ex) {
            $this->logger->error('Error : ' . $ex->getMessage());
        }
        return $result;
    }
    
    
    public function getCategoriesForEventTrigger() {
        $res = array();
        switch ($this->countryCode) {
            case Constant::COUNTRY_CODE_SINGAPORE:
                $res = self::$CATEGORY_MAPPING_FOR_SG;
                break;
            
            case Constant::COUNTRY_CODE_MALAYSIA:
                $res = self::$CATEGORY_MAPPING_FOR_MY;
                break;
            
            case Constant::COUNTRY_CODE_AUSTRALIA:
                $res = self::$CATEGORY_MAPPING_FOR_AU;
                break;
        }
        return $res;
        
    }
    
    public function getSuitableForForEventTrigger() {
        $res = array();
        switch ($this->countryCode) {
            case Constant::COUNTRY_CODE_SINGAPORE:
                $res = self::$SUITABLE_FOR_MAPPING_FOR_SG;
                break;
            
            case Constant::COUNTRY_CODE_MALAYSIA:
                $res = self::$SUITABLE_FOR_MAPPING_FOR_MY;
                break;
            
            case Constant::COUNTRY_CODE_AUSTRALIA:
                $res = self::$SUITABLE_FOR_MAPPING_FOR_AU;
                break;
        }
        return $res;
        
    }
    
    public function getCuisineForEventTrigger() {
        $res = array();
        switch ($this->countryCode) {
            case Constant::COUNTRY_CODE_SINGAPORE:
                $res = self::$CUISINE_MAPPING_FOR_SG;
                break;
            
            case Constant::COUNTRY_CODE_MALAYSIA:
                $res = self::$CUISINE_MAPPING_FOR_MY;
                break;
            
            case Constant::COUNTRY_CODE_AUSTRALIA:
                $res = self::$CUISINE_MAPPING_FOR_AU;
                break;
        }
        return $res;
        
    }
    
}

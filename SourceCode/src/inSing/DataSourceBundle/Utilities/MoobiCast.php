<?php

namespace inSing\DataSourceBundle\Utilities;

class MoobiCast {

    const CODE_DATA_CACHE_KEY = "CODE_DATA_CACHE_KEY_MOOBICAST"; //userid + country + phone
    const CODE_TIME_CACHE_KEY = 86400;//unit second

    //code format: {X}-{YYYYY} => [A-Z] - [0-9]*5
    protected $container;

    public function __construct($containner) {
        $this->container = $containner;
    }

    private function randomStringArray($array, $length) {
        $max = count($array) - 1;
        $srt = '';
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $srt .= $array[$rand];
        }
        return $srt;
    }

    private function generateCode(&$out, $lengthCharacter = 1, $lengthNumber = 5) {
        //code format: {X}-{YYYYY} => [A-Z] - [0-9]*5
        $format = "{X}-{YYYYY}";
        $characters = range('A', 'Z');
        $strCharacter = $this->randomStringArray($characters, $lengthCharacter);
        //out
        $out = $strCharacter;
        $format = str_replace('{X}', $strCharacter, $format);

        $numbersAndCharacters = array_merge (range('0', '9'), $characters);
        $strAfter = $this->randomStringArray($numbersAndCharacters, $lengthNumber);
        $format = str_replace('{YYYYY}', $strAfter, $format);

        return $format;
    }

    private function getCodeCacheKey($userId, $phone) {
        //userid + country + phone
        $country = $this->container->getParameter('country_code');
        $cache_key = self::CODE_DATA_CACHE_KEY . $userId . $country . $phone;
        return $cache_key;
    }

    private function storeCodeInCache($userId, $phone, $code) {
        $mobicast_message_expired = $this->container->getParameter('mobicast_message_expired');
        $cache = $this->container->get('hgw.cache');
        $cache_key = $this->getCodeCacheKey($userId, $phone);
        //unit second
        $cache_time = $mobicast_message_expired * 60; //self::CODE_TIME_CACHE_KEY;
        return $cache->save($cache_key, $code, $cache_time);
    }

    private function getCodeFromInCache($userId, $phone) {
        $cache = $this->container->get('hgw.cache');
        $cache_key = $this->getCodeCacheKey($userId, $phone);
        $dataInCache = $cache->fetch($cache_key);
        return $dataInCache;
    }

    private function generateMesage($code) {
        $mobicast_message = $this->container->getParameter('mobicast_message');
        $mobicast_message_expired = $this->container->getParameter('mobicast_message_expired');
        $mobicast_message = str_replace('{X-XXXXX}', $code, $mobicast_message);
        $mobicast_message = str_replace('{Y}', $mobicast_message_expired, $mobicast_message);
        return $mobicast_message;
    }

    public function makePhoneFrom($countryCode, $phone) {
        $matches = array();
        $pre = '';
        if (preg_match('/\d{1,}/', $countryCode, $matches)) {
            $pre = $matches[0];
        }
        return "{$pre}{$phone}";
    }

    private function buildUrl($phone, $code) {
        //http://v2b.moobicast.com/rest/send/sms/{key}}?to={phone}&from={sendernam}&message={message}

        $mobicast_key = urldecode($this->container->getParameter('mobicast_key'));
        $mobicast_sender_name = urldecode($this->container->getParameter('mobicast_sender_name'));
        $mobicast_url = $this->container->getParameter('mobicast_url');

        $message = urlencode($this->generateMesage($code));
        $phone = urldecode($phone);

        $mobicast_url = str_replace('{key}', $mobicast_key, $mobicast_url);
        $mobicast_url = str_replace('{phone}', $phone, $mobicast_url);
        $mobicast_url = str_replace('{sender_name}', $mobicast_sender_name, $mobicast_url);
        $mobicast_url = str_replace('{message}', $message, $mobicast_url);

        return $mobicast_url;
    }

    private function sendSmsByService($phone, $code) {
        $mobicast_http_username = $this->container->getParameter('mobicast_http_username');
        $mobicast_http_password = $this->container->getParameter('mobicast_http_password');

        $url = $this->buildUrl($phone, $code);
        $this->container->get('monolog.logger.gw')->info('sendSmsbyMoobileCast >> url:' . $url);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$mobicast_http_username:$mobicast_http_password");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);                          
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);                           
        curl_setopt($ch, CURLOPT_USERAGENT, 'Sample Code');        
        //$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code
        $result = curl_exec($ch);        
        //var_dump($result);        
        curl_close($ch);
        return $result;
    }

    public function sendSms($userId, $phone) {
        $pre = '';
        $code = $this->generateCode($pre);
        //store code in cache
        $this->storeCodeInCache($userId, $phone, $code);

        //send sms        
        $res = $this->sendSmsByService($phone, $code);
        //write log
        $this->container->get('monolog.logger.gw')->info('sendSmsbyMoobileCast >> ' . $res);
        if(empty($res)) {
            return false;
        }
        $time = time();
        return array('time' => $time, 'pre' => $pre);
    }

    public function verifyCode($userId, $phone, $code) {
        //check code
        //get code from cache
        $codeInCache = $this->getCodeFromInCache($userId, $phone);
        return ($code === $codeInCache);
    }

}

<?php

namespace inSing\DataSourceBundle\Lib;

use inSing\DataSourceBundle\Lib\Monitor\SoapClientMonitor;
use inSing\DataSourceBundle\Lib\Pimple;
use inSing\DataSourceBundle\Lib\UtilHelper;

/**
 * A wrapper class of SoapClient
 *
 * @author Trung Nguyen
 */

class SoapClient implements \SplSubject
{
    const MAX_EXECUTE_TIME = 5;
    
    /**
     * @var \SoapClient
     */
    protected $soapClient;
    
    /**
     * @var string
     */
    protected $url;
    
    /**
     * @var int
     */
    protected $timeout = 5;
    
    /**
     * @var int
     */
    protected $startTime;
    
    /**
     * @var int
     */
    protected $stopTime;
    
    /**
     * @var string
     */
    protected $currentMethod;
    
    /**
     * @var array
     */
    private $observers = array();
    
    /**
     * @var array
     */
    protected $lastError = null;
    
    /**
     * Constructor
     */
    public function __construct() 
    {
        //
    }

    /**
     * Get magic method
     *
     * @param string $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    
    /**
     * Set Soap URL
     * 
     * @param string $url
     */
    public function setUrl($url) 
    {
        // reset SoapClient object
        $this->soapClient = null;

        $this->url = $url;
        
    }
    
    /**
     * Initialize SoapClient instance
     */
    protected function init()
    {
        if (empty($this->soapClient) || is_null($this->soapClient)) {
            
            $options = array(
                'connection_timeout' => $this->timeout
            );
            
            // get start time
            $this->startTime  = UtilHelper::getMicroTimeInFloat();
            
            $this->currentMethod = 'init';
            
            $this->soapClient = new \SoapClient($this->url, $options);
            
            // get stop time
            $this->stopTime = UtilHelper::getMicroTimeInFloat();
            
            $this->lastError = null;
            
            // notify observer objects
            $this->notify();
                
        }
    }
    
    /**
     * Call soap method
     * 
     * @param string $method
     * @param array $params
     */
    public function __call($method, $args)
    {
        $this->startTime = microtime(true);
    
        try {
            $this->init();
            
            // get start time
            $this->startTime  = UtilHelper::getMicroTimeInFloat();
            
            $this->currentMethod = $method;
            
            $result = call_user_func_array(array($this->soapClient, $method), $args);
            
            // reset var.
            $this->lastError = null;
            
        } catch (\SoapFault $e) {
            $this->lastError = array(
                'url'    => $this->url,
                'method' => $method,
                'error_code' => $e->getCode(),
                'error_str'  => $e->getMessage()
            );
            
            $result = false;
        }
    
        // get stop time
        $this->stopTime = UtilHelper::getMicroTimeInFloat();
    
        // notify observer objects
        $this->notify();
        
        return $result;
    }
    
    /**
     * Get last error
     *
     * @return array
     */
    public function getLastError()
    {
        return $this->lastError;
    }
    
    /**
     * Attaches an SplObserver to Curl
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function attach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        $this->observers[$id] = $obs;
    }
    
    /**
     * Detaches the SplObserver from the Curl
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function detach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        unset($this->observers[$id]);
    }
    
    /**
     * Notify all observers
     *
     * @author Trung Nguyen
     * @param  string $type
     * @return void
     */
    public function notify()
    {
        foreach($this->observers as $obs) {
            $obs->update($this);
        }
    }
    
}


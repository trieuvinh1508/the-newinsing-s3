<?php 

namespace inSing\DataSourceBundle\Lib\Monitor;

/**
 * An observer class for monitoring Exception
 *
 * @author Trung Nguyen
 */
class ExceptionMonitor implements \SplObserver 
{
    const LOG_FILE = 'data_source.log';
    
    /**
     * @var Logger
     */
    protected $logger;
    
    /**
     * Constructor
     * 
     * @param Logger $logger
     */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }
    
    /**
     * Log exception 
     *
     * @param  \SplSubject $subject
     * @return void
     */
    public function update(\SplSubject $subject)
    {
        $lastError = $subject->getLastError();
        
        $context = array(
            $lastError['class'],
            $lastError['method'],
            $lastError['error_code'],
            $lastError['error_str']
        );
        
        // log to file
        $this->logger->addErrorToFile($subject->pid, $context, self::LOG_FILE);
    }
}
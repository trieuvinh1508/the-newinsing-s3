<?php
namespace inSing\DataSourceBundle\Lib;

use inSing\DataSourceBundle\Lib\UtilHelper;

/**
 * A wrapper class of CURL lib.
 *
 * @author Trung Nguyen
 */
class Curl implements \SplSubject
{
    const MAX_EXECUTE_TIME = 5;

    /**
     * @var int
     */
    protected $timeout = 10;

    /**
     * @var int
     */
    protected $method;

    /**
     * @var array
     */
    protected $headers;

    /**
     * @var resource
     */
    protected $ch;

    /**
     * @var array
     */
    private $observers = array();

    /**
     * @var int
     */
    protected $startTime;

    /**
     * @var int
     */
    protected $stopTime;

    /**
     * @var string
     */
    protected $currentUrl;

    /**
     * @var Exception
     */
    protected $exception;

    /**
     * @var array
     */
    protected $lastError = null;

    // define constants
    const HTTP_GET  = 1;

    const HTTP_POST = 2;

    const HTTP_PUT  = 3;

    /**
     * Constructor
     */
    public function __construct()
    {
        // create a new cURL resource
        $this->ch = curl_init();

        if (!$this->ch) {
            throw new \Exception('Cant create cURL resource');
        }
    }

    /**
     * Get magic method
     *
     * @param string $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    /**
     * Set headers
     *
     * @param string $header
     */
    public function setHeader($header)
    {
        $this->headers[] = $header;
    }

    /**
     * Set HTTP method
     *
     * @param int $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * Set option for cURL
     *
     * @param int $option_key
     * @param mixed $value
     */
    public function setOption($option_key, $value)
    {
        curl_setopt($this->ch, $option_key, $value);
    }

    /**
     * Set timeout for cURL
     *
     * @param mixed $value
     */
    public function setTimeout($value)
    {
    	$this->timeout = $value;
    }

    /**
     * Get last error
     *
     * @return array
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     *
     * @param string $url
     * @param array $fields
     * @throws Exception
     * @return Ambigous <\inSing\DataSourceBundle\Lib\Returns, mixed>
     */
    public function call($url, $fields = array())
    {
        // get start time
        $this->startTime  = UtilHelper::getMicroTimeInFloat();
        $this->currentUrl = $url;

        // set URL and other appropriate options
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);

        curl_setopt($this->ch, ( $this->method == self::HTTP_POST ? CURLOPT_POST : CURLOPT_HTTPGET), true);

        if (count($fields) > 0 && $this->method == self::HTTP_POST) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->convertFieldsToString($fields));
        }

        // execute the given cURL session
        $result = curl_exec($this->ch);

        // get stop time
        $this->stopTime = UtilHelper::getMicroTimeInFloat();

        if (curl_errno($this->ch) != CURLE_OK) {
            $this->lastError = array(
                'url'    => $url,
                'method' => $this->method == self::HTTP_GET ? 'GET' : 'POST',
                'fields' => $this->convertFieldsToString($fields),
                'error_code' => curl_errno($this->ch),
                'error_str'  => curl_error($this->ch),
            );
            $result = false;
        } else {
            $this->lastError = null;
        }

        $this->notify();

        return $result;
    }

    /**
     * @author Cuong.Bui
     */
    public function cURLCall($url, $fields = array())
    {
        $this->startTime  = UtilHelper::getMicroTimeInFloat();
        $this->currentUrl = $url;

        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($this->ch, ( $this->method == self::HTTP_POST ? CURLOPT_POST : CURLOPT_HTTPGET), true);

        if (count($fields) > 0 && $this->method == self::HTTP_POST) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->convertFieldsToString($fields));
        }

        $result = curl_exec($this->ch);
        $this->stopTime = UtilHelper::getMicroTimeInFloat();

        return $result;
    }

    /**
     * Convert an array of fields into string
     *
     * @param array $fields
     * @return string
     */
    protected function convertFieldsToString(array $fields)
    {
        $xp = array();

        foreach ($fields as $key => $value) {
            $xp[] = $key . '=' . urlencode($value);
        }

        return implode('&', $xp);
    }

    /**
     * Attaches an SplObserver to Curl
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function attach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        $this->observers[$id] = $obs;
    }

    /**
     * Detaches the SplObserver from the Curl
     *
     * @author Trung Nguyen
     * @param  SplObserver $obs
     * @return void
     */
    public function detach(\SplObserver $obs)
    {
        $id = spl_object_hash($obs);
        unset($this->observers[$id]);
    }

    /**
     * Notify all observers
     *
     * @author Trung Nguyen
     * @param  string $type
     * @return void
     */
    public function notify()
    {
        foreach($this->observers as $obs) {
            $obs->update($this);
        }
    }
}
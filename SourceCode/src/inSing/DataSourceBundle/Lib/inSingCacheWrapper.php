<?php

namespace inSing\DataSourceBundle\Lib;

/**
 * Wrapper from cache util bundle
 * @author Khuong Phan
 */
use Symfony\Component\DependencyInjection\ContainerInterface;

class inSingCacheWrapper
{
    private $cache = null;
    private $prefix = "";

    /**
     * @param ContainerInterface $container
     * @author Cuong.Au
     */

    public function __construct(ContainerInterface $container) 
    {
        $cache = $container->get('insing.util.cache');
        $this->cache = $cache->getDefault();
        $this->cache->setCacheLifetime($container->getParameter('new_insing_cache_time'));
        $this->prefix = $container->getParameter('new_insing_cache_prefix') ;
    }

    /**
     * @param $data
     * @param $key
     * @param int $life_time
     * @author Cuong.Au
     */

    public function setCache($data,$key,$life_time=0, $override = false)
    {
        if($override || !$this->cache->hasCache($this->buildString($key))){
            if($life_time){
                $this->cache->setCacheLifetime($life_time);
            }
            $this->cache->setCache($this->buildString($key),$data);
        }
        return true;
    }

    /**
     * @param $key
     * @return bool
     * @author Cuong.Au
     */

    public function getCache($key)
    {
        if($this->cache->hasCache($this->buildString($key))){
            return $this->cache->getCache($this->buildString($key));
        }
        return false;
    }

    /**
     * @param $key
     * @return mixed
     * @author Cuong.Au
     */

    public function checkCache($key)
    {
        return $this->cache->hasCache($this->buildString($key));
    }

    public function clearCache($key)
    {
        return $this->cache->deleteCache($this->buildString($key));
    }

    public function clearAllCache()
    {
        return $this->cache->deleteAll();
    }

    /**
     * Combine all elements to string
     *
     * @param array $params
     * @return string
     * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
     */

    protected function buildString($param)
    {
        $tmp = array();

        // slutify strings closure
        $slugify = function($text) {
            // replace non letter or digits by -
            $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

            // trim
            $text = trim($text, '-');

            // transliterate
            if (function_exists('iconv')) {
                $text = @iconv('utf-8', 'us-ascii//TRANSLIT', $text);
            }

            // lowercase
            $text = strtolower($text);

            // remove unwanted characters
            $text = preg_replace('~[^-\w]+~', '', $text);

            // default output
            if (empty($text)) {
                return 'n-a';
            }

            return $text;
        };

        // array filter closure
        $callback = function($value, $key) use(&$tmp, $slugify) {
            if (is_object($value) && $value instanceof \DateTime) {
                $tmp[] = $slugify($key).'_'.$slugify($value->format('Y-m-d'));
            } elseif (trim($value) != '') {
                $tmp[] = $slugify($key).'_'.$slugify($value);
            }
        };

        if (is_array($param)) {
            array_walk_recursive($param, $callback);
            $cacheName = $this->prefix.'_'.implode('_', $tmp);
            if ( strlen($cacheName) > 200 ) { // cache length 250, we use 50 for prefix if needed
                return md5($cacheName);
            }
            
            return $cacheName;
        } else {
            $cacheName = $this->prefix.'_'.$slugify($param);
            if ( strlen($cacheName) > 200 ) { // cache length 250, we use 50 for prefix if needed
                return md5($cacheName);
            }
            
            return $cacheName;
        }
    }
}
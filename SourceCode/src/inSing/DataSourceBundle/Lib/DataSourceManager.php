<?php

namespace inSing\DataSourceBundle\Lib;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A factory class is used to generate DataSource objects
 *
 * @author Trung Nguyen <trung.nguyen@s3corp.com.vn>
 * @ref http://symfony.com/doc/current/components/dependency_injection/factories.html
 */
class DataSourceManager
{
    const ROOT_CLASS_PATH = 'inSing\FeaturesBundle\Lib\DataSource\\';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Generate data source object
     *
     * @param string $source_type
     * @return IDataSource
     */
    public function get($class_name)
    {
        $className = self::ROOT_CLASS_PATH.$class_name;

        if (class_exists($className)) {
            // initialize object
            $obj = new $className();

            // get configurations
            $config = $this->container->getParameter('insing_features');
            
            // inject some necessary services/parameters
            $obj->doctrine     = $this->container->get('doctrine');
            $obj->DSManager    = $this->container->get('insing.datasource.manager');
            $obj->dataTracker  = $this->container->get('insing.features.data_tracker');
            $obj->logger       = $this->container->get('insing.util.logger');
            $obj->timeOut      = $config['datasource_cache_timeout'];
            $obj->prefix       = $class_name;
            $obj->config       = $config;
            $obj->checkCache   = ( $this->container->get('request') && $this->container->get('request')->attributes->get('preview_mode') ? false : true );
            $obj->cache        = $this->container->get('new.insing.cache');
        }

        return isset($obj) ? $obj : false;
    }

}
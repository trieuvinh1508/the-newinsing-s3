<?php
namespace inSing\DataSourceBundle\Model;

use inSing\DataSourceBundle\Lib\Curl;
use inSing\DataSourceBundle\Lib\UtilHelper;
use inSing\DataSourceBundle\Model\DataSource;
use inSing\DataSourceBundle\Lib\Exception\RuntimeErrorException;
use inSing\DataSourceBundle\Lib\Exception\ResultIsEmptyException;

/**
 * A class is responsible for getting data from CMS API
 *
 * @author Vu Luu
 */
class CMSSource extends DataSource
{
    const PREFIX_CACHE = 'cms_source_';
    const LAST_UPDATED_DATE = 'last_updated_date';
    const PUBLISH_DATE = 'publish_date';
    const RANDOM_BY_CHANNEL = 'channel';

    /**
     * @var Cache
     */
    protected $cache;

    /**
     * @var array
     */
    protected $parameters = array();

    /**
     * Register methods will be monitored
     *
     * @see \inSing\DataSourceBundle\Model\DataSource::register()
     */
    protected function register()
    {
        return array(
            'getSessionToken',
            'getCMSListByTags',
            'getCMSListByVideoAndChannels',
            'getCMSListAll',
            'getCMSListByChannel',
        	'getCMSListByEvergreenAndChannels',
        	'getCMSListBySeriesAndChannels',
            'getCmsDetailsByArticleId',
            'getCMSGalleriesByTags',
            'getCMSGalleriesByVideoAndChannels',
            'getCMSGalleriesByChannel',
            'getCMSGalleriesByEvergreenAndChannels',
        	'getCMSGalleriesBySeriesAndChannels',
            'getCmsGalleryDetailsByArticleId',
            'getGalleriesPrevNext',
            'getArticlesPrevNext',
            'findArticles',
            'findGalleries',
            'findContents'
        );
    }

    protected function init()
    {
        $this->parameters = $this->get('parameters');
        $this->cache = $this->get('cache')->getDefault();
    }

    /**
     * Get session token after authenticate
     *
     * @throws RuntimerrorException
     * @return: session_token
     */
    public function getSessionToken()
    {
        // first check if token has been existed in cache
        if (!$this->cache->hasCache(self::PREFIX_CACHE . 'session_token') || !$this->cache->getCache(self::PREFIX_CACHE . 'session_token')) {
            $arr_result_auth = self::authenticate();

            if (is_array($arr_result_auth) && count($arr_result_auth) > 0 && $arr_result_auth['result_code'] == 200) {
                $this->cache->setCache(self::PREFIX_CACHE . 'session_token', $arr_result_auth['session_token'], $this->parameters['cache_timeout']);

                return $arr_result_auth['session_token'];
            }
        } else {
            return $this->cache->getCache(self::PREFIX_CACHE . 'session_token');
        }
    }

    /**
     * Authenticate
     * @author  Vu.Luu
     * Authenticate
     * @return NULL|multitype:unknown
     */
    private function authenticate()
    {
        $result = array();

        $url = $this->parameters['cms_api']['auth_url'];
        $access_token = $this->parameters['cms_api']['access_token'];
        $token_secret = $this->parameters['cms_api']['access_token_secret'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['access_token'] = $access_token;
        $fields['token_secret'] = $token_secret;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setMethod(Curl::HTTP_GET);
        $result = $cURL->call($url . '?' . UtilHelper::convertFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not get Session token');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200 && isset($result['session_token'])) {
                    return $result;
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    //--------------------------------------GALLERY--------------------------------------//
    /**
     * Get Cms Galleries By Tag
     * @author	Vu.Luu
     * @param String $tag
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSGalleriesByTags($tags = array(), $page_size = 20, $gallery_type = 0)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_galleries_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();

        if (!empty($tags)) {
    		$fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $tags);
    	}

        $fields['gallery_type'] = $gallery_type;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_size'] = $page_size;
        $fields['sort_by'] = self::PUBLISH_DATE;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find galleries by tag');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Galleries By Video And Channels
     * @author	Vu.Luu
     * @param array channels
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSGalleriesByVideoAndChannels($channels = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_galleries_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['video'] = true;

        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_size'] = $page_size;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find galleries by video');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Galleries By Channel
     * @author	Vu.Luu
     * @param array $channels Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSGalleriesByChannel($channels = array(), $page_size = 20, $gallery_type = 0)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_galleries_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['gallery_type'] = $gallery_type;
        $fields['sort_by'] = self::PUBLISH_DATE;
        $fields['page_size'] = $page_size;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find galleries by channel');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Galleries By Channel and Evergreen
     * @author	Vu.Luu
     * @param array $channel Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSGalleriesByEvergreenAndChannels($channels = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_galleries_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['evergreen'] = true;

        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['page_size'] = $page_size;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find galleries by channel');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Gallery Detail by Article Id
     * @author	Vu.Luu
     * @param int $articleId
     * @param int $imageHeight option
     * @param int $imageWidth option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCmsGalleryDetailsByArticleId($articleID, $imageHeight = null, $imageWidth = null)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_gallery_detail_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        //Add gallery_id with 15.000.000 if it less than
        $fields['gallery_id'] = ($articleID < 15000000) ? $articleID + 15000000 : $articleID;

        if (null != $imageHeight) {
            $fields['imageHeight'] = $imageHeight;
        }
        if (null != $imageWidth) {
            $fields['imageWidth'] = $imageWidth;
        }

        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find gallery details by article id');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['details']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['details'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Galleries Prev Next
     * @author	Vu.Luu
     * @param Int $galleryId
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getGalleriesPrevNext($galleryId = null, $tag = null)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_galleries_prev_next'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['gallery_id'] = $galleryId;
        if (null != $tag) {
    		$fields['tag'] = $tag;
    	}
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);
        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find galleries prev next');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    //--------------------------------------ARTICLES--------------------------------------//
    /**
     * Get Cms List By Tag
     * @author	Vu.Luu
     * @param String $tag
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListByTags($tags = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();

        if (!empty($tags)) {
    		$fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $tags);
    	}
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_size'] = $page_size;
        $fields['sort_by'] = self::PUBLISH_DATE;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles by tag');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms List By Video
     * @author	Vu.Luu
     * @param array channels
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListByVideoAndChannels($channels = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['video'] = true;

        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_size'] = $page_size;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles by video');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms List All
     * @author	Vu.Luu
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListAll()
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_size'] = UtilHelper::PAGESIZE_ALL_CHANNEL;
        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $result = $cURL->call($url . '?' . UtilHelper::convertFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find all articles');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms List By Channel
     * @author	Vu.Luu
     * @param array $channels Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListByChannel($channels = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['page_size'] = $page_size;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));

        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles by channel');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms List By Channel and Evergreen
     * @author	Vu.Luu
     * @param array $channel Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListByEvergreenAndChannels($channels = array(), $page_size = 20)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['evergreen'] = true;

        if (!empty($channels)) {
            $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
        }

        $fields['sort_by'] = self::LAST_UPDATED_DATE;
        $fields['page_size'] = $page_size;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        //$fields['random'] = self::RANDOM_BY_CHANNEL;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);

        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles by channel');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms List By Channel and Series Name
     * @author	Vu.Luu
     * @param array $channel Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSListBySeriesAndChannels($channels = array(), $series = array(), $page_size = 20)
    {
    	$result = array();

    	$url = $this->parameters['cms_api']['service_find_articles_url'];
    	$secret = $this->parameters['cms_api']['consumer_secret'];

    	$fields['session_token'] = $this->getSessionToken();

    	if (!empty($channels)) {
    		$fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
    	}

    	if (!empty($series)) {
    		$fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $series);
    	}

    	$fields['sort_by'] = self::LAST_UPDATED_DATE;
    	$fields['page_size'] = $page_size;
    	$fields['status'] = UtilHelper::PUBLISH_STATUS;
    	$fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

    	$cURL = $this->get('curl');
    	$cURL->setTimeout($this->parameters['cms_api']['timeout']);

    	$result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));

    	if (is_null($result)) {
    		throw new RuntimeErrorException('Error, can not find articles by channel and series');
    	} else {
    		$result = json_decode($result, true);
    		if (isset($result['status']) && $result['status'] != 200) {
    			throw new RuntimeErrorException($result['error'], $result['status']);
    		} else {
    			if (!isset($result['result_code'])) {
    				throw new RuntimeErrorException(json_encode($result));
    			}
    			if ($result['result_code'] == 200) {
    				if (null == $result['results']) {
    					throw new ResultIsEmptyException("Data not found");
    				} else {
    					return $result['results'];
    				}
    			} else {
    				throw new RuntimeErrorException($result['result_message'], $result['result_code']);
    			}
    		}
    	}
    }

    /**
     * Get Cms Galleries By Channel and Series Name
     * @author	Vu.Luu
     * @param array $channel Ex: array('HungryGoWhere.com')
     * @param int $page_size option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCMSGalleriesBySeriesAndChannels($channels = array(), $series = array(), $page_size = 20)
    {
    	$result = array();

    	$url = $this->parameters['cms_api']['service_find_galleries_url'];
    	$secret = $this->parameters['cms_api']['consumer_secret'];

    	$fields['session_token'] = $this->getSessionToken();

    	if (!empty($channels)) {
    		$fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $channels);
    	}

    	if (!empty($series)) {
    		$fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $series);
    	}

    	$fields['sort_by'] = self::LAST_UPDATED_DATE;
    	$fields['page_size'] = $page_size;
    	$fields['status'] = UtilHelper::PUBLISH_STATUS;
    	$fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

    	$cURL = $this->get('curl');
    	$cURL->setTimeout($this->parameters['cms_api']['timeout']);

    	$result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));

    	if (is_null($result)) {
    		throw new RuntimeErrorException('Error, can not find galleries by channel');
    	} else {
    		$result = json_decode($result, true);
    		if (isset($result['status']) && $result['status'] != 200) {
    			throw new RuntimeErrorException($result['error'], $result['status']);
    		} else {
    			if (!isset($result['result_code'])) {
    				throw new RuntimeErrorException(json_encode($result));
    			}
    			if ($result['result_code'] == 200) {
    				if (null == $result['results']) {
    					throw new ResultIsEmptyException("Data not found");
    				} else {
    					return $result['results'];
    				}
    			} else {
    				throw new RuntimeErrorException($result['result_message'], $result['result_code']);
    			}
    		}
    	}
    }

    /**
     * Get Cms Detail by Article Id
     * @author	Vu.Luu
     * @param int $articleId
     * @param int $imageHeight option
     * @param int $imageWidth option
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getCmsDetailsByArticleId($articleID, $imageHeight = null, $imageWidth = null)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_article_details_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['article_id'] = $articleID;

        if (null != $imageHeight) {
            $fields['imageHeight'] = $imageHeight;
        }
        if (null != $imageWidth) {
            $fields['imageWidth'] = $imageWidth;
        }

        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles details by article id');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['details']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['details'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * Get Cms Series All
     * @author	Vu.Luu
     * @param String $channel
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getSeriesListByChannel($channel = null)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_series_all_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['channel'] = $channel;
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['page_number'] = 0; //show all results
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles by tag');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['details']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['details'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }

    /**
     * @author Cuong.Bui
     */
    public function findArticles(array $params)
    {
        $url = $this->parameters['cms_api']['service_find_articles_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields = array(
            'session_token' => $this->getSessionToken(),
            'status' => UtilHelper::PUBLISH_STATUS,
            'sort_by' => self::PUBLISH_DATE,
            'page_number' => isset($params['page']) ? $params['page'] : 1,
            'page_size' => isset($params['limit']) ? $params['limit'] : 20
        );

        if (isset($params['filters']['title'])) {
            $fields['title'] = $params['filters']['title'];
        }

        if (isset($params['filters']['channel'])) {
            if (!is_array($params['filters']['channel'])) {
                $fields['channel' . rawurlencode('[]')] = $params['filters']['channel'];
            } else {
                $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $params['filters']['channel']);
            }
        }

        if (isset($params['filters']['tags'])) {
            if (!is_array($params['filters']['tags'])) {
                $fields['tags' . rawurlencode('[]')] = $params['filters']['tags'];
            } else {
                $fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $params['filters']['tags']);
            }
        }

        if (isset($params['filters']['occasions'])) {
            if (!is_array($params['filters']['occasions'])) {
                $fields['occasions' . rawurlencode('[]')] = $params['filters']['occasions'];
            } else {
                $fields['occasions' . rawurlencode('[]')] = implode('&occasions[]=', $params['filters']['occasions']);
            }
        }

        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);
        $url = $url . '?' . UtilHelper::convertComplexFieldsToString($fields);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url);

        if (!$result || !count($result)) {
            return array();
        } else {
            $result = json_decode($result, true);

            if (isset($result['status']) && $result['status'] != 200) {
                return array();
            } else {
                if (!isset($result['result_code'])) {
                    return array();
                }

                if ($result['result_code'] == 200) {
                    if (!count($result['results'])) {
                        return array();
                    } else {
                        return $result;
                    }
                } else {
                    return array();
                }
            }
        }
    }

    /**
     * @author Cuong.Bui
     */
    public function findGalleries(array $params)
    {
        $url = $this->parameters['cms_api']['service_find_galleries_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields = array(
            'session_token' => $this->getSessionToken(),
            'status' => UtilHelper::PUBLISH_STATUS,
            'sort_by' => self::PUBLISH_DATE,
            'page_number' => isset($params['page']) ? $params['page'] : 1,
            'page_size' => isset($params['limit']) ? $params['limit'] : 20
        );

        if (isset($params['filters']['title'])) {
            $fields['title'] = $params['filters']['title'];
        }

        if (isset($params['filters']['channel'])) {
            if (!is_array($params['filters']['channel'])) {
                $fields['channel' . rawurlencode('[]')] = $params['filters']['channel'];
            } else {
                $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $params['filters']['channel']);
            }
        }

        if (isset($params['filters']['tags'])) {
            if (!is_array($params['filters']['tags'])) {
                $fields['tags' . rawurlencode('[]')] = $params['filters']['tags'];
            } else {
                $fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $params['filters']['tags']);
            }
        }

        if (isset($params['filters']['occasions'])) {
            if (!is_array($params['filters']['occasions'])) {
                $fields['occasions' . rawurlencode('[]')] = $params['filters']['occasions'];
            } else {
                $fields['occasions' . rawurlencode('[]')] = implode('&occasions[]=', $params['filters']['occasions']);
            }
        }

        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);
        $url = $url . '?' . UtilHelper::convertComplexFieldsToString($fields);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url);

        if (!$result || !count($result)) {
            return array();
        } else {
            $result = json_decode($result, true);

            if (isset($result['status']) && $result['status'] != 200) {
                return array();
            } else {
                if (!isset($result['result_code'])) {
                    return array();
                }

                if ($result['result_code'] == 200) {
                    if (!count($result['results'])) {
                        return array();
                    } else {
                        return $result;
                    }
                } else {
                    return array();
                }
            }
        }
    }

    /**
     * @author Cuong.Bui
     */
    public function findContents(array $params)
    {
        $url = $this->parameters['cms_api']['service_find_contents_url'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields = array(
            'session_token' => $this->getSessionToken(),
            'status' => UtilHelper::PUBLISH_STATUS,
            'sort_by' => self::PUBLISH_DATE,
            'page_number' => isset($params['page']) ? $params['page'] : 1,
            'page_size' => isset($params['limit']) ? $params['limit'] : 20
        );

        if (isset($params['filters']['title'])) {
            $fields['title'] = $params['filters']['title'];
        }

        if (isset($params['filters']['channel'])) {
            $fields['channel'] = $params['filters']['channel'];

            /*
            if (!is_array($params['filters']['channel'])) {
                $fields['channel' . rawurlencode('[]')] = $params['filters']['channel'];
            } else {
                $fields['channel' . rawurlencode('[]')] = implode('&channel[]=', $params['filters']['channel']);
            }
            */
        }

        if (isset($params['filters']['tags'])) {
            $fields['tags'] = $params['filters']['tags'];

            /*
            if (!is_array($params['filters']['tags'])) {
                $fields['tags' . rawurlencode('[]')] = $params['filters']['tags'];
            } else {
                $fields['tags' . rawurlencode('[]')] = implode('&tags[]=', $params['filters']['tags']);
            }
            */
        }

        if (isset($params['filters']['occasions'])) {
            $fields['occasions'] = $params['filters']['occasions'];

            /*
            if (!is_array($params['filters']['occasions'])) {
                $fields['occasions' . rawurlencode('[]')] = $params['filters']['occasions'];
            } else {
                $fields['occasions' . rawurlencode('[]')] = implode('&occations[]=', $params['filters']['occasions']);
            }
            */
        }

        $fields['sig'] = UtilHelper::genNewSignatureWithSecret($url, $fields, $secret);
        $url = $url . '?' . UtilHelper::convertNewComplexFieldsToString($fields);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url);

        if (!$result || !count($result)) {
            return array();
        } else {
            $result = json_decode($result, true);

            if (isset($result['status']) && $result['status'] != 200) {
                return array();
            } else {
                if (!isset($result['result_code'])) {
                    return array();
                }

                if ($result['result_code'] == 200) {
                    if (!count($result['results'])) {
                        return array();
                    } else {
                        return $result;
                    }
                } else {
                    return array();
                }
            }
        }
    }

    /**
     * Get Cms Articles Prev Next
     * @author	Vu.Luu
     * @param Int $galleryId
     * @return array
     * @throws RuntimerrorException, ResultIsEmptyException
     */
    public function getArticlesPrevNext($articleId = null, $tag = null)
    {
        $result = array();

        $url = $this->parameters['cms_api']['service_articles_prev_next'];
        $secret = $this->parameters['cms_api']['consumer_secret'];

        $fields['session_token'] = $this->getSessionToken();
        $fields['article_id'] = $articleId;
        if (null != $tag) {
    		$fields['tag'] = $tag;
    	}
        $fields['status'] = UtilHelper::PUBLISH_STATUS;
        $fields['sig'] = UtilHelper::genSignatureWithSecret2($url, $fields, $secret);

        $cURL = $this->get('curl');
        $cURL->setTimeout($this->parameters['cms_api']['timeout']);
        $result = $cURL->call($url . '?' . UtilHelper::convertComplexFieldsToString($fields));
        if (is_null($result)) {
            throw new RuntimeErrorException('Error, can not find articles prev next');
        } else {
            $result = json_decode($result, true);
            if (isset($result['status']) && $result['status'] != 200) {
                throw new RuntimeErrorException($result['error'], $result['status']);
            } else {
                if (!isset($result['result_code'])) {
                    throw new RuntimeErrorException(json_encode($result));
                }
                if ($result['result_code'] == 200) {
                    if (null == $result['results']) {
                        throw new ResultIsEmptyException("Data not found");
                    } else {
                        return $result['results'];
                    }
                } else {
                    throw new RuntimeErrorException($result['result_message'], $result['result_code']);
                }
            }
        }
    }
}
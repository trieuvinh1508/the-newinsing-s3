<?php

namespace inSing\DataSourceBundle\Model;

use inSing\DataSourceBundle\Lib\Monitor\SoapClientMonitor;

use inSing\DataSourceBundle\Lib\Monitor\CurlMonitor;

use Symfony\Bridge\Doctrine\RegistryInterface;
use inSing\DataSourceBundle\Lib\Pimple;
use inSing\DataSourceBundle\Lib\Curl;
use inSing\DataSourceBundle\Lib\SoapClient;
use inSing\DataSourceBundle\Lib\Monitor\SpeedMonitor;
use inSing\DataSourceBundle\Lib\Monitor\ExceptionMonitor;
use inSing\DataSourceBundle\Model\SuperDealSource;
use inSing\DataSourceBundle\Model\RnRSource;
use inSing\DataSourceBundle\Model\MovieSource;
use inSing\DataSourceBundle\Model\UsereSource;
use inSing\DataSourceBundle\Model\EventSource;
use inSing\DataSourceBundle\Model\CMSSource;
use inSing\DataSourceBundle\Model\EventsApiSource;
use inSing\DataSourceBundle\Model\BusinessSource;
use inSing\DataSourceBundle\Model\ArticleSource;
use inSing\DataSourceBundle\Model\GallerySource;
use inSing\DataSourceBundle\Model\FreshSource;
use inSing\UtilBundle\Cache\Cache;
use inSing\UtilBundle\Log\Logger;

/**
 * DataSource Factory class
 *
 * @author Trung Nguyen
 */
class DataSourceManager extends Pimple
{
    /**
     * Initialize services
     *
     * @param array $config
     * @param Cache $cache
     * @param Logger $logger
     * @param RegistryInterface $doctrine
     * @return void
     */
    public function init(array $config, Cache $cache, Logger $logger, RegistryInterface $doctrine)
    {
        // initialize services for container
        // [config]
        $this->values['parameters'] = $config;

        // [logger]
        $this->values['logger'] = $logger;

        // [cache]
        $this->values['cache'] = $cache;

        // [doctrine]
        $this->values['doctrine'] = $doctrine;

        // [speed_monitor]
        $this->values['speed_monitor'] = function ($c) {
            return new SpeedMonitor($c['logger']);
        };

        // [curl_monitor]
        $this->values['curl_monitor'] = function ($c) {
            return new CurlMonitor($c['logger']);
        };

        // [soapclient_monitor]
        $this->values['soapclient_monitor'] = function ($c) {
            return new SoapClientMonitor($c['logger']);
        };

        // [exception_monitor]
        $this->values['exception_monitor'] = function ($c) {
            return new ExceptionMonitor($c['logger']);
        };

        // [curl]
        $this->values['curl'] = function ($c) {
            $cUrl = new Curl();
            $cUrl->attach($c['curl_monitor']);
            return $cUrl;
        };

        // [soapclient]
        $this->values['soapclient'] = function ($c) {
            $soapClient = new SoapClient();
            $soapClient->attach($c['soapclient_monitor']);
            return $soapClient;
        };


        // [movie_api]
        $this->values['movie'] = $this->share(function ($c) {
            // init. wrapper of MovieSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new MovieSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [user_api]
        $this->values['user'] = $this->share(function ($c) {
            // init. wrapper of MovieSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new UserSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [superdeal_api]
        $this->values['superdeal'] = $this->share(function ($c) {
            // init. wrapper of SuperDealSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new SuperDealSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [event]
        $this->values['event'] = $this->share(function ($c) {
            // init. wrapper of SuperDealSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new EventSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [cms]
        $this->values['cms'] = $this->share(function ($c) {
            // init. wrapper of SuperDealSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new CMSSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [eventsApi]
        $this->values['eventsApi'] = $this->share(function ($c) {
            // init. wrapper of SuperDealSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new EventsApiSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [business_api]
        $this->values['business'] = $this->share(function ($c) {
            // init. wrapper of BusinessSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new BusinessSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [article_api]
        $this->values['article'] = $this->share(function ($c) {
            // init. wrapper of ArticleSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new ArticleSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [gallery_api]
        $this->values['gallery'] = $this->share(function ($c) {
            // init. wrapper of GallerySource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new GallerySource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [rnr_api]
        $this->values['rnr'] = $this->share(function ($c) {
            // init. wrapper of GallerySource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new RnRSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });

        // [fresh_api]
        $this->values['fresh'] = $this->share(function ($c) {
            // init. wrapper of SuperDealSource
            $dsw = new DataSourceWrapper();
            $dsw->setDataSource(new FreshSource($c));
            $dsw->attach($c['speed_monitor']);
            $dsw->attach($c['exception_monitor']);
            return $dsw;
        });
    }

}
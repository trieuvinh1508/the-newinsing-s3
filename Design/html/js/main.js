var main = {
  carousel: function() {
    $("#top-carousel").owlCarousel({
      navigation: true,
      navigationText: ["‹", "›"],
      slideSpeed: 300,
      paginationSpeed: 400,
      singleItem: true,
      pagination: false,
      autoHeight: false,
      autoPlay: 10000,
      stopOnHover: true,
      responsiveRefreshRate: 500
    });
    
  },
  isHomePage: function() {
    return $('#page-container').hasClass('page-home');
  },

  isMobile: function() {
    return window.device.mobile();
  },
  isTablet: function() {
    return device.tablet();
  },
  isPortrait: function() {
    return device.portrait();
  },
  isDesktop: function() {
    return device.desktop();
  },
  toggle_grid_list: function($this, action) {
    var thisObj = this;

    var screen_width = main.viewport().width;
    var window_size_mobile = screen_width <= 767;
    var window_size_portrait = (screen_width > 767 && screen_width <= 980);

    var $module = $this.closest('.module-grid-list');
    var $itemList = $module.find('.item-list');
    var item_total = $itemList.find('.item').size();
    var row_total = 1;
    if (item_total > 3) {
      if (item_total > 6) {
        row_total = 3;
      } else {
        row_total = 2;
      }
    }
    var min_height = $module.find('.item:eq(0)').height() + 35;
    var max_height = min_height * row_total;
    var isMobile = window.device.mobile();
    if (isMobile || window_size_mobile) {
      min_height = 0;
      max_height = 0;
      $module.find('.item').each(function(index, el) {
        var it_height = $(this).height();
        if (it_height > 0) {
          it_height = it_height + 20;
        }
        if (index < 3) {
          min_height = min_height + it_height;
        }
        max_height = max_height + it_height;
      });
      min_height = min_height - 5;
      max_height = max_height - 5;
      if ($module.hasClass('top-movie')) {
        min_height = min_height + $module.find('.item:eq(3)').height() + 20;
      }
    } else if ((device.tablet() && device.portrait()) || window_size_portrait) {
      max_height = 0;
      min_height = 0;
      $module.find('.item').each(function(index, el) {
        var it_height = $(this).height() + 20;
        if (index == 0) {
          max_height = max_height + it_height
          min_height = it_height + 15;
        } else if (index == 3 || index == 6) {
          max_height = max_height + it_height;
        }
      });
      min_height = min_height - 15;
    } else {
      max_height = 0;
      min_height = 0;
      $module.find('.item').each(function(index, el) {
        var it_height = $(this).height() + 30;
        if (index == 0) {
          max_height = max_height + it_height
          min_height = it_height + 15;
        } else if (index == 3 || index == 6) {
          max_height = max_height + it_height;
        }
      });
      min_height = min_height - 15;
      max_height = max_height + 15;
    }
    if (action == 'get') {
      return {
        minHeight: min_height,
        maxHeight: max_height
      }
    }
    var timeout = 1000;
    var class_for_change = 'opened';
    if ($module.hasClass(class_for_change)) {
      $itemList.stop().animate({
        height: min_height
      }, timeout);
      var scrollTop = $itemList.offset().top;
      if (isMobile) {
        scrollTop = scrollTop + (min_height / 3) * 2;
      }
      $('html,body').animate({
        scrollTop: scrollTop
      }, timeout);
      $module.removeClass(class_for_change);
    } else {
      $itemList.stop().animate({
        height: max_height
      }, timeout, function() {
        $module.addClass(class_for_change);
      });
    }
    // truncate desc after expand
    if (!$itemList.hasClass('hasTruncate')) {
      //main.homepage_truncateContent();
      $itemList.addClass('hasTruncate');
    }
  },
  homepage_auto_expand_list: function($this, action) {
    setTimeout(function() {
      main.toggle_grid_list($this, action);
    }, 700);
  },
  homepage_getHeightGridList: function($list) {
    return main.toggle_grid_list($list, 'get');
  },
  homepage_setMinHeightGridList: function() {
    setTimeout(function() {
      var $pageHome = $('.page-home');
      var $module = $pageHome.find('.module-grid-list');
      var $itemList = $module.find('.item-list');

      var screen_width = main.viewport().width;
      var window_size_mobile = screen_width <= 767;
      var window_size_portrait = (screen_width > 767 && screen_width <= 980);

      $itemList.each(function(index, el) {
        var min_height = 0;
        var opened = $(this).closest('.module-grid-list').hasClass('opened');
        $(this).find('.item:lt(3)').css('height', 'auto');

        $(this).find('.item').each(function(index, el) {
          if (index < 3) {
            var item_height = $(this).height();
            if (item_height > 0) {
              item_height = item_height + 20;
            }
            if (window.device.mobile() || window_size_mobile) {
              min_height = min_height + item_height;
            } else {
              min_height = Math.max(item_height, min_height);
            }
          }
        });
        var height_set = min_height;
        if (window.device.mobile() || window_size_mobile) {
          height_set = min_height;
          if ($(this).closest('.module-grid-list').hasClass('top-movie')) {
            height_set = height_set + $(this).find('.item:eq(3)').height() + 20;
          }
        } else if ((device.tablet() && device.portrait()) || window_size_portrait) {
          $(this).find('.item:lt(3)').height(min_height - 20);
          height_set = min_height;
        } else {
          $(this).find('.item:lt(3)').height(min_height - 15);
          height_set = min_height + 20;
        }
        if (opened) {
          var dem = main.homepage_getHeightGridList($(this));
          $(this).height(dem.maxHeight);
        } else {
          $(this).height(height_set);
        }
      });
    }, 100);
  },

  homepage_truncateContent: function(action) {
    var thisObj = this;
    $('.page-home .module-grid-list .item-list .item .entry .title-wrap a').each(function(index, el) {
      var $this = $(this);
      var text = $.trim($this.text());
      var origin_text = $this.data('origin-text') || '';
      if (origin_text.length == 0) {
        $this.data('origin-text', text)
      }
      if (action == "refresh") {
        $this.removeClass('hasTruncate');
        text = origin_text;
      }
      var length_limit = 55;
      if (device.tablet() && device.portrait()) {
        length_limit = 40;
      } else if (device.mobile() && device.iphone()) {
        length_limit = 55;
        if (device.landscape()) {
          length_limit = 90;
        }
      }
      if (text.length > length_limit) {
        var shortText = text.substring(0, length_limit).split(" ").slice(0, -1).join(" ") + " ...";
        $this.text(shortText);
        $this.addClass('hasTruncate');
      }
    });

    $('.page-home .module-grid-list .item-list .item .entry .desc').each(function(index, el) {
      var $this = $(this);
      var text = $.trim($this.text());
      var origin_text = $this.data('origin-text') || '';
      if (origin_text.length == 0) {
        $this.data('origin-text', text)
      }
      if (action == "refresh") {
        $this.removeClass('hasTruncate');
        text = origin_text;
      }
      var length_limit = 82;
      if (device.tablet() && device.portrait()) {
        length_limit = 60;
      } else if (device.mobile() && device.iphone()) {
        length_limit = 76;
        if (device.landscape()) {
          length_limit = 122;
        }
      }
      if (text.length > length_limit) {
        var shortText = text.substring(0, length_limit).split(" ").slice(0, -1).join(" ") + " ...";
        $this.text(shortText);
        $this.addClass('hasTruncate');
      }
    });
  },
  homepage_onResize: function() {
    var thisObj = this;
    $(window).bind('resize', function() {
      setTimeout(function() {
        main.homepage_setMinHeightGridList();
      }, 500);
    });
  },
  movie: {
    isLanding: function() {
      return $('#page-container').hasClass('page-movie-landing');
    },
    isNowShowing: function() {
      return $('#page-container').hasClass('page-movie-now-showing');
    },
    isMovieNews: function() {
      return $('#page-container').hasClass('page-movie-news');
    },
    isArticleSingle: function() {
      return $('#page-container').hasClass('page-movie-article-single');
    },
    isMovieShowtimes: function() {
      return $('#page-container').hasClass('page-movie-showtimes');
    },
    fitRow: function($container, options) {

      var cols = options.col,
        $els = $container,
        maxH = 0,
        j;
      $els.each(function(i, p) {
        var $p = $(p),
          h;
        $p.css('height', 'auto');
        maxH = Math.max($p.outerHeight(), maxH);
        if (i % cols == cols - 1 || i == cols - 1) {
          for (j = cols; j; j--) {
            $p.css('height', maxH);
            $p = $p.prev();
          }
          maxH = 0;
        }
      });
    },

    landing: {
      carousel: function() {
        var visible = 5
        var scroll = visible - 1;
        if (main.isMobile()) {
          visible = 2;
          scroll = 1;
          $('.module-movie-carousel .caption').html($('.module-movie-carousel .caption').text());
        }
        $(".movie-slideshow").jCarouselLite({
          btnNext: ".carousel-controls .carousel-next",
          btnPrev: ".carousel-controls .carousel-prev",
          scroll: scroll,
          visible: visible,
          afterEnd: function(items) {
            $(items).find('.thumbnail img[data-src]').each(function(index, el) {
              $(this).attr('src', $(this).attr('data-src'));
              $(this).removeAttr('data-src');
            });
          }
        });
      },
      toggle_list: function($this) {
        main.movie.toggle_list($this);
      },
      trailer_popup: function(opt) {
        var option = {
          url: ""
        };
        $.extend(true, option, opt);
        $('.movie-trailer').find('.movie-trailer-modal').find('iframe').attr('src', option.url);
        $('.movie-trailer').find('.movie-trailer-modal').show();

      },
      init: function() {
        this.carousel();
        if (main.isMobile()) {
          $('.movie-news').find('.mpu-ads').parent().remove();
          main.movie.fitRow($('.movie-on-demand .item'), {
            col: 2
          });
          main.movie.fitRow($('.coming-soon .item'), {
            col: 2
          });
          setTimeout(function() {
            main.movie.setMinHeight('.movie-news', 6);
            main.movie.setMinHeight('.movie-on-demand ', 4);
          }, 100);
        } else {
          main.movie.setMinHeight('.movie-news', 6);
        }

        $('.movie-trailer').find('.movie-trailer-modal .x-close').click(function(event) {
          $(this).closest('.movie-trailer-modal').hide();
        });
      }
    },
    nowShowing: function() {
      if (typeof $.fn.jCarouselLite != 'undefined') {
        var visible = 4;
        if (main.isMobile()) {
          visible = 2;
        }
        var scroll = visible - 1;
        var $item = $(".popular-this-week .carousel").find('.item');
        var $controls = $(".popular-this-week .carousel-controls");
        var size = $item.size();
        if (size <= visible) {
          $controls.hide();
        } else {
          $controls.show();
        }
        $(".popular-this-week .carousel").jCarouselLite({
          btnNext: ".popular-this-week .carousel-controls .carousel-next",
          btnPrev: ".popular-this-week .carousel-controls .carousel-prev",
          scroll: scroll,
          visible: visible
        });
      }

      $('.all-movie').find('.item:gt(5)').hide();
      if (main.isMobile()) {
        main.movie.fitRow($('.editor-picks .item'), {
          col: 2
        });
        main.movie.fitRow($('.all-movie .item'), {
          col: 2
        });
        main.movie.setMinHeight('.editor-picks', 4);
        main.movie.setMinHeight('.all-movie', 4);
      } else {
        main.movie.setMinHeight('.all-movie', 6);
      }

    },
    movieNews: function() {
      if (main.isMobile()) {
        $('.movie-news').find('.mpu-ads').parent().remove();
        main.movie.setMinHeight('.movie-news', 3);

      }
      main.movie.setMinHeight('.movie-news', 6);
    },
    articleSingle: {
      movieTrailer: function() {},
      init: function() {
        this.movieTrailer();
      }
    },
    movieShowtimes: {
      init: function() {
        if (main.isMobile()) {
          main.movie.fitRow($('.movies-now-showing .item'), {
            col: 2
          });
          
          var $module = $('.movies-now-showing');
          var $itemList = $('.movies-now-showing .content');
          var $items = $('.movies-now-showing').find('.item');
          var $view = $('.movies-now-showing .view');
          $items.filter(':gt(3)').hide();
          var minHeight = $itemList.height();
          $itemList.data('min-height', minHeight);
          $itemList.height(minHeight);
          $view.find('a').click(function(e) {
            var timeout = 1000;
            var class_for_change = 'opened';
            var min_height = $itemList.data('min-height') || 720;

            if ($module.hasClass(class_for_change)) {
              $itemList.stop().animate({
                height: min_height
              }, timeout);
              var scrollTop = $itemList.offset().top;
              $('html,body').animate({
                scrollTop: scrollTop
              }, timeout);
              $module.removeClass(class_for_change);
            } else {
              var item_per_row = 2;
              var max_height = Math.ceil($itemList.find('.item').size() / item_per_row) * min_height / 2;
              $itemList.find('.item:gt(' + (item_per_row - 1) + ')').show();
              $itemList.stop().animate({
                height: max_height
              }, timeout, function() {
                $module.addClass(class_for_change);
              });
            }
          });
        }
      }
    },
    isFirst: true,
    setMinHeight: function(elm, visible) {
      setTimeout(function() {
        var $movieNewsList = $(elm).find('.item-list');
        $movieNewsList.find('.item:gt(' + (visible - 1) + ')').hide();
        var minHeight = $movieNewsList.height();
        $movieNewsList.data('min-height', minHeight);
        $movieNewsList.height(minHeight);
        main.movie.isFirst = false;
        $movieNewsList.find('.item:gt(' + (visible - 1) + ')').show();
      }, 800);
    },
    module_photos: {
      reinit: function() {
        $('.module-movie-photo').each(function(index, el) {
          var $this = $(this);
          $this.find('.carousels').removeAttr('style');
          $this.find('.carousels ul').removeAttr('style');
          $this.find('.carousels ul li').removeAttr('style');
          var html = $this.find('.carousels').html();
          $this.find('.carousels').remove();

          $this.append('<div class="carousels">' + html + '</div>');
          var start = $this.data('carousel-start') || '';
          start = parseInt(start || 0);
          if (start > 0) {
            $this.find('.prev').show();
          } else {
            $this.find('.prev').hide();
          }
          var end = $this.data('carousel-end') || '';
          end = parseInt(end || 0);
          if (end >= $this.find('.carousels').find('li.item').size()) {
            $this.find('.next').hide();
          } else {
            $this.find('.next').show();
          }
        });
        main.movie.module_photos.init();
      },
      resize: function() {
        setTimeout(function() {
          $(window).bind('resize', function() {
            main.movie.module_photos.reinit();
          });
        }, 500);
      },
      init: function() {
        if (typeof $.fn.jCarouselLite != 'undefined') {
          var visible = 4;
          if (main.isMobile()) {
            visible = 2;
          } else if (main.isTablet() && main.isPortrait()) {
            visible = 3;
          }
          $('.module-movie-photo').each(function() {
            var $this = $(this);
            var start = $this.data('carousel-start') || '';
            start = parseInt(start || 0);
            var size = $this.find('.carousels').find('li').size();
            if (start > size - visible) {
              start = size - visible;
            }
            if (size <= visible) {
              $this.addClass('no-carousels');
              return;
            }
            $this.find('.carousels').jCarouselLite({
              btnNext: $this.find(".next"),
              btnPrev: $this.find(".prev"),
              start: start,
              scroll: visible - 1,
              visible: visible,
              circular: false,
              afterEnd: function(items) {
                var size = $this.find('.carousels').find('li.item').size();
                var item_first = items[0];
                var item_last = items[items.length - 1];
                var index_prev = $this.find('.carousels').find(item_first).index() + 1;
                var index_next = $this.find('.carousels').find(item_last).index() + 1;
                $this.data('carousel-start', index_prev);
                $this.data('carousel-end', index_next);
                if (index_next >= size) {
                  $this.find(".next").hide();
                } else {
                  $this.find(".next").show();

                }
                if (index_prev == 1) {
                  $this.find(".prev").hide();
                } else {
                  $this.find(".prev").show();
                }
              }
            });
          });
        }
      }
    },
    toggle_list: function($this) {
      var first_timeout = 0;
      if (main.movie.isFirst) {
        first_timeout = 1000;
      }
      setTimeout(function() {
        var $module = $this.closest('.module-movie-grid-list');
        var $itemList = $module.find('.item-list');
        var timeout = 1000;
        var class_for_change = 'opened';
        var min_height = $itemList.data('min-height') || 386;

        if ($module.hasClass(class_for_change)) {
          $itemList.stop().animate({
            height: min_height
          }, timeout);
          var scrollTop = $itemList.offset().top;
          $('html,body').animate({
            scrollTop: scrollTop
          }, timeout, function() {

          });
          $module.removeClass(class_for_change);
        } else {
          var item_per_row = 3;
          if (main.movie.isNowShowing()) {
            item_per_row = 6;
            if (main.isMobile()) {
              item_per_row = 2;
            }
          }
          var max_height = Math.ceil($itemList.find('.item').size() / item_per_row) * min_height;
          // movie news
          if (main.movie.isMovieNews() || main.movie.isLanding()) {
            max_height = min_height + (min_height / 2);
          }
          $itemList.find('.item:gt(' + (item_per_row - 1) + ')').show();
          $itemList.stop().animate({
            height: max_height
          }, timeout, function() {
            $module.addClass(class_for_change);
          });
        }
        main.truncate_auto();
      }, first_timeout);
    },
    init: function() {

      if (main.movie.isLanding()) {
        main.movie.landing.init();
      } else if (main.movie.isNowShowing()) {
        main.movie.nowShowing();
      } else if (main.movie.isMovieNews()) {
        main.movie.movieNews();
      } else if (main.movie.isArticleSingle()) {
        main.movie.articleSingle.init();
      } else if (main.movie.isMovieShowtimes()) {
        main.movie.movieShowtimes.init();
      }
      main.movie.module_photos.init();
      main.movie.module_photos.resize();
    }
  },
  toggleNav: function() {
    var timeoutNav = null;
    // pc hover
    var fnNav = function($this, toggle) {
      if ($('#layout').hasClass('desktop-mobile')) {
        return;
      }
      if ($this.find('.navbar-sub').length) {
        if (timeoutNav) {
          clearTimeout(timeoutNav);
        }
        timeoutNav = setTimeout(function() {
          if (toggle == 'show') {
            $this.find('.navbar-sub').slideDown();
            $('#page-header').animate({
              marginBottom: 42
            });
          } else {
            $this.find('.navbar-sub').slideUp();
            $('#page-header').animate({
              marginBottom: 0
            });
          }
        }, 100);
      }
    };

    $('.pc-device .navbar-nav > li').each(function(index, el) {
      var $this = $(this);
      $this.hover(function(e) {
        $(this).addClass('hover');
        fnNav($this, 'show');
      }, function() {
        $(this).removeClass('hover');
        if ($this.hasClass('selected')) {
          return false;
        }
        fnNav($this, 'hide');
      });
    });
    // hover for nav touch
    if (Modernizr.mq("screen and (min-width:768px)")) {
      $('.touch-device .navbar-nav li a').click(function(e) {
        var $li = $(this).parent('li');
        if ($li.find('.navbar-sub').length) {
          if ($li.hasClass('hover')) {} else {
            $li.addClass('hover');
            fnNav($li, 'show');
            return false;
          }
        } else {
          fnNav($('.touch-device .navbar-nav li'), 'hide');
          $('.touch-device .navbar-nav li').removeClass('hover')
          $li.toggleClass('hover');
        }
      });
    }
  },
  event: function(text, options) {
    var thisObj = this;
    $('#page-header').find('.navbar-nav li').hover(function() {
      $(this).addClass('hover');
    }, function() {
      $(this).removeClass('hover');
    });
  },
  viewport: function() {
    var viewport = new Object();
    viewport.width = 0;
    viewport.height = 0;
    if (typeof window.innerWidth != 'undefined') {
      viewport.width = window.innerWidth,
        viewport.height = window.innerHeight
    } else if (typeof document.documentElement != 'undefined' && typeof document.documentElement.clientWidth !=
      'undefined' && document.documentElement.clientWidth != 0) {
      viewport.width = document.documentElement.clientWidth,
        viewport.height = document.documentElement.clientHeight
    } else {
      viewport.width = document.getElementsByTagName('body')[0].clientWidth,
        viewport.height = document.getElementsByTagName('body')[0].clientHeight
    }
    return viewport;
  },
  calculateSize: function(str, opts) {
    function createDummyElement(text, options) {
      var element = document.createElement('div'),
        textNode = document.createTextNode(text);

      element.appendChild(textNode);

      element.style.fontFamily = options.font;
      element.style.fontSize = options.fontSize;
      element.style.fontWeight = options.fontWeight;
      element.style.lineHeight = options.lineHeight;
      element.style.position = 'absolute';
      element.style.visibility = 'hidden';
      element.style.left = '-999px';
      element.style.top = '-999px';
      element.style.width = '999999px';
      element.style.height = 'auto';

      document.body.appendChild(element);

      return element;
    }

    function destoryElement(element) {
      element.parentNode.removeChild(element);
    }
    var options = {
      font: 'OpenSans',
      fontSize: '14px',
      lineHeight: 'normal',
      fontWeight: 'normal'
    };
    $.extend(true, options, opts);
    var size = {},
      element;

    element = createDummyElement(str, options);

    size.width = element.offsetWidth;
    size.height = element.offsetHeight;

    destoryElement(element);

    return size;
  },
  dropdown: function() {
    $(document).on('click', '.dropdown', function(e) {
      if ($(this).find('.dropdown-container').length == 0) return;

      $(this).toggleClass('open');
      $(this).closest('body').find('.dropdown').not($(this)).removeClass('open');
      //Trigger NanoScroller
      $(this).find('.nano').nanoScroller({
        preventPageScrolling: true,
        alwaysVisible: true,
        sliderMaxHeight: 100
      });
      e.stopPropagation();
    });
    $(document).on('click', '.dropdown li', function(e) {
      if ($(this).hasClass('group-label')) {
        e.stopPropagation();
        return;
      }
      // For text
      var optionsValue = $(this).attr('data-value');
      var optionText = $(this).text();
      var $dropdown = $(this).closest('.dropdown');
      $dropdown.find('.value').text(optionText);
      $dropdown.find('input[type="hidden"]').val(optionsValue);
      // For input text
      var optionsValueInput = $(this).attr('title');
      $(this).closest('.dropdown').find('.input-field').val(optionsValueInput);
      $(this).parent().find('.selected').removeClass('selected');
      $(this).addClass('selected');
    });
    /*->checkbox list event */
    $(document).on('click', '.dropdown-checklist .dropdown-container', function(e) {
      e.stopPropagation();
    });
    $(document).on('change', '.dropdown-checklist input:checkbox', function(e) {
      var $this = $(this);
      var $parent = $this.closest('.dropdown-checklist');
      var $value = $parent.find('.dropdown-head .value');
      if (!$value.data('default-text')) {
        var default_text = $value.attr('data-default-text') || '';
        if (default_text.length == 0) {
          default_text = $.trim($value.text());
        }
        $value.data('default-text', default_text);
      }
      var selected = $parent.find('.dropdown-container input:checkbox:checked').size();

      if (selected == 0) {
        $value.text($value.data('default-text'));
      } else {
        $value.text(selected + ' Selected');
      }
      var values = [];
      $parent.find('input[type="hidden"]').val('');
      $parent.find('.dropdown-container input:checkbox:checked').each(function() {
        var value = $(this).val();
        values.push(value);
      });
      $parent.find('input[type="hidden"]').val(values.join(','));
      e.stopPropagation();
    });
    /*checkbox list event <-*/
    $(document).click(function() {
      $('.dropdown').removeClass('open');
    });
    if (typeof $.fn.chosen != 'undefined') {
      $('.dropdown-chosen select').chosen();

      $('.dropdown-chosen select').on('change', function(evt, params) {
        var $this = $(this);
        var $chosen = $this.closest('.dropdown-chosen');
        var value = $.trim(params.selected);
        if (value.length) {
          $chosen.find('.chosen-search input').val(value);
        }
      });
      $('.dropdown-chosen').find('.chosen-search input').focusout(function(event) {
        var value = $.trim($(this).val());
        var $chosen = $(this).closest('.dropdown-chosen');
        if (value.length) {
          $chosen.find('a.chosen-single span').text(value);
          $chosen.find('a.chosen-single').removeClass('chosen-default');
        } else {
          var default_text = $chosen.find('select').attr('data-placeholder') || '';
          $chosen.find('a.chosen-single span').text(default_text);
          $chosen.find('a.chosen-single').addClass('chosen-default');
        }
      });
      $('.dropdown-chosen').on('click', '.chosen-single', function() {
        var $this = $(this);
        var $chosen = $this.closest('.dropdown-chosen');
        var value = $this.text();
        if (!$this.hasClass('chosen-default') && value.length) {
          $chosen.find('.chosen-search input').val(value).select();
          $chosen.find('.chosen-search input').keyup();
        }
      });
    }

  },
  datepicker: function() {
    $('.dropdown-datepicker').each(function(index, el) {
      var $this = $(this);

      var $date_input = $(this).find('.datepicker');
      var $dropdownHead = $this.find('.dropdown-head');
      var $value = $dropdownHead.find('.value');
      var activity = $this.attr('activity') || '';
      var dateFormat = $date_input.attr('dateFormat') || 'dd-M-yy';
      var isRangeSelect = $this.hasClass('range-select');
      var cur = -1,
        prv = -1;
      if (isRangeSelect) {
        if ($this.find('.fromdate').length == 0) {
          $('<input type="hidden" name="fromdate" class="fromdate"><input type="hidden" name="todate" class="todate">').insertAfter($date_input);
        }

      }
      $date_input.datepicker({
        dateFormat: dateFormat,
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        firstDay: 1,
        showOtherMonths: true,
        selectOtherMonths: true,
        onSelect: function(dateText, inst) {
          if (isRangeSelect) {
            var count = parseInt($date_input.data('count-click') || 1);
            if (inst.inline) {
              inst.inline = false;
            } else {
              inst.inline = true;
            }
            var d1,
              d2;
            prv = cur;
            if (count == 1) {
              prv = cur = -1;

            }
            cur = (new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay)).getTime();
            if (prv == -1 || prv == cur) {
              prv = cur;
              $value.text(dateText);
              $this.find('.fromdate').val(dateText);
              $this.find('.todate').val('');
            } else {
              d1 = $.datepicker.formatDate('dd M', new Date(Math.min(prv, cur)), {});
              d2 = $.datepicker.formatDate('dd M', new Date(Math.max(prv, cur)), {});
              $value.text(d1 + '-' + d2);
              $this.find('.fromdate').val($.datepicker.formatDate('dd-M-yy', new Date(Math.min(prv, cur)), {}));
              $this.find('.todate').val($.datepicker.formatDate('dd-M-yy', new Date(Math.max(prv, cur)), {}));
            }
            $date_input.data('count-click', count + 1);
          } else {
            $value.text(dateText);
          }
        },
        onClose: function(dateText, inst) {
          if (isRangeSelect) {
            inst.inline = false;
            $date_input.data('count-click', '');
          }
        },
        beforeShow: function(input, inst) {
          setTimeout(function() {
            var $input = $(input);
            var top = $input.offset().top + $input.height() + 5;
            var left = $input.offset().left + $input.width() - 262;
            if (main.isDesktop()) {
              inst.dpDiv.css({
                left: left
              });
            }
          }, 10);
          var $widget = $(input).datepicker('widget');
          if (activity && !$widget.hasClass(activity))
            $widget.addClass(activity);
        },
        beforeShowDay: function(date) {
          if (isRangeSelect) {
            var fromDate = prv;
            var toDate = cur;
            if (prv == -1 && cur == -1) {
              var fromDateText = $this.find('.fromdate').val();
              var toDateText = $this.find('.todate').val();
              if (fromDateText.length && toDateText.length) {
                fromDate = $.datepicker.parseDate("dd-M-yy", fromDateText).getTime();
                toDate = $.datepicker.parseDate("dd-M-yy", toDateText).getTime();
              }
            }
            return [true, ((date.getTime() >= Math.min(fromDate, toDate) && date.getTime() <= Math.max(fromDate, toDate)) ? 'date-range-selected' : '')];
          }
          return [true, ''];
        }
      });
      // trigger show calendar
      $dropdownHead.on('click', function(e) {
        var $datepicker = $(this).find('.hasDatepicker');
        if ($datepicker.length) {
          if ($datepicker.datepicker("widget").is(":visible"))
            $datepicker.datepicker("hide");
          else
            $datepicker.datepicker("show");
        }
      });
    });
    $('.datepicker-control').each(function() {
      var $input = $(this).find('.datepicker');
      var $icon = $(this).find('.icon');
      var class_custom = $(this).attr('class');
      var dateFormat = $input.attr('dateFormat') || 'dd-M-yy';
      if ($input.length) {
        $input.datepicker({
          dateFormat: dateFormat,
          dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
          firstDay: 1,
          showOtherMonths: true,
          selectOtherMonths: true,
          beforeShow: function(input, inst) {
            if ($('#page-container').hasClass('page-event-submit')) {
              setTimeout(function() {
                var $input = $(input);
                var top = $input.offset().top + $input.outerHeight();
                var left = $input.offset().left + $input.outerWidth() - 266;
                if (main.isMobile()) {
                  inst.dpDiv.css({
                    top: top
                  });
                }
              }, 10);
            }
            var $widget = $(input).datepicker('widget');
            if (class_custom && !$widget.hasClass(class_custom))
              $widget.addClass(class_custom);
          }
        });
        if ($icon.length) {
          $icon.click(function() {
            $(this).prev('input.datepicker').datepicker('show');
          });
        }
      }
    });
  },
  rating: function() {
    if ($('.rating-control.enabled').length) {
      $('.rating-control.enabled').each(function(index, el) {
        var $rating = $(this);
        $rating.find('span').click(function(e) {
          var $this = $(this);
          var $parent = $this.parent();
          var index = $parent.find($this).index();
          $parent.find('span').removeClass('selected');
          $this.addClass('selected');
          $parent.find('span').filter(':gt(' + (index) + ')').addClass('empty');
          $parent.find('input').val(index + 1);
        });
        $rating.find('span').hover(function(e) {
          var $this = $(this);
          var $parent = $this.parent();
          var index = $parent.find($this).index();
          $parent.find('span').addClass('empty');
          $parent.find('span').filter(':lt(' + (index + 1) + ')').removeClass('empty');
          //e.stopPropagation();
        }, function(e) {
          var $this = $(this);
          var $parent = $this.parent();
          var index = $parent.find($this).index();
          $parent.find('span').addClass('empty');
          var $selected = $parent.find('.selected');
          if ($selected.length) {
            var selected_index = $selected.index();
            $parent.find('span').filter(':lt(' + (selected_index + 1) + ')').removeClass('empty');
          }
        });
        $rating.hover(function() {}, function() {
          var $parent = $(this);
          var $selected = $parent.find('.selected');
          if ($selected.length) {
            var selected_index = $selected.index();
            $parent.find('span').filter(':lt(' + (selected_index + 1) + ')').removeClass('empty');
          }
        });
      });

    }
  },
  search: function() {
    $('#page-header').find('.link-search').on('click', function() {
      $('.search-container').show();
      $('body').addClass('noScroll');
    });
    $('.search-container').find('.search-overlay-close').on('click', function() {
      $('.search-container').hide();
      $('body').removeClass('noScroll');
    });
  },
  customControl: function() {
    $(document).on('change', '.iCheckbox input', function() {
      if (this.checked) {
        $(this).parent().addClass('checked');
      } else {
        $(this).parent().removeClass('checked');
      }
    });

    $(document).on('change', '.radio-group .iRadio input', function() {
      var $parent = $(this).closest('.radio-group');
      $parent.find('.iRadio').removeClass('checked')
      $(this).parent().addClass('checked');
    });
  },
  wallpaperAds: function() {
    // ad-wallpaper
    $(window).load(function() {
      $(".ad-empty").each(function() {
        var adIframe = $(this).find("iframe")[0];
        if ($(adIframe).is("iframe")) {
          $(this).removeClass("ad-empty");
        }
      });
      var wallpaperAds = $("#wallpaper-ads");
      var wallpaperFrame = wallpaperAds.find("iframe");
      var wallpaperDisplay = wallpaperAds.css("display");

      if (wallpaperFrame.is("iframe") && wallpaperDisplay == "block") {
        if (/MSIE\s([\d.]+)/.test(navigator.userAgent)) {
          //Get the IE version.  This will be 6 for IE6, 7 for IE7, etc...
          version = new Number(RegExp.$1);
          if (version == 8) {
            var margin = ((1600 - $(document).width()) / 2) + 10;
          } else if (version > 8) {
            var margin = ((1600 - $(document).width()) / 2) + 8;
          } else {
            var margin = ((1600 - $(document).width()) / 2);
          }
        } else {
          var margin = ((1600 - $(document).width()) / 2);
        }
        wallpaperFrame.css("margin-left", "-" + margin + "px");

        $(window).resize(function() {
          if (/MSIE\s([\d.]+)/.test(navigator.userAgent)) {
            //Get the IE version.  This will be 6 for IE6, 7 for IE7, etc...
            version = new Number(RegExp.$1);
            if (version == 8) {
              var margin = ((1600 - $(document).width()) / 2) + 10;
            } else if (version > 8) {
              var margin = ((1600 - $(document).width()) / 2) + 8;
            } else {
              var margin = ((1600 - $(document).width()) / 2);
            }
          } else {
            var margin = ((1600 - $(document).width()) / 2);
          }
          wallpaperFrame.css("margin-left", "-" + margin + "px");
        });
      }
    });
  },
  desktop: {
    mobileSize: function() {
      var screen_width = main.viewport().width;
      var window_size_mobile = screen_width <= 767;
      var window_size_portrait = (screen_width > 767 && screen_width <= 980);
      var $layout = $('#layout');
      if (window_size_mobile) {
        $layout.addClass('desktop-mobile');
      } else if (window_size_portrait) {
        $layout.addClass('desktop-tablet-portrait');
      } else {
        $layout.removeClass('desktop-mobile desktop-tablet-portrait');
      }
      var $pageHeader = $('#page-header');
      if (window_size_mobile) {
        if ($pageHeader.hasClass('hasActiveScriptMobile')) {
          main.mobile.fixHeader();
        } else {
          $pageHeader.addClass('hasActiveScriptMobile');
          main.mobile.init();
        }
      } else {
        main.mobile.unFixHeader();
        setTimeout(function() {
          $('body').removeClass('noScroll');
          $pageHeader.find('.navbar,.navbar-nav').removeAttr('style');
        }, 200);
      }
    },
    init: function() {
      main.desktop.mobileSize();
      $(window).bind('resize', function(event) {
        main.desktop.mobileSize();
      });
    }
  },
  msie: {
    placeholder: function() {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur();
      //trigger submit form
      $('[placeholder]').parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
        })
      });
    },
    init: function() {
      // for IE version <= 9
      if ($.browser.msie && $.browser.version <= 9) {
        this.placeholder();
      }
    }
  },
  mobile: {
    showSearch: function(ext) {
      this.hideMenu('ignore_extend');
      $('.header-mobile .header-front').addClass('hidden');
      $('.header-mobile .input-search-group').removeClass('hidden');
      $('.header-mobile .input-search-group .input-search-text').focus();
      $('.search-container').show();
      if (ext != 'ignore_extend') {
        localStorage.cachedScrollTop = $('body').scrollTop();
        $('body').addClass('noScroll');
      }
    },
    hideSearch: function(ext) {
      $('.search-container').hide();
      $('.header-mobile .header-front').removeClass('hidden');
      $('.header-mobile .input-search-group').addClass('hidden');
      if (ext != 'ignore_extend') {
        $('body').removeClass('noScroll');
        $('body').scrollTop(localStorage.cachedScrollTop || 0);
      }
    },
    showMenu: function(ext) {
      this.hideSearch('ignore_extend');
      var $this = $('.nav-mobile');
      $this.find('.icon').addClass('icon-menu-close').removeClass('icon-menu');
      $('.navbar').removeClass('hidden-xs');
      $this.addClass('opened');
      this.fixHeightNavbar();
      if (ext != 'ignore_extend') {
        localStorage.cachedScrollTop = $('body').scrollTop();
        $('body').addClass('noScroll');
      }
    },
    hideMenu: function(ext) {
      var $this = $('.nav-mobile');
      $this.find('.icon').addClass('icon-menu').removeClass('icon-menu-close');
      $('.navbar').addClass('hidden-xs');
      $this.removeClass('opened');
      if (ext != 'ignore_extend') {
        $('body').removeClass('noScroll');
        $('body').scrollTop(localStorage.cachedScrollTop || 0);
      }

    },
    header: function() {
      var thisObj = this;
      $('.header-mobile .link-search-mobile').on('click', function() {
        var opt = '';
        if ($('.navbar').is(':visible'))
          opt = 'ignore_extend';
        thisObj.showSearch(opt);
        return false;
      });
      $('.search-container').find('.search-overlay-close').on('click', function() {
        var opt = '';
        if ($('.navbar').is(':visible'))
          opt = 'ignore_extend';
        thisObj.hideSearch(opt);
        return false;
      });
      // show menu
      $('.header-mobile .nav-mobile').on('click', function() {
        var $this = $(this);
        var opt = '';
        if ($('.search-container').is(':visible'))
          opt = 'ignore_extend';
        if ($this.hasClass('opened')) {
          thisObj.hideMenu(opt);
        } else {
          thisObj.showMenu(opt);
        }

        return false;
      });
    },
    dectectScroll: function() {
      var thisObj = this;
      var $header = $('#page-header');
      var header_height = $header.find('.header-mobile').height();
      var positionNaviSite = $('#page-header .header-mobile').position().top + header_height;
      var bodyScrollTop = $('body').scrollTop();
      if (bodyScrollTop > positionNaviSite) {
        thisObj.fixHeader();
      } else {
        thisObj.unFixHeader();
      }
    },
    fixHeader: function() {
      var $header = $('#page-header');
      $('body').addClass('respon-sticky');
      $header.addClass('sticky');
      this.fixHeightNav();
    },
    fixHeightNav: function() {
      $('.navbar').height($(window).height() - 54);
    },
    fixHeightNavbar: function() {
      var navbar = $('.navbar');
      var navbarNav = $('.navbar-nav');
      var social = $('.navbar .social-share');
      var login = $('.navbar .login-panel');
      $('.navbar-nav').css('height', 'auto');
      if (navbar.height() > navbarNav.height() + 105) {
        $('.navbar-nav').height($('.navbar').height() - 128);
      }
    },
    unFixHeader: function() {
      var $header = $('#page-header');
      $('body').removeClass('respon-sticky');
      $header.removeClass('sticky');
    },
    resize: function() {
      this.fixHeightNav();
      if ($('.navbar').is(':visible')) {
        this.fixHeightNavbar();
      }

    },
    init: function() {
      var thisObj = this;
      this.header();
      this.fixHeader();
      $(window).resize(function() {
        thisObj.resize();
      });
    }
  },
  checkellipsis: function(obj) {
    $(obj).each(function() {
      if ($(this).find('.title-wrap').height() > 48) {
        $(this).find('.title-wrap').addClass('pure-ellipsis');
      }

      if ($(this).find('.desc').height() > 40) {
        $(this).find('.desc').addClass('pure-ellipsis');
      }
    });
  },
  truncate: function(obj, line) {
    line = line || 2;
    var text = $(obj).eq(0).text();
    var fontFamily = $(obj).eq(0).css('font-family');
    var fontSize = $(obj).eq(0).css('font-size');
    var lineHeight = $(obj).eq(0).css('line-height');
    var size = main.calculateSize(text, {
      font: fontFamily,
      fontSize: fontSize,
      lineHeight: lineHeight
    });
    var maxHeight = size.height * line;
    $(obj).each(function() {
      if ($(this).height() > maxHeight) {
        $(this).addClass('pure-ellipsis');
        $(this).css('max-height', maxHeight);
      }
    });
  },

  truncate_auto: function() {
    var fn = function(obj) {
      var line = $(obj).attr('data-truncate-line') || '2';

      var text = $(obj).text();
      var fontFamily = $(obj).css('font-family');
      var fontSize = $(obj).css('font-size');
      var lineHeight = $(obj).css('line-height');
      var size = main.calculateSize(text, {
        font: fontFamily,
        fontSize: fontSize,
        lineHeight: lineHeight
      });
      if (main.isMobile()) {
        var mobile_line = $(obj).attr('data-truncate-mobile-line') || '';
        if (mobile_line.length) {
          line = mobile_line;
        }
      }
      var maxHeight = size.height * line;
      if ($(obj).height() > maxHeight) {
        $(obj).addClass('pure-ellipsis');
        $(obj).css('max-height', maxHeight);
      }
    };
    if ($('.isTruncate').length) {
      $('.isTruncate').each(function(index, el) {
        fn(this);
      });
    }
  },
  scrollTop: function() {
    $().UItoTop({
      text: 'Scroll To Top',
      easingType: 'easeOutQuart',
      containerID: 'toTop'
    });
  },
  modernizr_detect: function() {
    if (typeof Modernizr != "undefined") {
      if (Modernizr.touch)
        $('#layout').addClass('touch-device');
      else
        $('#layout').addClass('pc-device');
    }
  },
  expand: {
    toggle: function($this) {
      var $module = $this.closest('.module-expand');
      var $panelExpand = $module.find('.panel-expand');
      var $item = $panelExpand.find('.expand-item');
      var itemShow = parseInt($module.attr('item-show') || 1);
      var perRow = parseInt($module.attr('item-per-row') || 1);
      var minHeight = $panelExpand.data('min-height');
      var timeout = 1000;
      var min_height = $panelExpand.data('min-height');
      var opened_class = 'expanded';
      if ($module.hasClass(opened_class)) {
        $panelExpand.stop().animate({
          height: min_height
        }, timeout);
        var scrollTop = $panelExpand.offset().top;
        $('html,body').animate({
          scrollTop: scrollTop
        }, timeout);
        $module.removeClass(opened_class);
      } else {
        $item.filter(':gt(' + (itemShow - 1) + ')').show();
        $item.filter(':gt(' + (itemShow - 1) + ')').removeClass('hidden');
        var item_per_row = 1;
        var max_height = Math.ceil($item.size() / perRow) * minHeight / (itemShow / perRow);
        if (perRow == 1) {
          max_height = 0;
          $item.each(function(index, el) {
            max_height = max_height + $(this).outerHeight();
          });
        }
        var cache_max_height = $panelExpand.data('max-height') || '';
        if (cache_max_height) {
          max_height = parseInt(cache_max_height);
        }
        $panelExpand.stop().animate({
          height: max_height
        }, timeout, function() {
          $module.addClass(opened_class);
          if (('' + cache_max_height).length == 0) {
            setTimeout(function() {
              $panelExpand.data('max-height', $panelExpand.height());
            }, 100);
          }
        });
      }
    },
    init: function() {
      $('.module-expand').each(function(index, el) {
        var $module = $(this);
        var $panelExpand = $module.find('.panel-expand');
        var $item = $panelExpand.find('.expand-item');
        var $panelView = $module.find('.panel-view');
        var itemShow = parseInt($module.attr('item-show') || 1);
        var perRow = parseInt($module.attr('item-per-row') || 1);
        var dataLoad = $module.attr('data-load') || '';
        var size = $item.size();
        // set min height
        $item.filter(':gt(' + (itemShow - 1) + ')').hide();
        var minHeight = $panelExpand.height();
        $panelExpand.data('min-height', minHeight);
        $panelExpand.height(minHeight);
        if (dataLoad == 'ajax') {
          return;
        } else if (size <= itemShow) {
          $panelView.hide();
          return;
        }
        $panelView.find('a').click(function(event) {
          main.expand.toggle($(this));
        });

      });
    }
  },
  init: function() {
    this.modernizr_detect();
    this.dropdown();
    this.datepicker();
    this.customControl();
    this.search();
    this.event();
    this.rating();
    this.wallpaperAds();

    // for mobile
    if (main.isMobile()) {
      this.mobile.init();
    } else if (main.isDesktop()) {
      this.desktop.init();
    }
    // for IE
    this.msie.init();
    if (main.isHomePage()) {
      main.carousel();
      main.homepage_onResize();
      setTimeout(function() {
        main.checkellipsis('.module-grid-list .item');
        main.homepage_setMinHeightGridList();
      }, 500);
    }
    main.movie.init();
    // auto truncate
    setTimeout(function() {
      main.truncate_auto();
      main.expand.init();
    }, 500);

  }
};
// ready
$(function() {
  main.init();
  // $('.event-search').show();
  // $(document).on('click', function(event) {
  //   var class_name = $(this).attr('class');
  //   alert('class_name: ' + class_name);
  // });
});
